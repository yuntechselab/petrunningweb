-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- 主機: 127.0.0.1
-- 產生時間： 2015 �?06 ??16 ??17:53
-- 伺服器版本: 5.6.17
-- PHP 版本： 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- 資料庫： `supermen_db`
--
CREATE DATABASE IF NOT EXISTS `supermen_db` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `supermen_db`;

-- --------------------------------------------------------

--
-- 資料表結構 `apply_event`
--

CREATE TABLE IF NOT EXISTS `apply_event` (
  `no` int(11) NOT NULL AUTO_INCREMENT,
  `num` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `organizerName` varchar(255) DEFAULT NULL,
  `head` varchar(255) DEFAULT NULL,
  `ticketName` varchar(255) DEFAULT NULL,
  `ticketPrice` varchar(255) DEFAULT NULL,
  `date` varchar(255) DEFAULT NULL,
  `initialDate` varchar(255) DEFAULT NULL,
  `closeDate` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `eventNo` int(11) DEFAULT NULL,
  PRIMARY KEY (`no`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- 資料表的匯出資料 `apply_event`
--

INSERT INTO `apply_event` (`no`, `num`, `name`, `email`, `phone`, `organizerName`, `head`, `ticketName`, `ticketPrice`, `date`, `initialDate`, `closeDate`, `status`, `eventNo`) VALUES
(1, '20150616173841', '吳嘉國', '123@gmail.com', '0916716311', '吳嘉國', '123', '資管組', '免費票', '2015-06-17 20:00 ~ 2015-06-17 22:00', '2015-06-17', '2015-06-17 22:00', '已報名', 1);

-- --------------------------------------------------------

--
-- 資料表結構 `event`
--

CREATE TABLE IF NOT EXISTS `event` (
  `no` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) DEFAULT NULL,
  `head` varchar(255) DEFAULT NULL,
  `startDate` varchar(255) DEFAULT NULL,
  `startTime` varchar(255) DEFAULT NULL,
  `endDate` varchar(255) DEFAULT NULL,
  `endTime` varchar(255) DEFAULT NULL,
  `timeZone` varchar(255) DEFAULT NULL,
  `area` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `address2` varchar(255) NOT NULL DEFAULT '',
  `organizerName` varchar(255) DEFAULT NULL,
  `organizerEmail` varchar(255) DEFAULT NULL,
  `organizerPhone` varchar(255) DEFAULT NULL,
  `category` varchar(255) DEFAULT NULL,
  `summary` varchar(255) DEFAULT NULL,
  `content` varchar(255) DEFAULT NULL,
  `groupName` varchar(255) DEFAULT NULL,
  `ticketName` varchar(255) DEFAULT NULL,
  `ticketPrice` varchar(255) DEFAULT NULL,
  `ticketCount` varchar(255) DEFAULT NULL,
  `ticketSale` varchar(255) DEFAULT NULL,
  `ticketStartDate` varchar(255) DEFAULT NULL,
  `ticketStartTime` varchar(255) DEFAULT NULL,
  `ticketEndDate` varchar(255) DEFAULT NULL,
  `ticketEndTime` varchar(255) DEFAULT NULL,
  `ticketDescription` varchar(255) NOT NULL DEFAULT '',
  `precaution` varchar(255) NOT NULL DEFAULT '',
  `view` int(255) DEFAULT '0',
  `image` varchar(255) DEFAULT '',
  PRIMARY KEY (`no`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- 資料表的匯出資料 `event`
--

INSERT INTO `event` (`no`, `email`, `head`, `startDate`, `startTime`, `endDate`, `endTime`, `timeZone`, `area`, `address`, `address2`, `organizerName`, `organizerEmail`, `organizerPhone`, `category`, `summary`, `content`, `groupName`, `ticketName`, `ticketPrice`, `ticketCount`, `ticketSale`, `ticketStartDate`, `ticketStartTime`, `ticketEndDate`, `ticketEndTime`, `ticketDescription`, `precaution`, `view`, `image`) VALUES
(1, '123@gmail.com', '環校慢跑：永遠的夜間領跑者', '2015-06-17', '20:00', '2015-06-17', '22:00', '78', '9', '斗六市大學路123號', '', '蕭維慶', '123@gmail.com', '0912345678', '6', '欣賞校園兼慢跑擁有好心情', '<p>能夠欣賞重未停下腳步的夜間校園，</p><p>有益身體健康的慢跑兼顧，</p><p>讓我們跟著PO一起去慢跑吧~~</p>', '資管組', '資管組', '免費票', '1', 'true', '2015-05-01', '0:00', '2015-06-17', '17:00', '', '', 26, '擷取2.PNG'),
(2, '123@gmail.com', '悠閒騎車趣：帶您走過男主角曾走過的路', '2015-06-17', '0:00', '2015-06-21', '0:00', '78', '9', '斗六市大學路123號', '', '男主角', '123@gmail.com', '0912345678', '1', '每天兩小時與男主角腳踏車相會', '<p>斗六的最佳風景，<br></p><p>就在每天兩小時與男主角的腳踏車相會<span class="redactor-invisible-space">，</span></p><p><span class="redactor-invisible-space">心動不如馬上騎上鐵馬行動～</span></p>', '悠遊組', '騎車樂', '免費票', '100', 'true', '2015-05-01', '0:00', '2015-06-20', '10:09', '', '', 14, '11270584_981357598575740_248393023432671141_o.jpg'),
(3, '123@gmail.com', '斗六路跑：擁有情侶裝的開始', '2015-06-21', '6:00', '2015-06-21', '15:00', '78', '9', '斗六市大學路123號', '', '羅嘉文', '123@gmail.com', '0912345678', '6', '繞著斗六跑十圈', '<p><strong></strong>跑完一個斗六需要花多久的時間呢？</p><p>好奇嗎？　</p><p>不如跟著我一起嘗試吧！</p>', '路跑組', '運動組', '200', '2000', 'true', '2015-05-01', '0:00', '2015-06-17', '10:09', '', '', 4, '10479395_1127818353901466_1869734966188740266_n.jpg'),
(4, '123@gmail.com', '羽球任務：用羽球與巧克力哥有約', '2015-06-20', '15:00', '2015-06-20', '18:00', '78', '6', '台灣大道123號', '', '喬克維奇', '123@gmail.com', '0912345678', '1', '喬克維奇教你打羽球', '<p>羽球馬賽克友誼<strong></strong>賽，</p><p>然後喬克維奇是打網球的，</p><p>所以他不會出現唷～</p>', '大學組', '學生票', '免費票', '100', 'true', '2015-05-01', '4:00', '2015-06-17', '12:00', '', '', 13, '11393216_838016552919917_2757012875814330138_n.jpg'),
(5, '123@gmail.com', '心平氣和講座：不要為了我吵架', '2015-06-18', '14:00', '2015-06-18', '17:00', '78', '9', '雲林縣斗六市大學路3段123號', '雲林科技大學管理學院MA214', '葉少女', 'iamgirl@gg.com', '0987654321', '7', '葉少女的奇幻世界 ❤', '<p>我是葉少女，</p><p>我最大的煩惱就是大家都為了我吵架，</p><p>時間久了以後，我也練就了一套金剛不壞的守則，</p><p>所謂的葉少女UX之大家不要為了我吵架，</p><p>想聽我的分享嗎？　</p>', '女孩專屬票', '女孩專屬票', '0', '100', 'true', '2015-05-16', '4:00', '2015-06-16', '23:59', '', '感謝您的參加，葉少女等你唷～揪咪～', 23, '擷取.PNG'),
(6, '123@gmail.com', '每日雲夢湖手牽手散步趣', '2015-06-25', '22:00', '2015-06-30', '22:00', '78', '9', '雲林縣斗六市大學路3段123號 ', '雲夢湖', '天菜', 'HTJhang@gg.com', '0987654321', '6', '雲夢湖是個散步的好地方唷 !! 有益身體健康~', '<p>雲科雲夢湖歷史悠久，</p><p>不盡環境優美有鴨子還有壽星丟湖中外，</p><p>最近最夯的就是散步了，</p><p>多運動絕對有益身體健康唷~</p>', '原PO專屬票', '原PO專屬票', '免費票', '1', 'true', '2015-05-24', '4:00', '2015-06-17', '23:59', '', '', 10, '10959574_839718052783133_4643929136718211403_n.png');

-- --------------------------------------------------------

--
-- 資料表結構 `member`
--

CREATE TABLE IF NOT EXISTS `member` (
  `no` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `sex` varchar(255) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `nickname` varchar(255) NOT NULL DEFAULT '',
  `date` varchar(255) NOT NULL DEFAULT '',
  `id` varchar(255) NOT NULL DEFAULT '',
  `intro` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`no`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- 資料表的匯出資料 `member`
--

INSERT INTO `member` (`no`, `name`, `email`, `sex`, `code`, `phone`, `password`, `nickname`, `date`, `id`, `intro`) VALUES
(1, '吳嘉國', '123@gmail.com', '1', 'TW', '0916716311', '123', 'Dre', '2015-07-10', 'J122661926', '<p>456</p>');

-- --------------------------------------------------------

--
-- 資料表結構 `pedometer`
--

CREATE TABLE IF NOT EXISTS `pedometer` (
  `no` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) DEFAULT NULL,
  `step` varchar(255) DEFAULT NULL,
  `distance` varchar(255) DEFAULT NULL,
  `pace` varchar(255) DEFAULT NULL,
  `speed` varchar(255) DEFAULT NULL,
  `calories` varchar(255) DEFAULT NULL,
  `date` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`no`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- 資料表的匯出資料 `pedometer`
--

INSERT INTO `pedometer` (`no`, `email`, `step`, `distance`, `pace`, `speed`, `calories`, `date`) VALUES
(1, '123@gmail.com', '0', '40', '0', '0', '0', '2014-08-01'),
(2, '123@gmail.com', '0', '20', '0', '0', '0', '2015-03-14'),
(3, '123@gmail.com', '10', '0.001', '88', '1.05', '0', '2015-06-07'),
(4, '123@gmail.com', '10', '0.001', '88', '1.05', '0', '2015-06-12'),
(5, '123@gmail.com', '12', '40', '188', '2.25', '0', '2015-06-15'),
(6, '123@gmail.com', '12', '50', '188', '2.25', '0', '2015-06-15'),
(7, '123@gmail.com', '12', '50', '188', '2.25', '0', '2015-06-10'),
(8, '123@gmail.com', '12', '50', '188', '2.25', '0', '2015-06-15');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
