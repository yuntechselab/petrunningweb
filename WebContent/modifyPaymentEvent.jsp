﻿<%@ page language="java" import="com.supermen.vo.EventVo" import="com.supermen.servlet.controller.GlobalVariable" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<% EventVo event = (EventVo)request.getAttribute("event"); %>
<!DOCTYPE html>
<html ng-app="accupass.myEvent.detail" class="js flexbox flexboxlegacy canvas canvastext webgl no-touch geolocation postmessage websqldatabase indexeddb hashchange history draganddrop websockets rgba hsla multiplebgs backgroundsize borderimage borderradius boxshadow textshadow opacity cssanimations csscolumns cssgradients cssreflections csstransforms csstransforms3d csstransitions fontface generatedcontent video audio localstorage sessionstorage webworkers applicationcache svg inlinesvg smil svgclippaths ng-scope">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"><style type="text/css">.gm-style .gm-style-mtc label,.gm-style .gm-style-mtc div{font-weight:400}</style><style type="text/css">.gm-style .gm-style-cc span,.gm-style .gm-style-cc a,.gm-style .gm-style-mtc div{font-size:10px}</style><link type="text/css" rel="stylesheet" href="./createEvent_files/css"><style type="text/css">@media print {  .gm-style .gmnoprint, .gmnoprint {    display:none  }}@media screen {  .gm-style .gmnoscreen, .gmnoscreen {    display:none  }}</style><style type="text/css">.gm-style{font-family:Roboto,Arial,sans-serif;font-size:11px;font-weight:400;text-decoration:none}</style><style type="text/css">@charset "UTF-8";[ng\:cloak],[ng-cloak],[data-ng-cloak],[x-ng-cloak],.ng-cloak,.x-ng-cloak,.ng-hide{display:none !important;}ng\:form{display:block;}.ng-animate-block-transitions{transition:0s all!important;-webkit-transition:0s all!important;}.ng-hide-add-active,.ng-hide-remove{display:block!important;}</style>

    <title>Supermen</title>
	<link rel="Shortcut Icon" type="image/x-icon" href="./icon/shortcut.png" />
    <link type="text/css" rel="stylesheet" href="./register_files/css.min.css">
    <script type="application/javascript" async="" defer="" src="./createEvent_files/track.js"></script><script type="text/javascript" async="" src="./createEvent_files/26hI9f54AQMRNaPo1MvFkw.js"></script><script id="facebook-jssdk" src="./createEvent_files/all.js"></script><script type="text/javascript" async="" src="./createEvent_files/atrk.js"></script><script async="" src="./createEvent_files/analytics.js"></script><script async="" src="./createEvent_files/fbds.js"></script><script type="text/javascript" src="./createEvent_files/js.min.js"></script>

    <script type="text/javascript" src="./createEvent_files/resource"></script>
    
	<script src="uploadify/jquery.uploadify.min.js" type="text/javascript"></script>
	<link href="uploadify/uploadify.css" rel="stylesheet" type="text/css" />  
	<link href="uploadify/customer.css" rel="stylesheet" type="text/css" />
	
<script type="text/javascript">
	function check() {
		var startDate = document.reg.startDate.value; 
		startDate.replace(/-/g, "/");
		var endDate = document.reg.endDate.value; 
		endDate.replace(/-/g, "/");
		var startTime = startDate + " " + document.reg.startTime.value; //設定開始時間格式
		var endTime = endDate + " " + document.reg.endTime.value; //設定結束時間格式
		
		var ticketStartDate = document.reg.ticketStartDate.value; 
		ticketStartDate.replace(/-/g, "/");
		var ticketEndDate = document.reg.ticketEndDate.value; 
		ticketEndDate.replace(/-/g, "/");
		var ticketStartTime = ticketStartDate + " " + document.reg.ticketStartTime.value; //設定售票開始時間格式
		var ticketEndTime = ticketEndDate + " " + document.reg.ticketEndTime.value; //設定售票結束時間格式
		
		var email = document.reg.organizerEmail.value;
	    var rege = /^[-A-Za-z0-9_]+[-A-Za-z0-9_.]*[@]{1}[-A-Za-z0-9_]+[-A-Za-z0-9_.]*[.]{1}[A-Za-z]{2,5}$/;
	    
	    if (document.reg.head.value == "") {
			alert("任務名稱為必填欄位");
			document.reg.head.focus();
			return false;
		}
	    
	    if (Date.parse(startTime).valueOf() > Date.parse(endTime).valueOf()) {
	    	alert("開始時間不能晚於結束時間");
	    	return false;
	    }
	    
		if (document.reg.startDate.value == "") {
			alert("開始日期為必填欄位");
			document.reg.startDate.focus();
			return false;
		}
		
		if (document.reg.startTime.value == "") {
			alert("開始時間為必填欄位");
			document.reg.startTime.focus();
			return false;
		}
		
		if (document.reg.endDate.value == "") {
			alert("結束日期為必填欄位");
			document.reg.endDate.focus();
			return false;
		}
		
		if (document.reg.endTime.value == "") {
			alert("結束時間為必填欄位");
			document.reg.endTime.focus();
			return false;
		}
		
		if (reg.area.value == "none") {
			alert("地區為必選欄位");
			document.reg.area.focus();
			return false;
		}
		
		if (document.reg.address.value == "") {
			alert("地址為必填欄位");
			document.reg.address.focus();
			return false;
		}

		if (document.reg.organizerName.value == "") {
			alert("創建人名稱為必填欄位");
			document.reg.organizerName.focus();
			return false;
		}
		
		if (document.reg.organizerEmail.value == "") {
			alert("創建人Email為必填欄位");
			document.reg.organizerEmail.focus();
			return false;
		}
		
        if (rege.exec(email) == null) { 
			alert("無效的創建人電子郵件地址");
			document.reg.organizerEmail.focus();
			return false;
        }
        
		if (document.reg.organizerPhone.value == "") {
			alert("創建人手機為必填欄位");
			document.reg.organizerPhone.focus();
			return false;
		}
		
	    if (document.reg.organizerPhone.value.length < 10) {
			alert("創建人手機長度過短");
			document.reg.organizerPhone.focus();
			return false;
		}
						
		if (reg.category.value == "none") {
			alert("任務類型為必選欄位");
			document.reg.category.focus();
			return false;
		}
	    
		if (document.reg.summary.value == "") {
			alert("任務摘要為必填欄位");
			document.reg.summary.focus();
			return false;
		}
		
		if (document.reg.content.value == "") {
			alert("任務介紹為必填欄位");
			document.reg.content.focus();
			return false;
		}
		
		if (document.reg.groupName.value == "") {
			alert("分組名稱為必填欄位");
			document.reg.groupName.focus();
			return false;
		}

		if (document.reg.ticketName.value == "") {
			alert("票劵名稱為必填欄位");
			document.reg.ticketName.focus();
			return false;
		}
		
		if (document.reg.ticketPrice.value == "") {
			alert("票價為必填欄位");
			document.reg.ticketPrice.focus();
			return false;
		}
		
		if (document.reg.ticketCount.value == "") {
			alert("張數為必填欄位");
			document.reg.ticketCount.focus();
			return false;
		}
		
		if (document.reg.ticketCount.value < 1) {
			alert("票券數量不能低於 1");
			document.reg.ticketCount.focus();
			return false;
		}
		
	    if (Date.parse(ticketStartTime).valueOf() > Date.parse(ticketEndTime).valueOf()) {
	    	alert("售票開始時間不能晚於售票結束時間");
	    	return false;
	    }
	    
		if (document.reg.ticketStartDate.value == "") {
			alert("售票開始日期為必填欄位");
			document.reg.ticketStartDate.focus();
			return false;
		}
		
		if (document.reg.ticketStartTime.value == "") {
			alert("售票開始時間為必填欄位");
			document.reg.ticketStartTime.focus();
			return false;
		}
		
		if (document.reg.ticketEndDate.value == "") {
			alert("售票結束日期為必填欄位");
			document.reg.ticketEndDate.focus();
			return false;
		}
		
		if (document.reg.ticketEndTime.value == "") {
			alert("售票結束時間為必填欄位");
			document.reg.ticketEndTime.focus();
			return false;
		}
		
		document.reg.submit();
		return true;		
	}
	
	$(function() {  
  		$("#upload_org_code").uploadify({
	       	'auto'            : true,
	       	'buttonClass'     : 'my-uploadify-button',
	        'buttonText'      : '上傳圖片',
	        'fileSizeLimit'   : '4MB',
	        'width'           : 880,
	        'height'          : 48,
	        'fileTypeExts'    : '*.jpg;*.jpge;*.gif;*.png',
	        'fileTypeDesc'    : '請上傳圖片格式(jpg, jpeg, gif, png)',
	        'progressData'    : 'speed',
	        'queueID'         : 'some_file_queue',
	        'removeCompleted' : false,
	        'cancelImg'       : '${pageContext.request.contextPath}/js/uploadify/uploadify-cancel.png',
	        'queueSizeLimit'  : 2,
	        'removeTimeout'   : 1,
	        'swf'             : '${pageContext.request.contextPath}/uploadify/uploadify.swf',
	        'uploader'        : '${pageContext.request.contextPath}/UploadifyServlet',
	        'multi'           : false,
	        'overrideEvents'  : ['onSelectError', 'onDialogClose', 'onCancel', 'onClearQueue'],
	        'onClearQueue'    : function(queueItemCount) {
	        	console.log(queueItemCount);
	        },
	       	'onDialogClose'   : function(queueData) {
	         	console.log(queueData.filesSelected);
	          	console.log(queueData.filesQueued);
	      	},
	      	'onUploadSuccess' : function(file, data, response) {
	          	$('#' + file.id).find('.data').html('');
	          	console.log('data=' + data);
	           	$("#upload_org_code_name").val(data);
	           	$("#upload_org_code_img").attr("src", "${pageContext.request.contextPath}/GetImgServlet?file=" + data);  
	           	$("#upload_org_code_img").show();
	       	},
	     	'onSelectError'   : function(file, errorCode, errorMsg) {
	       		switch(errorCode) {
	            	case -110: //file.name檔案名字
	               		alert("您的檔案格式大小不符合規定，請重新上傳");
	                	break;
	                	
	              	case -120:
	                   	alert("您的檔案大小異常");
	                  	break;
	                  	
	               	case -130:
	                   	alert("請上傳圖片格式(jpg, jpeg, gif, png)");
	                   	break;
	           	}
	      	}
		});
	});
</script>

<style type="text/css" media="screen">
.my-uploadify-button {
	background: none;
	border: none;
	text-shadow: none;
	border-radius: 0;
	font-size: 14.5px;
	font-family: 新細明體;
	font-weight: normal;
}
.uploadify:hover .my-uploadify-button {
	background: none;
	border: none;
}

.uploadImg {
	line-height: 45px;
	height: 45px;
	background: rgba(0,0,0,.5);
	background: #000 \9;
	color: #fff;
	width: 100%;
	display: block;
	position: absolute;
	z-index: 0;
	left: 0;
	bottom: 0;
}
.uploadImg:hover {
	background: rgba(0,0,0,.75);
	color: #fff;
}
</style>

	<script type="text/javascript" charset="UTF-8" src="./createEvent_files/common.js"></script><script type="text/javascript" charset="UTF-8" src="./createEvent_files/map.js"></script><script type="text/javascript" charset="UTF-8" src="./createEvent_files/util.js"></script><script type="text/javascript" charset="UTF-8" src="./createEvent_files/marker.js"></script><script type="text/javascript" charset="UTF-8" src="./createEvent_files/geocoder.js"></script><style type="text/css" media="screen">.uv-icon{-moz-box-sizing:border-box;-webkit-box-sizing:border-box;box-sizing:border-box;display:inline-block;cursor:pointer;position:relative;-moz-transition:all 300ms;-o-transition:all 300ms;-webkit-transition:all 300ms;transition:all 300ms;width:39px;height:39px;position:fixed;z-index:100002;opacity:0.8;-moz-transition:opacity 100ms;-o-transition:opacity 100ms;-webkit-transition:opacity 100ms;transition:opacity 100ms}.uv-icon.uv-bottom-right{bottom:10px;right:12px}.uv-icon.uv-top-right{top:10px;right:12px}.uv-icon.uv-bottom-left{bottom:10px;left:12px}.uv-icon.uv-top-left{top:10px;left:12px}.uv-icon.uv-is-selected{opacity:1}.uv-icon svg{width:39px;height:39px}.uv-popover{font-family:sans-serif;font-weight:100;font-size:13px;color:black;position:fixed;z-index:100001}.uv-popover-content{-moz-border-radius:5px;-webkit-border-radius:5px;border-radius:5px;background:white;position:relative;width:325px;height:325px;-moz-transition:background 200ms;-o-transition:background 200ms;-webkit-transition:background 200ms;transition:background 200ms}.uv-bottom .uv-popover-content{-moz-box-shadow:rgba(0,0,0,0.3) 0 -10px 60px,rgba(0,0,0,0.1) 0 0 20px;-webkit-box-shadow:rgba(0,0,0,0.3) 0 -10px 60px,rgba(0,0,0,0.1) 0 0 20px;box-shadow:rgba(0,0,0,0.3) 0 -10px 60px,rgba(0,0,0,0.1) 0 0 20px}.uv-top .uv-popover-content{-moz-box-shadow:rgba(0,0,0,0.3) 0 10px 60px,rgba(0,0,0,0.1) 0 0 20px;-webkit-box-shadow:rgba(0,0,0,0.3) 0 10px 60px,rgba(0,0,0,0.1) 0 0 20px;box-shadow:rgba(0,0,0,0.3) 0 10px 60px,rgba(0,0,0,0.1) 0 0 20px}.uv-left .uv-popover-content{-moz-box-shadow:rgba(0,0,0,0.3) 10px 0 60px,rgba(0,0,0,0.1) 0 0 20px;-webkit-box-shadow:rgba(0,0,0,0.3) 10px 0 60px,rgba(0,0,0,0.1) 0 0 20px;box-shadow:rgba(0,0,0,0.3) 10px 0 60px,rgba(0,0,0,0.1) 0 0 20px}.uv-right .uv-popover-content{-moz-box-shadow:rgba(0,0,0,0.3) -10px 0 60px,rgba(0,0,0,0.1) 0 0 20px;-webkit-box-shadow:rgba(0,0,0,0.3) -10px 0 60px,rgba(0,0,0,0.1) 0 0 20px;box-shadow:rgba(0,0,0,0.3) -10px 0 60px,rgba(0,0,0,0.1) 0 0 20px}.uv-ie8 .uv-popover-content{position:relative}.uv-ie8 .uv-popover-content .uv-popover-content-shadow{display:block;background:black;content:'';position:absolute;left:-15px;top:-15px;width:100%;height:100%;filter:progid:DXImageTransform.Microsoft.Blur(PixelRadius=15,MakeShadow=true,ShadowOpacity=0.30);z-index:-1}.uv-popover-tail{border:9px solid transparent;width:0;z-index:10;position:absolute;-moz-transition:border-top-color 200ms;-o-transition:border-top-color 200ms;-webkit-transition:border-top-color 200ms;transition:border-top-color 200ms}.uv-top .uv-popover-tail{bottom:-20px;border-top:11px solid white}.uv-bottom .uv-popover-tail{top:-20px;border-bottom:11px solid white}.uv-left .uv-popover-tail{right:-20px;border-left:11px solid white}.uv-right .uv-popover-tail{left:-20px;border-right:11px solid white}.uv-popover-loading{background:white;-moz-border-radius:5px;-webkit-border-radius:5px;border-radius:5px;position:absolute;width:100%;height:100%;left:0;top:0}.uv-popover-loading-text{position:absolute;top:50%;margin-top:-0.5em;width:100%;text-align:center}.uv-popover-iframe-container{height:100%}.uv-popover-iframe{-moz-border-radius:5px;-webkit-border-radius:5px;border-radius:5px;overflow:hidden}.uv-is-hidden{display:none}.uv-is-invisible{display:block !important;visibility:hidden !important}.uv-is-transitioning{display:block !important}.uv-no-transition{-moz-transition:none !important;-webkit-transition:none !important;-o-transition:color 0 ease-in !important;transition:none !important}.uv-fade{opacity:1;-moz-transition:opacity 200ms ease-out;-o-transition:opacity 200ms ease-out;-webkit-transition:opacity 200ms ease-out;transition:opacity 200ms ease-out}.uv-fade.uv-is-hidden{opacity:0}.uv-scale-top,.uv-scale-top-left,.uv-scale-top-right,.uv-scale-bottom,.uv-scale-bottom-left,.uv-scale-bottom-right,.uv-scale-right,.uv-scale-right-top,.uv-scale-right-bottom,.uv-scale-left,.uv-scale-left-top,.uv-scale-left-bottom,.uv-slide-top,.uv-slide-bottom,.uv-slide-left,.uv-slide-right{opacity:1;-moz-transition:all 80ms ease-out;-o-transition:all 80ms ease-out;-webkit-transition:all 80ms ease-out;transition:all 80ms ease-out}.uv-scale-top.uv-is-hidden{opacity:0;-moz-transform:scale(0.8) translateY(-15%);-ms-transform:scale(0.8) translateY(-15%);-webkit-transform:scale(0.8) translateY(-15%);transform:scale(0.8) translateY(-15%)}.uv-scale-top-left.uv-is-hidden{opacity:0;-moz-transform:scale(0.8) translateY(-15%) translateX(-10%);-ms-transform:scale(0.8) translateY(-15%) translateX(-10%);-webkit-transform:scale(0.8) translateY(-15%) translateX(-10%);transform:scale(0.8) translateY(-15%) translateX(-10%)}.uv-scale-top-right.uv-is-hidden{opacity:0;-moz-transform:scale(0.8) translateY(-15%) translateX(10%);-ms-transform:scale(0.8) translateY(-15%) translateX(10%);-webkit-transform:scale(0.8) translateY(-15%) translateX(10%);transform:scale(0.8) translateY(-15%) translateX(10%)}.uv-scale-bottom.uv-is-hidden{opacity:0;-moz-transform:scale(0.8) translateY(15%);-ms-transform:scale(0.8) translateY(15%);-webkit-transform:scale(0.8) translateY(15%);transform:scale(0.8) translateY(15%)}.uv-scale-bottom-left.uv-is-hidden{opacity:0;-moz-transform:scale(0.8) translateY(15%) translateX(-10%);-ms-transform:scale(0.8) translateY(15%) translateX(-10%);-webkit-transform:scale(0.8) translateY(15%) translateX(-10%);transform:scale(0.8) translateY(15%) translateX(-10%)}.uv-scale-bottom-right.uv-is-hidden{opacity:0;-moz-transform:scale(0.8) translateY(15%) translateX(10%);-ms-transform:scale(0.8) translateY(15%) translateX(10%);-webkit-transform:scale(0.8) translateY(15%) translateX(10%);transform:scale(0.8) translateY(15%) translateX(10%)}.uv-scale-right.uv-is-hidden{opacity:0;-moz-transform:scale(0.8) translateX(15%);-ms-transform:scale(0.8) translateX(15%);-webkit-transform:scale(0.8) translateX(15%);transform:scale(0.8) translateX(15%)}.uv-scale-right-top.uv-is-hidden{opacity:0;-moz-transform:scale(0.8) translateX(15%) translateY(-10%);-ms-transform:scale(0.8) translateX(15%) translateY(-10%);-webkit-transform:scale(0.8) translateX(15%) translateY(-10%);transform:scale(0.8) translateX(15%) translateY(-10%)}.uv-scale-right-bottom.uv-is-hidden{opacity:0;-moz-transform:scale(0.8) translateX(15%) translateY(10%);-ms-transform:scale(0.8) translateX(15%) translateY(10%);-webkit-transform:scale(0.8) translateX(15%) translateY(10%);transform:scale(0.8) translateX(15%) translateY(10%)}.uv-scale-left.uv-is-hidden{opacity:0;-moz-transform:scale(0.8) translateX(-15%);-ms-transform:scale(0.8) translateX(-15%);-webkit-transform:scale(0.8) translateX(-15%);transform:scale(0.8) translateX(-15%)}.uv-scale-left-top.uv-is-hidden{opacity:0;-moz-transform:scale(0.8) translateX(-15%) translateY(-10%);-ms-transform:scale(0.8) translateX(-15%) translateY(-10%);-webkit-transform:scale(0.8) translateX(-15%) translateY(-10%);transform:scale(0.8) translateX(-15%) translateY(-10%)}.uv-scale-left-bottom.uv-is-hidden{opacity:0;-moz-transform:scale(0.8) translateX(-15%) translateY(10%);-ms-transform:scale(0.8) translateX(-15%) translateY(10%);-webkit-transform:scale(0.8) translateX(-15%) translateY(10%);transform:scale(0.8) translateX(-15%) translateY(10%)}.uv-slide-top.uv-is-hidden{-moz-transform:translateY(-100%);-ms-transform:translateY(-100%);-webkit-transform:translateY(-100%);transform:translateY(-100%)}.uv-slide-bottom.uv-is-hidden{-moz-transform:translateY(100%);-ms-transform:translateY(100%);-webkit-transform:translateY(100%);transform:translateY(100%)}.uv-slide-left.uv-is-hidden{-moz-transform:translateX(-100%);-ms-transform:translateX(-100%);-webkit-transform:translateX(-100%);transform:translateX(-100%)}.uv-slide-right.uv-is-hidden{-moz-transform:translateX(100%);-ms-transform:translateX(100%);-webkit-transform:translateX(100%);transform:translateX(100%)}</style>
</head>
<body>
<div class="APCSS_wrapper"></div>
<nav class="navbar navbar-default navbar-fixed-top APCSS_header" role="navigation">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="navbar-header">
                    <!-- desktop version -->
                    <h1 class="accupass">
                        <a class="navbar-brand hidden-xs hidden-sm" href="index2.jsp" target="_self">
                            <img src="./icon/supermen.png" style="position: relative; width: 125px; height: 30px;">
                        </a>
                    </h1>
                    <form name="search" action="SearchEventServlet2" method="post" class="apcss-search-form ng-pristine ng-valid">
                        <div class="apcss-search-form-wrapper">
                            <input type="text" name="keyword" class="apcss-search-form-input" style="color:#FFFFFF" placeholder="找任務" onfocus="if(this.placeholder=='找任務'){this.placeholder=''};this.style.color='#FFFFFF';" onblur="if(this.placeholder==''||this.placeholder=='找任務'){this.placeholder='找任務';this.style.color='#FFFFFF';}">
                            <span class="apcss-search-form-button apcss-search-focus">
                            	<button type="submit" class="btn btn-primary ng-scope" style="position: relative; width: 38px; height: 38px; top: -6.5px; right: 12px;"><img src="./icon/search.png" style="position: relative; top: 0px; right: 1px;" width="14" height="14"></button>
                            </span>
                        </div>
                    </form>
                    <div class="dropdown apcss-dropdown-explore-activity" ng-class="hover">
                        <a class="apcss-dropdown-explore-activity-button" role="button" ng-mouseover="hover=&#39;open&#39;" ng-mouseleave="hover=&#39;&#39;" target="_blank">
                            <span>探索任務</span><img src="./icon/triangle.png" style="position: relative; top: -1px;">
                        </a>
                        <ul class="dropdown-menu apcss-dropdown-explore-activity-menu" ng-mouseleave="hover=&#39;&#39;" role="menu">
    
    <li role="presentation" class="dropdown-header">熱門主打</li>
        <li role="presentation">
            <a ga-click="Hit_1" role="menuitem" tabindex="-1" href="http://www.yuntech.edu.tw/" title="五月跑步約起來" target="_blank">
                <img src="./icon/banner1.jpg">
            </a>
        </li>
        <li role="presentation">
            <a ga-click="Hit_2" role="menuitem" tabindex="-1" href="http://www.yuntech.edu.tw/" title="小翼貓的一天" target="_blank">
                <img src="./icon/banner2.jpg">
            </a>
        </li>
        <li role="presentation" class="dropdown-header">時間</li>
        <li role="presentation">
            <div class="row">
            	<a class="col-md-3 menuitem" href="SearchEventServlet2?keyword=<%=GlobalVariable.keyword %>&date=1" title="今日" target="_self">今日</a>
              	<a class="col-md-3 menuitem" href="SearchEventServlet2?keyword=<%=GlobalVariable.keyword %>&date=2" title="明日" target="_self">明日</a>
             	<a class="col-md-3 menuitem" href="SearchEventServlet2?keyword=<%=GlobalVariable.keyword %>&date=3" title="本週" target="_self">本週</a>
               	<a class="col-md-3 menuitem" href="SearchEventServlet2?keyword=<%=GlobalVariable.keyword %>&date=4" title="本月" target="_self">本月</a>
            </div>
        </li>
</ul>
                    </div>
                </div>
                <div class="collapse navbar-collapse" id="APJS_id-userOpt">
                    <div class="apcss-userinfo-box navbar-right hidden-xs hidden-sm">
                        <div id="APJS_spinjs" class="APCSS_spinner" ap-page-load=""><div class="rect1"></div><div class="rect2"></div><div class="rect3"></div><div class="rect4"></div><div class="rect5"></div></div>
                            <div class="dropdown">
                                <a class="apcss-dropdown-user-button" id="APJS_dropdown-user" role="button" data-toggle="dropdown" data-target="#">
                                    <div class="apcss-avatar">
                                        <img src="./createEvent_files/default_user.png">
                                    </div>
                                    <img src="./icon/triangle.png" style="position: relative; left: 6px; top: -1px;">
                                </a>
                                <ul class="dropdown-menu" role="menu" aria-labelledby="APJS_dropdown-user">
                                    <li role="presentation" class="dropdown-header">參加任務</li>
                                    <li role="presentation"><a ga-click="User" role="menuitem" tabindex="-1" href="MyDetailServlet" target="_self">我的個人頁面</a></li>
                                    <li role="presentation"><a ga-click="My Tickets" role="menuitem" tabindex="-1" href="MyTicketServlet" target="_self">我的票劵</a></li>
                                    <li role="presentation"><a ga-click="My Tickets" role="menuitem" tabindex="-1" href="mySport.jsp" target="_self">我的運動記錄</a></li>
                                    <li role="presentation"><a ga-click="My Tickets" role="menuitem" tabindex="-1" href="myAchievement.jsp" target="_self">我的成就記錄</a></li>
                                    <li role="presentation" class="divider"></li>
                                    <li role="presentation" class="dropdown-header">主辦任務</li>
                                    <li role="presentation"><a ga-click="My Events" role="menuitem" tabindex="-1" href="ListEventServlet" target="_self">我登記的任務</a></li>
                                    <li role="presentation" class="divider"></li>
                                    <li role="presentation" class="dropdown-header">帳號設定</li>
                                    <li role="presentation"><a ga-click="Edit My Profile" role="menuitem" tabindex="-1" href="UpdateProfileServlet" target="_self">修改個人資料</a></li>
                                    <li role="presentation" class="divider"></li>
									<li role="presentation"><a ga-click="Log Out" role="menuitem" tabindex="-1" href="index.jsp" target="_self">登出</a></li>
								</ul>
                        	</div>
                        	<a ga-click="Create_events" class="apcss-create-activity-button" href="ListEventServlet" target="_self">新任務</a>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </nav>
<div class="APCSS_page-eventEdit hidden-xs hidden-sm">
<form name="reg" action="ModifyEventServlet" method="post" role="form" class="ng-pristine ng-valid">
 <nav class="navbar navbar-default" role="navigation">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="pull-right eventEdit">
                	<button type="button" class="btn btn-primary ng-scope" onClick="check()">儲存並發佈</button>
                	<input type="hidden" name="no" value='<%=event.getEventNo() %>'/>
                </div>
                <div class="collapse in navbar-collapse">
                    <ul class="nav navbar-nav">
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div>
        </div>
    </div>
</nav>

    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <input type="text" name="head" value="<%=event.getEventHead() %>" id="APJS_eventName" class="form-control eventEdit ng-pristine ng-valid" validator="[required]" ap-focus="">
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <figure class="APCSS_eventBannerBox">
                    <img style="display: block" id="upload_org_code_img" src="./upload/<%=event.getEventImage() %>" width="880" height="440">
                    <a class="uploadImg"><input type="file" name="upload_org_code" id="upload_org_code" ></a>
                    <input type="hidden" name="upload_org_code_name" id="upload_org_code_name" onclick="show(event_img)">
                </figure>
                <div class="APCSS_nav-eventRegisterBox"></div>
            </div>
        </div>
    </div>
    <div class="background-gray APCSS_eventInfo">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-8 col-md-8">
                    <div class="form-group">
                        <label for="APJS_id-eventStart">開始時間*</label>
                        <div class="row">
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <input name="startDate" value="<%=event.getEventStartDate() %>" id="APJS_id-eventStart" bs-datepicker="" start-date="+0d" end-date="2050-12-31" ng-model="event.start.date" type="text" validator="[required]" class="form-control ng-dirty ng-valid-event.start.date ng-valid ng-valid-date" placeholder="選擇日期">
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6 bootstrap-timepicker">
                                <input name="startTime" value="<%=event.getEventStartTime() %>" ng-model="event.start.time" type="text" validator="[required]" class="form-control ng-dirty ng-valid-event.start.time ng-valid ng-valid-time" bs-timepicker="" ap-start-time=",," ng-disabled="!event.start.date" placeholder="輸入時間" data-toggle="timepicker" disabled="disabled">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="APJS_id-eventEnd">結束時間*</label>
                        <div class="row">
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <input name="endDate" value="<%=event.getEventEndDate() %>" id="APJS_id-eventEnd" bs-datepicker="" start-date="" ng-model="event.end.date" type="text" validator="[required]" class="form-control ng-valid ng-dirty ng-valid-date ng-valid-event.end.date" placeholder="選擇日期">
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6 bootstrap-timepicker">
                                <input name="endTime" value="<%=event.getEventEndTime() %>" ng-model="event.end.time" type="text" validator="[required]" class="form-control ng-dirty ng-valid-event.end.time ng-valid ng-valid-time" bs-timepicker="" ap-end-time=",," ng-disabled="!event.end.date" placeholder="輸入時間" data-toggle="timepicker" disabled="disabled">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="APJS_id-timeZone">任務時區*</label>
                        <select name="timeZone" id="APJS_id-timeZone" class="form-control ng-pristine ng-valid" validator="[required]" validator-error="請選擇活動時區">
                                <option value="1" data-timezone="-12" <%= (event.getEventTimeZone().equals("1") ? "selected='selected'" : "") %>>
                                    (GMT-12:00) 國際換日線以西
                                </option>
                                <option value="2" data-timezone="-11" <%= (event.getEventTimeZone().equals("2") ? "selected='selected'" : "") %>>
                                    (GMT-11:00) 國際標準時間-11
                                </option>
                                <option value="3" data-timezone="-10" <%= (event.getEventTimeZone().equals("3") ? "selected='selected'" : "") %>>
                                    (GMT-10:00) 夏威夷
                                </option>
                                <option value="4" data-timezone="-9" <%= (event.getEventTimeZone().equals("4") ? "selected='selected'" : "") %>>
                                    (GMT-09:00) 阿拉斯加
                                </option>
                                <option value="5" data-timezone="-8" <%= (event.getEventTimeZone().equals("5") ? "selected='selected'" : "") %>>
                                    (GMT-08:00) 太平洋時間 (美國和加拿大)
                                </option>
                                <option value="6" data-timezone="-8" <%= (event.getEventTimeZone().equals("6") ? "selected='selected'" : "") %>>
                                    (GMT-08:00) 下加利福尼亞
                                </option>
                                <option value="7" data-timezone="-7" <%= (event.getEventTimeZone().equals("7") ? "selected='selected'" : "") %>>
                                    (GMT-07:00) 山區時間 (美國和加拿大)
                                </option>
                                <option value="8" data-timezone="-7" <%= (event.getEventTimeZone().equals("8") ? "selected='selected'" : "") %>>
                                    (GMT-07:00) 齊驊華，拉帕茲，馬札特蘭
                                </option>
                                <option value="9" data-timezone="-7" <%= (event.getEventTimeZone().equals("9") ? "selected='selected'" : "") %>>
                                    (GMT-07:00) 亞歷桑那
                                </option>
                                <option value="10" data-timezone="-6" <%= (event.getEventTimeZone().equals("10") ? "selected='selected'" : "") %>>
                                    (GMT-06:00) 薩克其萬 (加拿大)
                                </option>
                                <option value="11" data-timezone="-6" <%= (event.getEventTimeZone().equals("11") ? "selected='selected'" : "") %>>
                                    (GMT-06:00) 中美洲
                                </option>
                                <option value="12" data-timezone="-6" <%= (event.getEventTimeZone().equals("12") ? "selected='selected'" : "") %>>
                                    (GMT-06:00) 中部時間 (美國和加拿大)
                                </option>
                                <option value="13" data-timezone="-6" <%= (event.getEventTimeZone().equals("13") ? "selected='selected'" : "") %>>
                                    (GMT-06:00) 瓜達拉加若，墨西哥城，蒙特利
                                </option>
                                <option value="14" data-timezone="-5" <%= (event.getEventTimeZone().equals("14") ? "selected='selected'" : "") %>>
                                    (GMT-05:00) 東部時間 (美國和加拿大)
                                </option>
                                <option value="15" data-timezone="-5" <%= (event.getEventTimeZone().equals("15") ? "selected='selected'" : "") %>>
                                    (GMT-05:00) 波哥大，利馬，基多
                                </option>
                                <option value="16" data-timezone="-5" <%= (event.getEventTimeZone().equals("16") ? "selected='selected'" : "") %>>
                                    (GMT-05:00) 印第安納 (東部)
                                </option>
                                <option value="17" data-timezone="-4.5" <%= (event.getEventTimeZone().equals("17") ? "selected='selected'" : "") %>>
                                    (GMT-04:30) 卡拉卡斯
                                </option>
                                <option value="18" data-timezone="-4" <%= (event.getEventTimeZone().equals("18") ? "selected='selected'" : "") %>>
                                    (GMT-04:00) 大西洋時間 (加拿大)
                                </option>
                                <option value="19" data-timezone="-4" <%= (event.getEventTimeZone().equals("19") ? "selected='selected'" : "") %>>
                                    (GMT-04:00) 古雅巴
                                </option>
                                <option value="20" data-timezone="-4" <%= (event.getEventTimeZone().equals("20") ? "selected='selected'" : "") %>>
                                    (GMT-04:00) 聖地牙哥
                                </option>
                                <option value="21" data-timezone="-4" <%= (event.getEventTimeZone().equals("21") ? "selected='selected'" : "") %>>
                                    (GMT-04:00) 亞松森
                                </option>
                                <option value="22" data-timezone="-4" <%= (event.getEventTimeZone().equals("22") ? "selected='selected'" : "") %>>
                                    (GMT-04:00) 佐治敦，拉帕茲，瑪瑙斯，聖胡安
                                </option>
                                <option value="23" data-timezone="-3.5" <%= (event.getEventTimeZone().equals("23") ? "selected='selected'" : "") %>>
                                    (GMT-03:30) 紐芬蘭
                                </option>
                                <option value="24" data-timezone="-3" <%= (event.getEventTimeZone().equals("24") ? "selected='selected'" : "") %>>
                                    (GMT-03:00) 布宜諾斯艾利斯
                                </option>
                                <option value="25" data-timezone="-3" <%= (event.getEventTimeZone().equals("25") ? "selected='selected'" : "") %>>
                                    (GMT-03:00) 薩爾瓦多
                                </option>
                                <option value="26" data-timezone="-3" <%= (event.getEventTimeZone().equals("26") ? "selected='selected'" : "") %>>
                                    (GMT-03:00) 巴西利亞
                                </option>
                                <option value="27" data-timezone="-3" <%= (event.getEventTimeZone().equals("27") ? "selected='selected'" : "") %>>
                                    (GMT-03:00) 格陵蘭
                                </option>
                                <option value="28" data-timezone="-3" <%= (event.getEventTimeZone().equals("28") ? "selected='selected'" : "") %>>
                                    (GMT-03:00) 蒙特維多
                                </option>
                                <option value="29" data-timezone="-3" <%= (event.getEventTimeZone().equals("29") ? "selected='selected'" : "") %>>
                                    (GMT-03:00) 開雲，福塔力沙
                                </option>
                                <option value="30" data-timezone="-2" <%= (event.getEventTimeZone().equals("30") ? "selected='selected'" : "") %>>
                                    (GMT-02:00) 大西洋中部
                                </option>
                                <option value="31" data-timezone="-2" <%= (event.getEventTimeZone().equals("31") ? "selected='selected'" : "") %>>
                                    (GMT-02:00) 國際標準時間-02
                                </option>
                                <option value="32" data-timezone="-1" <%= (event.getEventTimeZone().equals("32") ? "selected='selected'" : "") %>>
                                    (GMT-01:00) 亞速爾群島
                                </option>
                                <option value="33" data-timezone="-1" <%= (event.getEventTimeZone().equals("33") ? "selected='selected'" : "") %>>
                                    (GMT-01:00) 維德角群島
                                </option>
                                <option value="34" data-timezone="0" <%= (event.getEventTimeZone().equals("34") ? "selected='selected'" : "") %>>
                                    (GMT) 都柏林，愛丁堡，里斯本，倫敦
                                </option>
                                <option value="35" data-timezone="0" <%= (event.getEventTimeZone().equals("35") ? "selected='selected'" : "") %>>
                                    (GMT) 蒙羅維亞，雷克雅維克
                                </option>
                                <option value="36" data-timezone="0" <%= (event.getEventTimeZone().equals("36") ? "selected='selected'" : "") %>>
                                    (GMT) 卡薩布蘭卡
                                </option>
                                <option value="37" data-timezone="0" <%= (event.getEventTimeZone().equals("37") ? "selected='selected'" : "") %>>
                                    (GMT) 國際標准時間
                                </option>
                                <option value="38" data-timezone="1" <%= (event.getEventTimeZone().equals("38") ? "selected='selected'" : "") %>>
                                    (GMT+01:00) 貝爾格勒，布拉提斯拉瓦，布達佩斯，盧布亞納，布拉格
                                </option>
                                <option value="39" data-timezone="1" <%= (event.getEventTimeZone().equals("39") ? "selected='selected'" : "") %>>
                                    (GMT+01:00) 塞拉耶佛，斯高彼亞，華沙，札格雷布
                                </option>
                                <option value="40" data-timezone="1" <%= (event.getEventTimeZone().equals("40") ? "selected='selected'" : "") %>>
                                    (GMT+01:00) 溫吐克
                                </option>
                                <option value="41" data-timezone="1" <%= (event.getEventTimeZone().equals("41") ? "selected='selected'" : "") %>>
                                    (GMT+01:00) 布魯塞爾，哥本哈根，馬德里，巴黎
                                </option>
                                <option value="42" data-timezone="1" <%= (event.getEventTimeZone().equals("42") ? "selected='selected'" : "") %>>
                                    (GMT+01:00) 中西非
                                </option>
                                <option value="43" data-timezone="1" <%= (event.getEventTimeZone().equals("43") ? "selected='selected'" : "") %>>
                                    (GMT+01:00) 阿姆斯特丹，柏林，伯恩，羅馬，斯德哥爾摩，維也納
                                </option>
                                <option value="44" data-timezone="2" <%= (event.getEventTimeZone().equals("44") ? "selected='selected'" : "") %>>
                                    (GMT+02:00) 東歐
                                </option>
                                <option value="45" data-timezone="2" <%= (event.getEventTimeZone().equals("45") ? "selected='selected'" : "") %>>
                                    (GMT+02:00) 開羅
                                </option>
                                <option value="46" data-timezone="2" <%= (event.getEventTimeZone().equals("46") ? "selected='selected'" : "") %>>
                                    (GMT+02:00) 赫爾辛基，凱耶夫，里加，蘇非亞，塔林，維爾紐斯
                                </option>
                                <option value="47" data-timezone="2" <%= (event.getEventTimeZone().equals("47") ? "selected='selected'" : "") %>>
                                    (GMT+02:00) 雅典，布加勒斯特
                                </option>
                                <option value="48" data-timezone="2" <%= (event.getEventTimeZone().equals("48") ? "selected='selected'" : "") %>>
                                    (GMT+02:00) 耶路撒冷
                                </option>
                                <option value="49" data-timezone="2" <%= (event.getEventTimeZone().equals("49") ? "selected='selected'" : "") %>>
                                    (GMT+02:00) 的黎波里
                                </option>
                                <option value="50" data-timezone="2" <%= (event.getEventTimeZone().equals("50") ? "selected='selected'" : "") %>>
                                    (GMT+02:00) 貝魯特
                                </option>
                                <option value="51" data-timezone="2" <%= (event.getEventTimeZone().equals("51") ? "selected='selected'" : "") %>>
                                    (GMT+02:00) 哈拉雷，皮托里
                                </option>
                                <option value="52" data-timezone="2" <%= (event.getEventTimeZone().equals("52") ? "selected='selected'" : "") %>>
                                    (GMT+02:00) 大馬士革
                                </option>
                                <option value="53" data-timezone="2" <%= (event.getEventTimeZone().equals("53") ? "selected='selected'" : "") %>>
                                    (GMT+02:00) 伊斯坦堡
                                </option>
                                <option value="54" data-timezone="3" <%= (event.getEventTimeZone().equals("54") ? "selected='selected'" : "") %>>
                                    (GMT+03:00) 科威特，利雅德
                                </option>
                                <option value="55" data-timezone="3" <%= (event.getEventTimeZone().equals("55") ? "selected='selected'" : "") %>>
                                    (GMT+03:00) 巴格達
                                </option>
                                <option value="56" data-timezone="3" <%= (event.getEventTimeZone().equals("56") ? "selected='selected'" : "") %>>
                                    (GMT+03:00) 奈洛比
                                </option>
                                <option value="57" data-timezone="3" <%= (event.getEventTimeZone().equals("57") ? "selected='selected'" : "") %>>
                                    (GMT+03:00) 安曼
                                </option>
                                <option value="58" data-timezone="3" <%= (event.getEventTimeZone().equals("58") ? "selected='selected'" : "") %>>
                                    (GMT+03:00) 卡里寧格勒，明斯克
                                </option>
                                <option value="59" data-timezone="3.5" <%= (event.getEventTimeZone().equals("59") ? "selected='selected'" : "") %>>
                                    (GMT+03:30) 德黑蘭
                                </option>
                                <option value="60" data-timezone="4" <%= (event.getEventTimeZone().equals("60") ? "selected='selected'" : "") %>>
                                    (GMT+04:00) 阿布達比，馬斯喀特
                                </option>
                                <option value="61" data-timezone="4" <%= (event.getEventTimeZone().equals("61") ? "selected='selected'" : "") %>>
                                    (GMT+04:00) 巴庫
                                </option>
                                <option value="62" data-timezone="4" <%= (event.getEventTimeZone().equals("62") ? "selected='selected'" : "") %>>
                                    (GMT+04:00) 葉里溫
                                </option>
                                <option value="63" data-timezone="4" <%= (event.getEventTimeZone().equals("63") ? "selected='selected'" : "") %>>
                                    (GMT+04:00) 第比利斯
                                </option>
                                <option value="64" data-timezone="4" <%= (event.getEventTimeZone().equals("64") ? "selected='selected'" : "") %>>
                                    (GMT+04:00) 路易士港
                                </option>
                                <option value="65" data-timezone="4" <%= (event.getEventTimeZone().equals("65") ? "selected='selected'" : "") %>>
                                    (GMT+04:00) 莫斯科，聖彼得堡，伏爾加格勒
                                </option>
                                <option value="66" data-timezone="4.5" <%= (event.getEventTimeZone().equals("66") ? "selected='selected'" : "") %>>
                                    (GMT+04:30) 喀布爾
                                </option>
                                <option value="67" data-timezone="5" <%= (event.getEventTimeZone().equals("67") ? "selected='selected'" : "") %>>
                                    (GMT+05:00) 伊斯蘭馬巴德，克洛奇
                                </option>
                                <option value="68" data-timezone="5" <%= (event.getEventTimeZone().equals("68") ? "selected='selected'" : "") %>>
                                    (GMT+05:00) 阿什哈巴德，塔什干
                                </option>
                                <option value="69" data-timezone="5.5" <%= (event.getEventTimeZone().equals("69") ? "selected='selected'" : "") %>>
                                    (GMT+05:30) 辰內，加爾各答，孟拜，新德里
                                </option>
                                <option value="70" data-timezone="5.5" <%= (event.getEventTimeZone().equals("70") ? "selected='selected'" : "") %>>
                                    (GMT+05:30) 斯里哈亞華登尼普拉
                                </option>
                                <option value="71" data-timezone="5.75" <%= (event.getEventTimeZone().equals("71") ? "selected='selected'" : "") %>>
                                    (GMT+05:45) 加德滿都
                                </option>
                                <option value="72" data-timezone="6" <%= (event.getEventTimeZone().equals("72") ? "selected='selected'" : "") %>>
                                    (GMT+06:00) 達卡
                                </option>
                                <option value="73" data-timezone="6" <%= (event.getEventTimeZone().equals("73") ? "selected='selected'" : "") %>>
                                    (GMT+06:00) 阿斯坦納
                                </option>
                                <option value="74" data-timezone="6" <%= (event.getEventTimeZone().equals("74") ? "selected='selected'" : "") %>>
                                    (GMT+06:00) 伊卡特林堡
                                </option>
                                <option value="75" data-timezone="6.5" <%= (event.getEventTimeZone().equals("75") ? "selected='selected'" : "") %>>
                                    (GMT+06:30) 仰光
                                </option>
                                <option value="76" data-timezone="7" <%= (event.getEventTimeZone().equals("76") ? "selected='selected'" : "") %>>
                                    (GMT+07:00) 諾曼斯比爾斯科
                                </option>
                                <option value="77" data-timezone="7" <%= (event.getEventTimeZone().equals("77") ? "selected='selected'" : "") %>>
                                    (GMT+07:00) 曼谷，河內，雅加達
                                </option>
                                <option value="78" data-timezone="8" <%= (event.getEventTimeZone().equals("78") ? "selected='selected'" : "") %>>
                                    (GMT+08:00) 台北
                                </option>
                                <option value="79" data-timezone="8" <%= (event.getEventTimeZone().equals("79") ? "selected='selected'" : "") %>>
                                    (GMT+08:00) 克拉斯諾亞爾斯克
                                </option>
                                <option value="80" data-timezone="8" <%= (event.getEventTimeZone().equals("80") ? "selected='selected'" : "") %>>
                                    (GMT+08:00) 吉隆坡，新加坡
                                </option>
                                <option value="81" data-timezone="8" <%= (event.getEventTimeZone().equals("81") ? "selected='selected'" : "") %>>
                                    (GMT+08:00) 北京，重慶，香港特別行政區，烏魯木齊
                                </option>
                                <option value="82" data-timezone="8" <%= (event.getEventTimeZone().equals("82") ? "selected='selected'" : "") %>>
                                    (GMT+08:00) 庫倫
                                </option>
                                <option value="83" data-timezone="8" <%= (event.getEventTimeZone().equals("83") ? "selected='selected'" : "") %>>
                                    (GMT+08:00) 伯斯
                                </option>
                                <option value="84" data-timezone="9" <%= (event.getEventTimeZone().equals("84") ? "selected='selected'" : "") %>>
                                    (GMT+09:00) 首爾
                                </option>
                                <option value="85" data-timezone="9" <%= (event.getEventTimeZone().equals("85") ? "selected='selected'" : "") %>>
                                    (GMT+09:00) 伊爾庫次克
                                </option>
                                <option value="86" data-timezone="9" <%= (event.getEventTimeZone().equals("86") ? "selected='selected'" : "") %>>
                                    (GMT+09:00) 大阪，北海道，東京
                                </option>
                                <option value="87" data-timezone="9.5" <%= (event.getEventTimeZone().equals("87") ? "selected='selected'" : "") %>>
                                    (GMT+09:30) 達爾文
                                </option>
                                <option value="88" data-timezone="9.5" <%= (event.getEventTimeZone().equals("88") ? "selected='selected'" : "") %>>
                                    (GMT+09:30) 愛德蘭
                                </option>
                                <option value="89" data-timezone="10" <%= (event.getEventTimeZone().equals("89") ? "selected='selected'" : "") %>>
                                    (GMT+10:00) 坎培拉，墨爾本，雪梨
                                </option>
                                <option value="90" data-timezone="10" <%= (event.getEventTimeZone().equals("90") ? "selected='selected'" : "") %>>
                                    (GMT+10:00) 布里斯本
                                </option>
                                <option value="91" data-timezone="10" <%= (event.getEventTimeZone().equals("91") ? "selected='selected'" : "") %>>
                                    (GMT+10:00) 霍巴特
                                </option>
                                <option value="92" data-timezone="10" <%= (event.getEventTimeZone().equals("92") ? "selected='selected'" : "") %>>
                                    (GMT+10:00) 關島，莫爾斯貝港
                                </option>
                                <option value="93" data-timezone="10" <%= (event.getEventTimeZone().equals("93") ? "selected='selected'" : "") %>>
                                    (GMT+10:00) 亞庫茲克
                                </option>
                                <option value="94" data-timezone="11" <%= (event.getEventTimeZone().equals("94") ? "selected='selected'" : "") %>>
                                    (GMT+11:00) 索羅門群島，新喀里多尼亞群島
                                </option>
                                <option value="95" data-timezone="11" <%= (event.getEventTimeZone().equals("95") ? "selected='selected'" : "") %>>
                                    (GMT+11:00) 海參威
                                </option>
                                <option value="96" data-timezone="12" <%= (event.getEventTimeZone().equals("96") ? "selected='selected'" : "") %>>
                                    (GMT+12:00) 斐濟
                                </option>
                                <option value="97" data-timezone="12" <%= (event.getEventTimeZone().equals("97") ? "selected='selected'" : "") %>>
                                    (GMT+12:00) 彼得保羅夫斯克-堪察加斯克
                                </option>
                                <option value="98" data-timezone="12" <%= (event.getEventTimeZone().equals("98") ? "selected='selected'" : "") %>>
                                    (GMT+12:00) 馬加丹
                                </option>
                                <option value="99" data-timezone="12" <%= (event.getEventTimeZone().equals("99") ? "selected='selected'" : "") %>>
                                    (GMT+12:00) 奧克蘭，威靈頓
                                </option>
                                <option value="100" data-timezone="12" <%= (event.getEventTimeZone().equals("100") ? "selected='selected'" : "") %>>
                                    (GMT+12:00) 國際標準時間+12
                                </option>
                                <option value="101" data-timezone="13" <%= (event.getEventTimeZone().equals("101") ? "selected='selected'" : "") %>>
                                    (GMT+13:00) 薩摩亞
                                </option>
                                <option value="102" data-timezone="13" <%= (event.getEventTimeZone().equals("102") ? "selected='selected'" : "") %>>
                                    (GMT+13:00) 努瓜婁發
                                </option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="APJS_id-location">地址*</label>
                        <select name="area" id="APJS_id-category" class="form-control ng-pristine ng-valid">
                            	<option value="none" selected="selected">請選擇地區</option>
                                <option value="1" <%= (event.getEventArea().equals("1") ? "selected='selected'" : "") %>>台北</option>
                                <option value="2" <%= (event.getEventArea().equals("2") ? "selected='selected'" : "") %>>新北</option>
                                <option value="3" <%= (event.getEventArea().equals("3") ? "selected='selected'" : "") %>>桃園</option>
                                <option value="4" <%= (event.getEventArea().equals("4") ? "selected='selected'" : "") %>>新竹</option>
                                <option value="5" <%= (event.getEventArea().equals("5") ? "selected='selected'" : "") %>>苗栗</option>
                                <option value="6" <%= (event.getEventArea().equals("6") ? "selected='selected'" : "") %>>台中</option>
                                <option value="7" <%= (event.getEventArea().equals("7") ? "selected='selected'" : "") %>>南投</option>
                                <option value="8" <%= (event.getEventArea().equals("8") ? "selected='selected'" : "") %>>彰化</option>
                                <option value="9" <%= (event.getEventArea().equals("9") ? "selected='selected'" : "") %>>雲林</option>
                                <option value="10" <%= (event.getEventArea().equals("10") ? "selected='selected'" : "") %>>嘉義</option>
                                <option value="11" <%= (event.getEventArea().equals("11") ? "selected='selected'" : "") %>>台南</option>
                                <option value="12" <%= (event.getEventArea().equals("12") ? "selected='selected'" : "") %>>高雄</option>
                                <option value="13" <%= (event.getEventArea().equals("13") ? "selected='selected'" : "") %>>屏東</option>
                                <option value="14" <%= (event.getEventArea().equals("14") ? "selected='selected'" : "") %>>宜花東</option>
                                <option value="15" <%= (event.getEventArea().equals("15") ? "selected='selected'" : "") %>>基隆</option>
                                <option value="16" <%= (event.getEventArea().equals("16") ? "selected='selected'" : "") %>>香港</option>
                                <option value="17" <%= (event.getEventArea().equals("17") ? "selected='selected'" : "") %>>其他</option>
                        </select>
                        <p></p>
                        <input type="text" name="address" value="<%=event.getEventAddress() %>" id="APJS_id-location" validator="[required]" class="form-control ng-pristine ng-valid">
                        <input type="text" name="address2" value="<%=event.getEventAddress2() %>" class="form-control addressOther ng-pristine ng-valid">
                    </div>
                    <div class="form-group">
                        <label for="APJS_id-location">創建人*</label>
                        <input type="text" name="organizerName" value="<%=event.getEventOrganizerName() %>" class="form-control ng-pristine ng-valid" placeholder="請輸入名稱">
                        <input type="text" name="organizerEmail" value="<%=event.getEventOrganizerEmail() %>" class="form-control addressOther ng-pristine ng-valid" placeholder="請輸入Email">
                        <input type="text" name="organizerPhone" value="<%=event.getEventOrganizerPhone() %>" class="form-control addressOther ng-pristine ng-valid" placeholder="請輸入手機" onkeyup="this.value=this.value.replace(/[^\d]/g,'')">
                    </div>
                    <div class="form-group">
                        <label for="APJS_id-category">任務類型*</label>
                        <select name="category" id="APJS_id-category" class="form-control ng-pristine ng-valid">
                            	<option value="none" selected="selected">請選擇任務類型</option>
                                <option value="1" <%= (event.getEventCategory().equals("1") ? "selected='selected'" : "") %>>有氧運動</option>
                                <option value="2" <%= (event.getEventCategory().equals("2") ? "selected='selected'" : "") %>>阻力運動</option>
                                <option value="3" <%= (event.getEventCategory().equals("3") ? "selected='selected'" : "") %>>重量訓練</option>
                                <option value="4" <%= (event.getEventCategory().equals("4") ? "selected='selected'" : "") %>>伸展運動</option>
                                <option value="5" <%= (event.getEventCategory().equals("5") ? "selected='selected'" : "") %>>氣功瑜珈</option>
                                <option value="6" <%= (event.getEventCategory().equals("6") ? "selected='selected'" : "") %>>健走與慢跑</option>
                                <option value="7" <%= (event.getEventCategory().equals("7") ? "selected='selected'" : "") %>>其他</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="APJS_id-abstract">任務摘要*</label>
                        <textarea name="summary" id="APJS_id-abstract" class="form-control ng-pristine ng-valid" validator="[required, lessThanWord]" validator-less-than="140"><%=event.getEventSummary() %></textarea>
                    </div>
                </div>
               <div id="map-canvas"></div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <h3 id="APJS_id-eventContent" class="APCSS_lineCaption"><span>任務介紹</span></h3>
                <div class="redactor-box">
                	<textarea name="content" ng-model="event.content" ap-redactor="{&quot;placeholder&quot;: &quot;請輸入你的活動介紹...&quot;}" validator="[redactorRequired]" class="ng-pristine ng-valid" dir="ltr" placeholder="請輸入你的活動介紹..." style="display: none;"><%=event.getEventContent() %></textarea>
                </div>
            </div>
        </div>
    </div>
    <div class="background-gray APCSS_eventTicket">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <h3 id="APJS_id-eventTicket" class="APCSS_lineCaption"><span>票券資訊</span></h3>
                    <div class="table-responsive">
                        <table class="table table-hover table-bordered APCSS_ticketBox ng-pristine ng-valid ui-sortable" ap-ticket-sortable="" ng-model="event.tickets">
                            <thead>
                                <tr>
                                    <th width="225">分組名稱</th>
                                    <th width="200">票券名稱</th>
                                    <th width="130">票價</th>
                                    <th width="80">張數</th>
                                    <th width="110">發售狀態</th>
                                </tr>
                            </thead>
                            <tbody class="ng-scope">
                                <tr>
                                    <td width="225" class="vhcenter ng-binding">
                                        <input type="text" name="groupName" value="<%=event.getEventGroupName() %>" class="form-control ng-pristine ng-valid" >
                                    </td>
                                    <td width="200">
                                        <div class="ticketNameTd">
                                            <input type="text" name="ticketName" value="<%=event.getEventTicketName() %>" class="form-control ng-pristine ng-valid" validator="[required]" ng-disabled="ticket.hasSale">
                                        </div>
                                    </td>
                                    <td width="130">
                                        <div class="ticketCountTd">
                                            <span class="currency ng-binding ng-scope">NT$</span>
                                            <input type="text" name="ticketPrice" value="<%=event.getEventTicketPrice() %>" class="form-control ng-scope ng-pristine ng-valid" onkeyup="this.value=this.value.replace(/[^\d]/g,'')">
                                        </div>
                                    </td>
                                    <td width="80">
                                        <input type="text" name="ticketCount" value="<%=event.getEventTicketCount() %>" class="form-control ng-pristine ng-valid" validator="[required, ticketCount]" validator-count="1" onkeyup="this.value=this.value.replace(/[^\d]/g,'')">
                                    </td>
                                    <td width="110">
                                        <select name="ticketSale" class="form-control ng-pristine ng-valid">
                                            <option value="true" <%= (event.getEventTicketSale().equals("true") ? "selected='selected'" : "") %>>發售</option>
                                            <option value="false" <%= (event.getEventTicketSale().equals("false") ? "selected='selected'" : "") %>>停售</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr class="">
                                    <td colspan="9">
                                        <div class="form-horizontal">
                                            <div class="form-group">
                                                <label for="APJS_id-sellStart" class="col-xs-2 control-label">售票時間</label>
                                                <div class="col-xs-2">
                                                    <input type="text" name="ticketStartDate" value="<%=event.getEventTicketStartDate() %>" id="APJS_id-sellStart" end-date="2015-07-04" ng-model="ticket.saleStart.date" class="form-control ng-valid ng-dirty ng-valid-date" bs-datepicker="" placeholder="選擇日期">
                                                </div>
                                                <div class="col-xs-2">
                                                    <div class="fix_symbolBox bootstrap-timepicker">
                                                        <input type="text" name="ticketStartTime" value="<%=event.getEventTicketStartTime() %>" ng-model="ticket.saleStart.time" class="form-control ng-valid ng-dirty ng-valid-time" bs-timepicker="" ap-start-time="&quot;2015-05-29T16:00:00.000Z&quot;,&quot;2015-05-29T16:00:00.000Z&quot;,4:00" placeholder="輸入時間" data-toggle="timepicker">
                                                        <span>~</span>
                                                    </div>
                                                </div>
                                                <div class="col-xs-2">
                                                    <input type="text" name="ticketEndDate" value="<%=event.getEventTicketEndDate() %>" start-date="2015-05-30" end-date="2050-12-31" ng-model="ticket.saleEnd.date" class="form-control ng-valid ng-dirty ng-valid-date" bs-datepicker="" placeholder="選擇日期">
                                                </div>
                                                <div class="col-xs-2 bootstrap-timepicker">
                                                    <input type="text" name="ticketEndTime" value="<%=event.getEventTicketEndTime() %>" ng-model="ticket.saleEnd.time" class="form-control ng-valid ng-dirty ng-valid-time" bs-timepicker="" ap-end-time="&quot;2015-05-29T16:00:00.000Z&quot;,&quot;2015-05-29T16:00:00.000Z&quot;,10:09" placeholder="輸入時間" data-toggle="timepicker">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="APJS_id-typeDescription" class="col-xs-2 control-label">票種說明</label>
                                                <div class="col-xs-10">
                                                    <textarea name="ticketDescription" id="APJS_id-typeDescription" class="form-control ng-pristine ng-valid" ng-disabled="ticket.hasSale"><%=event.getEventTicketDescription() %></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="APCSS_eventAdvanced">
        <div class="container">
            <div class="row">
                <h3 id="APJS_id-advanced" class="APCSS_lineCaption"><span>進階設定</span></h3>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="APJS_id-precautions" class="control-label">參加注意事項</label>
                        <textarea name="precaution" id="APJS_id-precautions" class="APCSS_precautions form-control ng-pristine ng-valid" rows="3"><%=event.getEventPrecaution() %></textarea>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </form>
</div>
<div class="apcss-footer">
    <div class="container">
        <div class="row apcss-footer-first-section">
            <div class="col-xs-12 col-sm-3 hidden-xs">
                <p class="apcss-footer-title">新建任務</p>
                    <a ga-click="Create Event" href="http://www.yuntech.edu.tw/" class="apcss-footer-content" target="_blank">如何開始第一個任務？</a>
                <a ga-click="FAQ" href="http://www.yuntech.edu.tw/" class="apcss-footer-content" target="_blank">常見問題</a>
            </div>
            <div class="col-xs-12 col-sm-3 hidden-xs">
                <p class="apcss-footer-title">參與任務</p>
                <a ga-click="Features" href="http://www.yuntech.edu.tw/" class="apcss-footer-content" target="_blank">功能介紹</a>
                
                <a ga-click="Event News" href="http://www.yuntech.edu.tw/" class="apcss-footer-content" target="_blank">任務新鮮事</a>
            </div>
            <div class="col-xs-12 col-sm-3 hidden-xs">
                <p class="apcss-footer-title">關於我們</p>
                <a ga-click="Join Us" href="http://www.yuntech.edu.tw/" class="apcss-footer-content" target="_blank">加入我們</a>
                <a ga-click="Terms of Service" href="http://www.yuntech.edu.tw/" class="apcss-footer-content" target="_blank">服務條款</a>
                    <a ga-click="Partner" href="http://www.yuntech.edu.tw/" class="apcss-footer-content" target="_blank">合作夥伴</a>
            </div>
            <div class="col-xs-12 col-sm-3 apcss-footer-phone-section2">
                <address>
                    <p class="apcss-footer-title">Supermen服務熱線</p>
                    <p class="apcss-footer-content">雲林斗六 : +886 1-1234-5678</p>
                    <p class="apcss-footer-content">週一至週五 09:00-18:00</p>
                    <a ga-click="Service Center" href="http://www.yuntech.edu.tw/" class="apcss-footer-customer-help" target="_blank" data-uv-lightbox="classic_widget" data-uv-mode="support" data-uv-primary-color="#0089d2" data-uv-link-color="#0089d2" data-uv-default-mode="support" data-uv-scanned="true" id="uv-1">
                        <span>客服小幫手</span>
                    </a>
                </address>
            </div>
        </div>
	    <a href="http://www.yuntech.edu.tw/" style="text-align: center; position: relative; Right: -160px; top: 0px;" class="apcss-logo" target="_blank">
	    	<h1>雲科大資管系團隊© Supermelly Inc. All Rights Reserved.</h1>
	    </a>
	</div>
</div>
</body>
</html>