﻿<%@ page language="java" import="com.supermen.servlet.controller.GlobalVariable" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html ng-app="accupass" class="js flexbox flexboxlegacy canvas canvastext webgl no-touch geolocation postmessage websqldatabase indexeddb hashchange history draganddrop websockets rgba hsla multiplebgs backgroundsize borderimage borderradius boxshadow textshadow opacity cssanimations csscolumns cssgradients cssreflections csstransforms csstransforms3d csstransitions fontface generatedcontent video audio localstorage sessionstorage webworkers applicationcache svg inlinesvg smil svgclippaths ng-scope">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"><style type="text/css">@charset "UTF-8";[ng\:cloak],[ng-cloak],[data-ng-cloak],[x-ng-cloak],.ng-cloak,.x-ng-cloak,.ng-hide{display:none !important;}ng\:form{display:block;}.ng-animate-block-transitions{transition:0s all!important;-webkit-transition:0s all!important;}.ng-hide-add-active,.ng-hide-remove{display:block!important;}</style>

    <title>Supermen</title>
	<link rel="Shortcut Icon" type="image/x-icon" href="./icon/shortcut.png" />
    <link type="text/css" rel="stylesheet" href="./register_files/css.min.css">
    <script type="application/javascript" async="" defer="" src="./searchNoEvent_files/track.js"></script><script type="text/javascript" async="" src="./searchNoEvent_files/26hI9f54AQMRNaPo1MvFkw.js"></script><script id="facebook-jssdk" src="./searchNoEvent_files/all.js"></script><script type="text/javascript" async="" src="./searchNoEvent_files/atrk.js"></script><script async="" src="./searchNoEvent_files/analytics.js"></script><script async="" src="./searchNoEvent_files/fbds.js"></script><script type="text/javascript" src="./searchNoEvent_files/js.min.js"></script>

    <script type="text/javascript" src="./searchNoEvent_files/resource"></script>
	<style type="text/css" media="screen">.uv-icon{-moz-box-sizing:border-box;-webkit-box-sizing:border-box;box-sizing:border-box;display:inline-block;cursor:pointer;position:relative;-moz-transition:all 300ms;-o-transition:all 300ms;-webkit-transition:all 300ms;transition:all 300ms;width:39px;height:39px;position:fixed;z-index:100002;opacity:0.8;-moz-transition:opacity 100ms;-o-transition:opacity 100ms;-webkit-transition:opacity 100ms;transition:opacity 100ms}.uv-icon.uv-bottom-right{bottom:10px;right:12px}.uv-icon.uv-top-right{top:10px;right:12px}.uv-icon.uv-bottom-left{bottom:10px;left:12px}.uv-icon.uv-top-left{top:10px;left:12px}.uv-icon.uv-is-selected{opacity:1}.uv-icon svg{width:39px;height:39px}.uv-popover{font-family:sans-serif;font-weight:100;font-size:13px;color:black;position:fixed;z-index:100001}.uv-popover-content{-moz-border-radius:5px;-webkit-border-radius:5px;border-radius:5px;background:white;position:relative;width:325px;height:325px;-moz-transition:background 200ms;-o-transition:background 200ms;-webkit-transition:background 200ms;transition:background 200ms}.uv-bottom .uv-popover-content{-moz-box-shadow:rgba(0,0,0,0.3) 0 -10px 60px,rgba(0,0,0,0.1) 0 0 20px;-webkit-box-shadow:rgba(0,0,0,0.3) 0 -10px 60px,rgba(0,0,0,0.1) 0 0 20px;box-shadow:rgba(0,0,0,0.3) 0 -10px 60px,rgba(0,0,0,0.1) 0 0 20px}.uv-top .uv-popover-content{-moz-box-shadow:rgba(0,0,0,0.3) 0 10px 60px,rgba(0,0,0,0.1) 0 0 20px;-webkit-box-shadow:rgba(0,0,0,0.3) 0 10px 60px,rgba(0,0,0,0.1) 0 0 20px;box-shadow:rgba(0,0,0,0.3) 0 10px 60px,rgba(0,0,0,0.1) 0 0 20px}.uv-left .uv-popover-content{-moz-box-shadow:rgba(0,0,0,0.3) 10px 0 60px,rgba(0,0,0,0.1) 0 0 20px;-webkit-box-shadow:rgba(0,0,0,0.3) 10px 0 60px,rgba(0,0,0,0.1) 0 0 20px;box-shadow:rgba(0,0,0,0.3) 10px 0 60px,rgba(0,0,0,0.1) 0 0 20px}.uv-right .uv-popover-content{-moz-box-shadow:rgba(0,0,0,0.3) -10px 0 60px,rgba(0,0,0,0.1) 0 0 20px;-webkit-box-shadow:rgba(0,0,0,0.3) -10px 0 60px,rgba(0,0,0,0.1) 0 0 20px;box-shadow:rgba(0,0,0,0.3) -10px 0 60px,rgba(0,0,0,0.1) 0 0 20px}.uv-ie8 .uv-popover-content{position:relative}.uv-ie8 .uv-popover-content .uv-popover-content-shadow{display:block;background:black;content:'';position:absolute;left:-15px;top:-15px;width:100%;height:100%;filter:progid:DXImageTransform.Microsoft.Blur(PixelRadius=15,MakeShadow=true,ShadowOpacity=0.30);z-index:-1}.uv-popover-tail{border:9px solid transparent;width:0;z-index:10;position:absolute;-moz-transition:border-top-color 200ms;-o-transition:border-top-color 200ms;-webkit-transition:border-top-color 200ms;transition:border-top-color 200ms}.uv-top .uv-popover-tail{bottom:-20px;border-top:11px solid white}.uv-bottom .uv-popover-tail{top:-20px;border-bottom:11px solid white}.uv-left .uv-popover-tail{right:-20px;border-left:11px solid white}.uv-right .uv-popover-tail{left:-20px;border-right:11px solid white}.uv-popover-loading{background:white;-moz-border-radius:5px;-webkit-border-radius:5px;border-radius:5px;position:absolute;width:100%;height:100%;left:0;top:0}.uv-popover-loading-text{position:absolute;top:50%;margin-top:-0.5em;width:100%;text-align:center}.uv-popover-iframe-container{height:100%}.uv-popover-iframe{-moz-border-radius:5px;-webkit-border-radius:5px;border-radius:5px;overflow:hidden}.uv-is-hidden{display:none}.uv-is-invisible{display:block !important;visibility:hidden !important}.uv-is-transitioning{display:block !important}.uv-no-transition{-moz-transition:none !important;-webkit-transition:none !important;-o-transition:color 0 ease-in !important;transition:none !important}.uv-fade{opacity:1;-moz-transition:opacity 200ms ease-out;-o-transition:opacity 200ms ease-out;-webkit-transition:opacity 200ms ease-out;transition:opacity 200ms ease-out}.uv-fade.uv-is-hidden{opacity:0}.uv-scale-top,.uv-scale-top-left,.uv-scale-top-right,.uv-scale-bottom,.uv-scale-bottom-left,.uv-scale-bottom-right,.uv-scale-right,.uv-scale-right-top,.uv-scale-right-bottom,.uv-scale-left,.uv-scale-left-top,.uv-scale-left-bottom,.uv-slide-top,.uv-slide-bottom,.uv-slide-left,.uv-slide-right{opacity:1;-moz-transition:all 80ms ease-out;-o-transition:all 80ms ease-out;-webkit-transition:all 80ms ease-out;transition:all 80ms ease-out}.uv-scale-top.uv-is-hidden{opacity:0;-moz-transform:scale(0.8) translateY(-15%);-ms-transform:scale(0.8) translateY(-15%);-webkit-transform:scale(0.8) translateY(-15%);transform:scale(0.8) translateY(-15%)}.uv-scale-top-left.uv-is-hidden{opacity:0;-moz-transform:scale(0.8) translateY(-15%) translateX(-10%);-ms-transform:scale(0.8) translateY(-15%) translateX(-10%);-webkit-transform:scale(0.8) translateY(-15%) translateX(-10%);transform:scale(0.8) translateY(-15%) translateX(-10%)}.uv-scale-top-right.uv-is-hidden{opacity:0;-moz-transform:scale(0.8) translateY(-15%) translateX(10%);-ms-transform:scale(0.8) translateY(-15%) translateX(10%);-webkit-transform:scale(0.8) translateY(-15%) translateX(10%);transform:scale(0.8) translateY(-15%) translateX(10%)}.uv-scale-bottom.uv-is-hidden{opacity:0;-moz-transform:scale(0.8) translateY(15%);-ms-transform:scale(0.8) translateY(15%);-webkit-transform:scale(0.8) translateY(15%);transform:scale(0.8) translateY(15%)}.uv-scale-bottom-left.uv-is-hidden{opacity:0;-moz-transform:scale(0.8) translateY(15%) translateX(-10%);-ms-transform:scale(0.8) translateY(15%) translateX(-10%);-webkit-transform:scale(0.8) translateY(15%) translateX(-10%);transform:scale(0.8) translateY(15%) translateX(-10%)}.uv-scale-bottom-right.uv-is-hidden{opacity:0;-moz-transform:scale(0.8) translateY(15%) translateX(10%);-ms-transform:scale(0.8) translateY(15%) translateX(10%);-webkit-transform:scale(0.8) translateY(15%) translateX(10%);transform:scale(0.8) translateY(15%) translateX(10%)}.uv-scale-right.uv-is-hidden{opacity:0;-moz-transform:scale(0.8) translateX(15%);-ms-transform:scale(0.8) translateX(15%);-webkit-transform:scale(0.8) translateX(15%);transform:scale(0.8) translateX(15%)}.uv-scale-right-top.uv-is-hidden{opacity:0;-moz-transform:scale(0.8) translateX(15%) translateY(-10%);-ms-transform:scale(0.8) translateX(15%) translateY(-10%);-webkit-transform:scale(0.8) translateX(15%) translateY(-10%);transform:scale(0.8) translateX(15%) translateY(-10%)}.uv-scale-right-bottom.uv-is-hidden{opacity:0;-moz-transform:scale(0.8) translateX(15%) translateY(10%);-ms-transform:scale(0.8) translateX(15%) translateY(10%);-webkit-transform:scale(0.8) translateX(15%) translateY(10%);transform:scale(0.8) translateX(15%) translateY(10%)}.uv-scale-left.uv-is-hidden{opacity:0;-moz-transform:scale(0.8) translateX(-15%);-ms-transform:scale(0.8) translateX(-15%);-webkit-transform:scale(0.8) translateX(-15%);transform:scale(0.8) translateX(-15%)}.uv-scale-left-top.uv-is-hidden{opacity:0;-moz-transform:scale(0.8) translateX(-15%) translateY(-10%);-ms-transform:scale(0.8) translateX(-15%) translateY(-10%);-webkit-transform:scale(0.8) translateX(-15%) translateY(-10%);transform:scale(0.8) translateX(-15%) translateY(-10%)}.uv-scale-left-bottom.uv-is-hidden{opacity:0;-moz-transform:scale(0.8) translateX(-15%) translateY(10%);-ms-transform:scale(0.8) translateX(-15%) translateY(10%);-webkit-transform:scale(0.8) translateX(-15%) translateY(10%);transform:scale(0.8) translateX(-15%) translateY(10%)}.uv-slide-top.uv-is-hidden{-moz-transform:translateY(-100%);-ms-transform:translateY(-100%);-webkit-transform:translateY(-100%);transform:translateY(-100%)}.uv-slide-bottom.uv-is-hidden{-moz-transform:translateY(100%);-ms-transform:translateY(100%);-webkit-transform:translateY(100%);transform:translateY(100%)}.uv-slide-left.uv-is-hidden{-moz-transform:translateX(-100%);-ms-transform:translateX(-100%);-webkit-transform:translateX(-100%);transform:translateX(-100%)}.uv-slide-right.uv-is-hidden{-moz-transform:translateX(100%);-ms-transform:translateX(100%);-webkit-transform:translateX(100%);transform:translateX(100%)}</style>
</head>
<body>
<div class="APCSS_wrapper"></div>
<nav class="navbar navbar-default navbar-fixed-top APCSS_header" role="navigation">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="navbar-header">
                    <!-- desktop version -->
                    <h1 class="accupass">
                        <a class="navbar-brand hidden-xs hidden-sm" href="index2.jsp" target="_self">
                            <img src="./icon/supermen.png" style="position: relative; width: 125px; height: 30px;">
                        </a>
                    </h1>
                    <form name="search" action="SearchEventServlet2" method="post" class="apcss-search-form ng-pristine ng-valid">
                        <div class="apcss-search-form-wrapper">
                            <input type="text" name="keyword" class="apcss-search-form-input" style="color:#FFFFFF" placeholder="找任務" onfocus="if(this.placeholder=='找任務'){this.placeholder=''};this.style.color='#FFFFFF';" onblur="if(this.placeholder==''||this.placeholder=='找任務'){this.placeholder='找任務';this.style.color='#FFFFFF';}">
                            <span class="apcss-search-form-button apcss-search-focus">
                            	<button type="submit" class="btn btn-primary ng-scope" style="position: relative; width: 38px; height: 38px; top: -6.5px; right: 12px;"><img src="./icon/search.png" style="position: relative; top: 0px; right: 1px;" width="14" height="14"></button>
                            </span>
                        </div>
                    </form>
                    <div class="dropdown apcss-dropdown-explore-activity" ng-class="hover">
                        <a class="apcss-dropdown-explore-activity-button" role="button" ng-mouseover="hover=&#39;open&#39;" ng-mouseleave="hover=&#39;&#39;" target="_blank">
                            <span>探索任務</span><img src="./icon/triangle.png" style="position: relative; top: -1px;">
                        </a>
                        <ul class="dropdown-menu apcss-dropdown-explore-activity-menu" ng-mouseleave="hover=&#39;&#39;" role="menu">
    
    <li role="presentation" class="dropdown-header">熱門主打</li>
        <li role="presentation">
            <a ga-click="Hit_1" role="menuitem" tabindex="-1" href="http://www.yuntech.edu.tw/" title="五月跑步約起來" target="_blank">
                <img src="./icon/banner1.jpg">
            </a>
        </li>
        <li role="presentation">
            <a ga-click="Hit_2" role="menuitem" tabindex="-1" href="http://www.yuntech.edu.tw/" title="小翼貓的一天" target="_blank">
                <img src="./icon/banner2.jpg">
            </a>
        </li>
        <li role="presentation" class="dropdown-header">時間</li>
        <li role="presentation">
            <div class="row">
            	<a class="col-md-3 menuitem" href="SearchEventServlet2?keyword=<%=GlobalVariable.keyword %>&date=1" title="今日" target="_self">今日</a>
              	<a class="col-md-3 menuitem" href="SearchEventServlet2?keyword=<%=GlobalVariable.keyword %>&date=2" title="明日" target="_self">明日</a>
             	<a class="col-md-3 menuitem" href="SearchEventServlet2?keyword=<%=GlobalVariable.keyword %>&date=3" title="本週" target="_self">本週</a>
               	<a class="col-md-3 menuitem" href="SearchEventServlet2?keyword=<%=GlobalVariable.keyword %>&date=4" title="本月" target="_self">本月</a>
            </div>
        </li>
</ul>
                    </div>
                </div>
                <div class="collapse navbar-collapse" id="APJS_id-userOpt">
                    <div class="apcss-userinfo-box navbar-right hidden-xs hidden-sm">
                        <div id="APJS_spinjs" class="APCSS_spinner" ap-page-load=""><div class="rect1"></div><div class="rect2"></div><div class="rect3"></div><div class="rect4"></div><div class="rect5"></div></div>
                            <div class="dropdown">
                                <a class="apcss-dropdown-user-button" id="APJS_dropdown-user" role="button" data-toggle="dropdown" data-target="#">
                                    <div class="apcss-avatar">
                                        <img src="./createEvent_files/default_user.png">
                                    </div>
                                    <img src="./icon/triangle.png" style="position: relative; left: 6px; top: -1px;">
                                </a>
                                <ul class="dropdown-menu" role="menu" aria-labelledby="APJS_dropdown-user">
                                    <li role="presentation" class="dropdown-header">參加任務</li>
                                    <li role="presentation"><a ga-click="User" role="menuitem" tabindex="-1" href="MyDetailServlet" target="_self">我的個人頁面</a></li>
                                    <li role="presentation"><a ga-click="My Tickets" role="menuitem" tabindex="-1" href="MyTicketServlet" target="_self">我的票劵</a></li>
                                    <li role="presentation"><a ga-click="My Tickets" role="menuitem" tabindex="-1" href="mySport.jsp" target="_self">我的運動記錄</a></li>
                                    <li role="presentation"><a ga-click="My Tickets" role="menuitem" tabindex="-1" href="myAchievement.jsp" target="_self">我的成就記錄</a></li>
                                    <li role="presentation" class="divider"></li>
                                    <li role="presentation" class="dropdown-header">主辦任務</li>
                                    <li role="presentation"><a ga-click="My Events" role="menuitem" tabindex="-1" href="ListEventServlet" target="_self">我登記的任務</a></li>
                                    <li role="presentation" class="divider"></li>
                                    <li role="presentation" class="dropdown-header">帳號設定</li>
                                    <li role="presentation"><a ga-click="Edit My Profile" role="menuitem" tabindex="-1" href="UpdateProfileServlet" target="_self">修改個人資料</a></li>
                                    <li role="presentation" class="divider"></li>
									<li role="presentation"><a ga-click="Log Out" role="menuitem" tabindex="-1" href="index.jsp" target="_self">登出</a></li>
								</ul>
                        	</div>
                        	<a ga-click="Create_events" class="apcss-create-activity-button" href="ListEventServlet" target="_self">新任務</a>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </nav>
<div class="APCSS_page-search" ap-search-bar="" ga-config-category="Find Event" ga-hot-tag-box="">
    <div class="APCSS_filterBox-search">
        <div class="container">
            <div class="row">
<div class="col-xs-12 col-sm-6 col-md-6" ga-hover="Search">
    <form name="reg" action="SearchEventServlet2" method="post" class="APCSS_serchBar ng-pristine ng-valid">
        <div class="input-group">
            <input type="text" name="keyword" class="form-control ng-pristine ng-valid" value="<%=GlobalVariable.keyword %>" placeholder="找活動">
            <span class="input-group-btn">
                <button type="submit" class="btn btn-primary ng-scope" style="position: relative; width: 39px; height: 34px; top: 0px; right: 0px;"><img src="./icon/search.png" style="position: relative; top: 0px; right: 1px;" width="14" height="14"></button>
            </span>
        </div>
    </form>
</div>
<div class="clearfix"></div>
<div class="col-xs-12">
    <dl class="dl-horizontal hidden-xs" ga-hover="Location">
        <dt>
            <span class="h3"><b>地區</b></span>
        </dt>
        <dd>
        	<a href="SearchEventServlet2?keyword=<%=GlobalVariable.keyword %>&area=0" <%= (GlobalVariable.area.equals("0") ? "class='active'" : "") %>>全部</a>
        	<a href="SearchEventServlet2?keyword=<%=GlobalVariable.keyword %>&area=1" <%= (GlobalVariable.area.equals("1") ? "class='active'" : "") %>>台北</a>
        	<a href="SearchEventServlet2?keyword=<%=GlobalVariable.keyword %>&area=2" <%= (GlobalVariable.area.equals("2") ? "class='active'" : "") %>>新北</a>
       		<a href="SearchEventServlet2?keyword=<%=GlobalVariable.keyword %>&area=3" <%= (GlobalVariable.area.equals("3") ? "class='active'" : "") %>>桃園</a>
       		<a href="SearchEventServlet2?keyword=<%=GlobalVariable.keyword %>&area=4" <%= (GlobalVariable.area.equals("4") ? "class='active'" : "") %>>新竹</a>
          	<a href="SearchEventServlet2?keyword=<%=GlobalVariable.keyword %>&area=5" <%= (GlobalVariable.area.equals("5") ? "class='active'" : "") %>>苗栗</a>
           	<a href="SearchEventServlet2?keyword=<%=GlobalVariable.keyword %>&area=6" <%= (GlobalVariable.area.equals("6") ? "class='active'" : "") %>>台中</a>
           	<a href="SearchEventServlet2?keyword=<%=GlobalVariable.keyword %>&area=7" <%= (GlobalVariable.area.equals("7") ? "class='active'" : "") %>>南投</a>
           	<a href="SearchEventServlet2?keyword=<%=GlobalVariable.keyword %>&area=8" <%= (GlobalVariable.area.equals("8") ? "class='active'" : "") %>>彰化</a>
          	<a href="SearchEventServlet2?keyword=<%=GlobalVariable.keyword %>&area=9" <%= (GlobalVariable.area.equals("9") ? "class='active'" : "") %>>雲林</a>
           	<a href="SearchEventServlet2?keyword=<%=GlobalVariable.keyword %>&area=10" <%= (GlobalVariable.area.equals("10") ? "class='active'" : "") %>>嘉義</a>
           	<a href="SearchEventServlet2?keyword=<%=GlobalVariable.keyword %>&area=11" <%= (GlobalVariable.area.equals("11") ? "class='active'" : "") %>>台南</a>
          	<a href="SearchEventServlet2?keyword=<%=GlobalVariable.keyword %>&area=12" <%= (GlobalVariable.area.equals("12") ? "class='active'" : "") %>>高雄</a>
          	<a href="SearchEventServlet2?keyword=<%=GlobalVariable.keyword %>&area=13" <%= (GlobalVariable.area.equals("13") ? "class='active'" : "") %>>屏東</a>
           	<a href="SearchEventServlet2?keyword=<%=GlobalVariable.keyword %>&area=14" <%= (GlobalVariable.area.equals("14") ? "class='active'" : "") %>>宜花東</a>
           	<a href="SearchEventServlet2?keyword=<%=GlobalVariable.keyword %>&area=15" <%= (GlobalVariable.area.equals("15") ? "class='active'" : "") %>>基隆</a>
          	<a href="SearchEventServlet2?keyword=<%=GlobalVariable.keyword %>&area=16" <%= (GlobalVariable.area.equals("16") ? "class='active'" : "") %>>香港</a>
            <a href="SearchEventServlet2?keyword=<%=GlobalVariable.keyword %>&area=17" <%= (GlobalVariable.area.equals("17") ? "class='active'" : "") %>>其他</a>
        </dd>
    </dl>
    <dl class="dl-horizontal hidden-xs">
        <dt>
            <span class="h3"><b>分類</b></span>
        </dt>
         <dd>
        	<a href="SearchEventServlet2?keyword=<%=GlobalVariable.keyword %>&category=0" <%= (GlobalVariable.category.equals("0") ? "class='active'" : "") %>><span class="">全部</span></a>
         	<a href="SearchEventServlet2?keyword=<%=GlobalVariable.keyword %>&category=1" <%= (GlobalVariable.category.equals("1") ? "class='active'" : "") %>><span class="">有氧運動</span></a>
           	<a href="SearchEventServlet2?keyword=<%=GlobalVariable.keyword %>&category=2" <%= (GlobalVariable.category.equals("2") ? "class='active'" : "") %>><span class="">阻力運動</span></a>
           	<a href="SearchEventServlet2?keyword=<%=GlobalVariable.keyword %>&category=3" <%= (GlobalVariable.category.equals("3") ? "class='active'" : "") %>><span class="">重量訓練</span></a>
           	<a href="SearchEventServlet2?keyword=<%=GlobalVariable.keyword %>&category=4" <%= (GlobalVariable.category.equals("4") ? "class='active'" : "") %>><span class="">伸展運動</span></a>
           	<a href="SearchEventServlet2?keyword=<%=GlobalVariable.keyword %>&category=5" <%= (GlobalVariable.category.equals("5") ? "class='active'" : "") %>><span class="">氣功瑜珈</span></a>
           	<a href="SearchEventServlet2?keyword=<%=GlobalVariable.keyword %>&category=6" <%= (GlobalVariable.category.equals("6") ? "class='active'" : "") %>><span class="">健走與慢跑</span></a>
        	<a href="SearchEventServlet2?keyword=<%=GlobalVariable.keyword %>&category=7" <%= (GlobalVariable.category.equals("7") ? "class='active'" : "") %>><span class="">其他</span></a>
        </dd>
    </dl>
    <dl class="dl-horizontal hidden-xs">
	    <dt>
	        <span class="h3"><b>時間</b></span>
	    </dt>
	    <dd>
	    	<a href="SearchEventServlet2?keyword=<%=GlobalVariable.keyword %>&date=0" <%= (GlobalVariable.date.equals("0") ? "class='active'" : "") %>>全部</a>
	    	<a href="SearchEventServlet2?keyword=<%=GlobalVariable.keyword %>&date=1" <%= (GlobalVariable.date.equals("1") ? "class='active'" : "") %>>今日</a>
	     	<a href="SearchEventServlet2?keyword=<%=GlobalVariable.keyword %>&date=2" <%= (GlobalVariable.date.equals("2") ? "class='active'" : "") %>>明日</a>
	       	<a href="SearchEventServlet2?keyword=<%=GlobalVariable.keyword %>&date=3" <%= (GlobalVariable.date.equals("3") ? "class='active'" : "") %>>本週</a>
	       	<a href="SearchEventServlet2?keyword=<%=GlobalVariable.keyword %>&date=4" <%= (GlobalVariable.date.equals("4") ? "class='active'" : "") %>>本月</a>
	    </dd>
    </dl>
    <div class="btn-group" ga-hover="Sort">
    	<a ga-click="sort_Latest" ng-class="{&#39;active&#39;:vm.SortBy==1}" type="button" class="btn btn-default active">最近活動</a>
    </div>
</div>
            </div>
        </div>
    </div>
    <div class="APCSS_wrap-search">
        <div class="container">
            <div class="row" ga-hover="Events">
                <div ui-view="searchView">

<div ng-init="vm.init={&quot;LocationType&quot;:0,&quot;CategoryType&quot;:0,&quot;TimeType&quot;:0,&quot;TicketPriceType&quot;:0,&quot;SortBy&quot;:1,&quot;CurrentIndex&quot;:0,&quot;Keyword&quot;:&quot;dssd&quot;,&quot;EndTime&quot;:&quot;99991231&quot;,&quot;StartTime&quot;:&quot;00010101&quot;,&quot;Action&quot;:&quot;Index&quot;,&quot;Controller&quot;:&quot;Search&quot;}" ga-event-item="" ga-event-item-limit="12" ga-ui-route-pagination="" class="ng-scope"></div>


<div class="APCSS_msg-search ng-scope">
    <div class="row">
        <div class="col-xs-4 col-sm-4 col-md-4">
            <img src="./searchNoEvent_files/search01.png" class="img-responsive">
        </div>
        <div class="col-xs-8 col-sm-8 col-md-8">
            <div class="info">
                <p class="h2">沒有找到任何任務！</p>
            </div>
        </div>
    </div>
</div>
</div>
                <div style="display:none">


<div ng-init="vm.init={&quot;LocationType&quot;:0,&quot;CategoryType&quot;:0,&quot;TimeType&quot;:0,&quot;TicketPriceType&quot;:0,&quot;SortBy&quot;:1,&quot;CurrentIndex&quot;:0,&quot;Keyword&quot;:&quot;dssd&quot;,&quot;EndTime&quot;:&quot;99991231&quot;,&quot;StartTime&quot;:&quot;00010101&quot;,&quot;Action&quot;:&quot;Index&quot;,&quot;Controller&quot;:&quot;Search&quot;}" ga-event-item="" ga-event-item-limit="12" ga-ui-route-pagination=""></div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="apcss-footer">
    <div class="container">
        <div class="row apcss-footer-first-section">
            <div class="col-xs-12 col-sm-3 hidden-xs">
                <p class="apcss-footer-title">新建任務</p>
                    <a ga-click="Create Event" href="http://www.yuntech.edu.tw/" class="apcss-footer-content" target="_blank">如何開始第一個任務？</a>
                <a ga-click="FAQ" href="http://www.yuntech.edu.tw/" class="apcss-footer-content" target="_blank">常見問題</a>
            </div>
            <div class="col-xs-12 col-sm-3 hidden-xs">
                <p class="apcss-footer-title">參與任務</p>
                <a ga-click="Features" href="http://www.yuntech.edu.tw/" class="apcss-footer-content" target="_blank">功能介紹</a>
                
                <a ga-click="Event News" href="http://www.yuntech.edu.tw/" class="apcss-footer-content" target="_blank">任務新鮮事</a>
            </div>
            <div class="col-xs-12 col-sm-3 hidden-xs">
                <p class="apcss-footer-title">關於我們</p>
                <a ga-click="Join Us" href="http://www.yuntech.edu.tw/" class="apcss-footer-content" target="_blank">加入我們</a>
                <a ga-click="Terms of Service" href="http://www.yuntech.edu.tw/" class="apcss-footer-content" target="_blank">服務條款</a>
                    <a ga-click="Partner" href="http://www.yuntech.edu.tw/" class="apcss-footer-content" target="_blank">合作夥伴</a>
            </div>
            <div class="col-xs-12 col-sm-3 apcss-footer-phone-section2">
                <address>
                    <p class="apcss-footer-title">Supermen服務熱線</p>
                    <p class="apcss-footer-content">雲林斗六 : +886 1-1234-5678</p>
                    <p class="apcss-footer-content">週一至週五 09:00-18:00</p>
                    <a ga-click="Service Center" href="http://www.yuntech.edu.tw/" class="apcss-footer-customer-help" target="_blank" data-uv-lightbox="classic_widget" data-uv-mode="support" data-uv-primary-color="#0089d2" data-uv-link-color="#0089d2" data-uv-default-mode="support" data-uv-scanned="true" id="uv-1">
                        <span>客服小幫手</span>
                    </a>
                </address>
            </div>
        </div>
	    <a href="http://www.yuntech.edu.tw/" style="text-align: center; position: relative; Right: -160px; top: 0px;" class="apcss-logo" target="_blank">
	    	<h1>雲科大資管系團隊© Supermelly Inc. All Rights Reserved.</h1>
	    </a>
	</div>
</div>
</body>
</html>