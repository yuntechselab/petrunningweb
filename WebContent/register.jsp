﻿<%@ page language="java" import="com.supermen.servlet.controller.GlobalVariable" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html ng-app="accupass" class="js flexbox flexboxlegacy canvas canvastext webgl no-touch geolocation postmessage websqldatabase indexeddb hashchange history draganddrop websockets rgba hsla multiplebgs backgroundsize borderimage borderradius boxshadow textshadow opacity cssanimations csscolumns cssgradients cssreflections csstransforms csstransforms3d csstransitions fontface generatedcontent video audio localstorage sessionstorage webworkers applicationcache svg inlinesvg smil svgclippaths ng-scope">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"><style type="text/css">@charset "UTF-8";[ng\:cloak],[ng-cloak],[data-ng-cloak],[x-ng-cloak],.ng-cloak,.x-ng-cloak,.ng-hide{display:none !important;}ng\:form{display:block;}.ng-animate-block-transitions{transition:0s all!important;-webkit-transition:0s all!important;}.ng-hide-add-active,.ng-hide-remove{display:block!important;}</style>

    <title>Supermen</title>
	<link rel="Shortcut Icon" type="image/x-icon" href="./icon/shortcut.png" />
    <link type="text/css" rel="stylesheet" href="./register_files/css.min.css">
    <script type="application/javascript" async="" defer="" src="./forgetPassword_files/track.js"></script><script type="text/javascript" async="" src="./forgetPassword_files/26hI9f54AQMRNaPo1MvFkw.js"></script><script id="facebook-jssdk" src="./forgetPassword_files/all.js"></script><script type="text/javascript" async="" src="./forgetPassword_files/atrk.js"></script><script async="" src="./forgetPassword_files/analytics.js"></script><script async="" src="./forgetPassword_files/fbds.js"></script><script type="text/javascript" src="./forgetPassword_files/js.min.js"></script>
    
    <script type="text/javascript" src="./forgetPassword_files/resource"></script>
    
<script type="text/javascript">
	function check() {
	    var mikExp = /[$\\@\\\#%\^\!\&\*\(\)\[\]\+\_\{\}\`\~\=\|]/;
	    var email = document.reg.email.value;
	    var rege = /^[-A-Za-z0-9_]+[-A-Za-z0-9_.]*[@]{1}[-A-Za-z0-9_]+[-A-Za-z0-9_.]*[.]{1}[A-Za-z]{2,5}$/;
	    
	    if (document.reg.name.value == "") {
			alert("姓名為必填欄位");
			document.reg.name.focus();
			return false;
		}
		
		if (document.reg.email.value == "") {
			alert("Email為必填欄位");
			document.reg.email.focus();
			return false;
		}
		
        if (rege.exec(email) == null) { 
			alert("無效的電子郵件地址");
			document.reg.email.focus();
			return false;
        }
        		
		if (reg.sex.value == "gender-info-text") {
			alert("性別為必選欄位");
			document.reg.sex.focus();
			return false;
		}
		
		if (document.reg.phone.value == "") {
			alert("手機為必填欄位");
			document.reg.phone.focus();
			return false;
		}

		if (document.reg.phone.value.length < 10) {
			alert("手機長度過短");
			document.reg.phone.focus();
			return false;
		}
						
		if (document.reg.password.value == "") {
			alert("密碼為必填欄位");
			document.reg.password.focus();
			return false;
		}
		
		if (document.reg.password.value.search(mikExp) != -1) {
		    alert("密碼含有以下的特殊字元： \n\r\n\r@ $ % ^ & * # ( ) [ ] \\ { + } ` ~ =  | \n\r\n\r請勿使用，謝謝配合\n");
		    return false;
	    }
		
		if (document.reg.password2.value == "") {
			alert("確認密碼為必填欄位");
			document.reg.password2.focus();
			return false;
		}
		
		if (document.reg.password.value != document.reg.password2.value) {
			alert("密碼與確認密碼不相符，請再試一次。");
			return false;
		}
	    
		document.reg.submit();
		return true;		
	}
</script>

	<style type="text/css" media="screen">.uv-icon{-moz-box-sizing:border-box;-webkit-box-sizing:border-box;box-sizing:border-box;display:inline-block;cursor:pointer;position:relative;-moz-transition:all 300ms;-o-transition:all 300ms;-webkit-transition:all 300ms;transition:all 300ms;width:39px;height:39px;position:fixed;z-index:100002;opacity:0.8;-moz-transition:opacity 100ms;-o-transition:opacity 100ms;-webkit-transition:opacity 100ms;transition:opacity 100ms}.uv-icon.uv-bottom-right{bottom:10px;right:12px}.uv-icon.uv-top-right{top:10px;right:12px}.uv-icon.uv-bottom-left{bottom:10px;left:12px}.uv-icon.uv-top-left{top:10px;left:12px}.uv-icon.uv-is-selected{opacity:1}.uv-icon svg{width:39px;height:39px}.uv-popover{font-family:sans-serif;font-weight:100;font-size:13px;color:black;position:fixed;z-index:100001}.uv-popover-content{-moz-border-radius:5px;-webkit-border-radius:5px;border-radius:5px;background:white;position:relative;width:325px;height:325px;-moz-transition:background 200ms;-o-transition:background 200ms;-webkit-transition:background 200ms;transition:background 200ms}.uv-bottom .uv-popover-content{-moz-box-shadow:rgba(0,0,0,0.3) 0 -10px 60px,rgba(0,0,0,0.1) 0 0 20px;-webkit-box-shadow:rgba(0,0,0,0.3) 0 -10px 60px,rgba(0,0,0,0.1) 0 0 20px;box-shadow:rgba(0,0,0,0.3) 0 -10px 60px,rgba(0,0,0,0.1) 0 0 20px}.uv-top .uv-popover-content{-moz-box-shadow:rgba(0,0,0,0.3) 0 10px 60px,rgba(0,0,0,0.1) 0 0 20px;-webkit-box-shadow:rgba(0,0,0,0.3) 0 10px 60px,rgba(0,0,0,0.1) 0 0 20px;box-shadow:rgba(0,0,0,0.3) 0 10px 60px,rgba(0,0,0,0.1) 0 0 20px}.uv-left .uv-popover-content{-moz-box-shadow:rgba(0,0,0,0.3) 10px 0 60px,rgba(0,0,0,0.1) 0 0 20px;-webkit-box-shadow:rgba(0,0,0,0.3) 10px 0 60px,rgba(0,0,0,0.1) 0 0 20px;box-shadow:rgba(0,0,0,0.3) 10px 0 60px,rgba(0,0,0,0.1) 0 0 20px}.uv-right .uv-popover-content{-moz-box-shadow:rgba(0,0,0,0.3) -10px 0 60px,rgba(0,0,0,0.1) 0 0 20px;-webkit-box-shadow:rgba(0,0,0,0.3) -10px 0 60px,rgba(0,0,0,0.1) 0 0 20px;box-shadow:rgba(0,0,0,0.3) -10px 0 60px,rgba(0,0,0,0.1) 0 0 20px}.uv-ie8 .uv-popover-content{position:relative}.uv-ie8 .uv-popover-content .uv-popover-content-shadow{display:block;background:black;content:'';position:absolute;left:-15px;top:-15px;width:100%;height:100%;filter:progid:DXImageTransform.Microsoft.Blur(PixelRadius=15,MakeShadow=true,ShadowOpacity=0.30);z-index:-1}.uv-popover-tail{border:9px solid transparent;width:0;z-index:10;position:absolute;-moz-transition:border-top-color 200ms;-o-transition:border-top-color 200ms;-webkit-transition:border-top-color 200ms;transition:border-top-color 200ms}.uv-top .uv-popover-tail{bottom:-20px;border-top:11px solid white}.uv-bottom .uv-popover-tail{top:-20px;border-bottom:11px solid white}.uv-left .uv-popover-tail{right:-20px;border-left:11px solid white}.uv-right .uv-popover-tail{left:-20px;border-right:11px solid white}.uv-popover-loading{background:white;-moz-border-radius:5px;-webkit-border-radius:5px;border-radius:5px;position:absolute;width:100%;height:100%;left:0;top:0}.uv-popover-loading-text{position:absolute;top:50%;margin-top:-0.5em;width:100%;text-align:center}.uv-popover-iframe-container{height:100%}.uv-popover-iframe{-moz-border-radius:5px;-webkit-border-radius:5px;border-radius:5px;overflow:hidden}.uv-is-hidden{display:none}.uv-is-invisible{display:block !important;visibility:hidden !important}.uv-is-transitioning{display:block !important}.uv-no-transition{-moz-transition:none !important;-webkit-transition:none !important;-o-transition:color 0 ease-in !important;transition:none !important}.uv-fade{opacity:1;-moz-transition:opacity 200ms ease-out;-o-transition:opacity 200ms ease-out;-webkit-transition:opacity 200ms ease-out;transition:opacity 200ms ease-out}.uv-fade.uv-is-hidden{opacity:0}.uv-scale-top,.uv-scale-top-left,.uv-scale-top-right,.uv-scale-bottom,.uv-scale-bottom-left,.uv-scale-bottom-right,.uv-scale-right,.uv-scale-right-top,.uv-scale-right-bottom,.uv-scale-left,.uv-scale-left-top,.uv-scale-left-bottom,.uv-slide-top,.uv-slide-bottom,.uv-slide-left,.uv-slide-right{opacity:1;-moz-transition:all 80ms ease-out;-o-transition:all 80ms ease-out;-webkit-transition:all 80ms ease-out;transition:all 80ms ease-out}.uv-scale-top.uv-is-hidden{opacity:0;-moz-transform:scale(0.8) translateY(-15%);-ms-transform:scale(0.8) translateY(-15%);-webkit-transform:scale(0.8) translateY(-15%);transform:scale(0.8) translateY(-15%)}.uv-scale-top-left.uv-is-hidden{opacity:0;-moz-transform:scale(0.8) translateY(-15%) translateX(-10%);-ms-transform:scale(0.8) translateY(-15%) translateX(-10%);-webkit-transform:scale(0.8) translateY(-15%) translateX(-10%);transform:scale(0.8) translateY(-15%) translateX(-10%)}.uv-scale-top-right.uv-is-hidden{opacity:0;-moz-transform:scale(0.8) translateY(-15%) translateX(10%);-ms-transform:scale(0.8) translateY(-15%) translateX(10%);-webkit-transform:scale(0.8) translateY(-15%) translateX(10%);transform:scale(0.8) translateY(-15%) translateX(10%)}.uv-scale-bottom.uv-is-hidden{opacity:0;-moz-transform:scale(0.8) translateY(15%);-ms-transform:scale(0.8) translateY(15%);-webkit-transform:scale(0.8) translateY(15%);transform:scale(0.8) translateY(15%)}.uv-scale-bottom-left.uv-is-hidden{opacity:0;-moz-transform:scale(0.8) translateY(15%) translateX(-10%);-ms-transform:scale(0.8) translateY(15%) translateX(-10%);-webkit-transform:scale(0.8) translateY(15%) translateX(-10%);transform:scale(0.8) translateY(15%) translateX(-10%)}.uv-scale-bottom-right.uv-is-hidden{opacity:0;-moz-transform:scale(0.8) translateY(15%) translateX(10%);-ms-transform:scale(0.8) translateY(15%) translateX(10%);-webkit-transform:scale(0.8) translateY(15%) translateX(10%);transform:scale(0.8) translateY(15%) translateX(10%)}.uv-scale-right.uv-is-hidden{opacity:0;-moz-transform:scale(0.8) translateX(15%);-ms-transform:scale(0.8) translateX(15%);-webkit-transform:scale(0.8) translateX(15%);transform:scale(0.8) translateX(15%)}.uv-scale-right-top.uv-is-hidden{opacity:0;-moz-transform:scale(0.8) translateX(15%) translateY(-10%);-ms-transform:scale(0.8) translateX(15%) translateY(-10%);-webkit-transform:scale(0.8) translateX(15%) translateY(-10%);transform:scale(0.8) translateX(15%) translateY(-10%)}.uv-scale-right-bottom.uv-is-hidden{opacity:0;-moz-transform:scale(0.8) translateX(15%) translateY(10%);-ms-transform:scale(0.8) translateX(15%) translateY(10%);-webkit-transform:scale(0.8) translateX(15%) translateY(10%);transform:scale(0.8) translateX(15%) translateY(10%)}.uv-scale-left.uv-is-hidden{opacity:0;-moz-transform:scale(0.8) translateX(-15%);-ms-transform:scale(0.8) translateX(-15%);-webkit-transform:scale(0.8) translateX(-15%);transform:scale(0.8) translateX(-15%)}.uv-scale-left-top.uv-is-hidden{opacity:0;-moz-transform:scale(0.8) translateX(-15%) translateY(-10%);-ms-transform:scale(0.8) translateX(-15%) translateY(-10%);-webkit-transform:scale(0.8) translateX(-15%) translateY(-10%);transform:scale(0.8) translateX(-15%) translateY(-10%)}.uv-scale-left-bottom.uv-is-hidden{opacity:0;-moz-transform:scale(0.8) translateX(-15%) translateY(10%);-ms-transform:scale(0.8) translateX(-15%) translateY(10%);-webkit-transform:scale(0.8) translateX(-15%) translateY(10%);transform:scale(0.8) translateX(-15%) translateY(10%)}.uv-slide-top.uv-is-hidden{-moz-transform:translateY(-100%);-ms-transform:translateY(-100%);-webkit-transform:translateY(-100%);transform:translateY(-100%)}.uv-slide-bottom.uv-is-hidden{-moz-transform:translateY(100%);-ms-transform:translateY(100%);-webkit-transform:translateY(100%);transform:translateY(100%)}.uv-slide-left.uv-is-hidden{-moz-transform:translateX(-100%);-ms-transform:translateX(-100%);-webkit-transform:translateX(-100%);transform:translateX(-100%)}.uv-slide-right.uv-is-hidden{-moz-transform:translateX(100%);-ms-transform:translateX(100%);-webkit-transform:translateX(100%);transform:translateX(100%)}</style>
</head>
<body>
<div class="APCSS_wrapper"></div>
<nav class="navbar navbar-default navbar-fixed-top APCSS_header" role="navigation">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="navbar-header">
                    <!-- desktop version -->
                    <h1 class="accupass">
                        <a class="navbar-brand hidden-xs hidden-sm" href="index.jsp" target="_self">
                            <img src="./icon/supermen.png" style="position: relative; width: 125px; height: 30px;">
                        </a>
                    </h1>
                    <form name="search" action="SearchEventServlet" method="post" class="apcss-search-form ng-pristine ng-valid">
                        <div class="apcss-search-form-wrapper">
                            <input type="text" name="keyword" class="apcss-search-form-input" style="color:#FFFFFF" placeholder="找任務" onfocus="if(this.placeholder=='找任務'){this.placeholder=''};this.style.color='#FFFFFF';" onblur="if(this.placeholder==''||this.placeholder=='找任務'){this.placeholder='找任務';this.style.color='#FFFFFF';}">
                            <span class="apcss-search-form-button apcss-search-focus">
                            	<button type="submit" class="btn btn-primary ng-scope" style="position: relative; width: 38px; height: 38px; top: -6.5px; right: 12px;"><img src="./icon/search.png" style="position: relative; top: 0px; right: 1px;" width="14" height="14"></button>
                            </span>
                        </div>
                    </form>
                    <div class="dropdown apcss-dropdown-explore-activity" ng-class="hover">
                        <a class="apcss-dropdown-explore-activity-button" role="button" ng-mouseover="hover=&#39;open&#39;" ng-mouseleave="hover=&#39;&#39;" target="_blank">
                            <span>探索任務</span><img src="./icon/triangle.png" style="position: relative; top: -1px;">
                        </a>
                        <ul class="dropdown-menu apcss-dropdown-explore-activity-menu" ng-mouseleave="hover=&#39;&#39;" role="menu">
    
    <li role="presentation" class="dropdown-header">熱門主打</li>
        <li role="presentation">
            <a ga-click="Hit_1" role="menuitem" tabindex="-1" href="http://www.yuntech.edu.tw/" title="五月跑步約起來" target="_blank">
                <img src="./icon/banner1.jpg">
            </a>
        </li>
        <li role="presentation">
            <a ga-click="Hit_2" role="menuitem" tabindex="-1" href="http://www.yuntech.edu.tw/" title="小翼貓的一天" target="_blank">
                <img src="./icon/banner2.jpg">
            </a>
        </li>
        <li role="presentation" class="dropdown-header">時間</li>
        <li role="presentation">
            <div class="row">
            	<a class="col-md-3 menuitem" href="SearchEventServlet?keyword=<%=GlobalVariable.keyword %>&date=1" title="今日" target="_self">今日</a>
              	<a class="col-md-3 menuitem" href="SearchEventServlet?keyword=<%=GlobalVariable.keyword %>&date=2" title="明日" target="_self">明日</a>
             	<a class="col-md-3 menuitem" href="SearchEventServlet?keyword=<%=GlobalVariable.keyword %>&date=3" title="本週" target="_self">本週</a>
               	<a class="col-md-3 menuitem" href="SearchEventServlet?keyword=<%=GlobalVariable.keyword %>&date=4" title="本月" target="_self">本月</a>
            </div>
        </li>
</ul>
                    </div>
                </div>
                <div class="collapse navbar-collapse" id="APJS_id-userOpt">
                    <div class="apcss-userinfo-box navbar-right hidden-xs hidden-sm">
                        <div id="APJS_spinjs" class="APCSS_spinner" ap-page-load=""><div class="rect1"></div><div class="rect2"></div><div class="rect3"></div><div class="rect4"></div><div class="rect5"></div></div>

                            <div class="apcss-userinfo-box-anonymous">
                                <a ga-click="Login" href="login.jsp" target="_self">登入</a>
                                <a ga-click="Signup" href="register.jsp" target="_self">註冊</a>
                            </div>
                        <a ga-click="Create_events" class="apcss-create-activity-button" href="login.jsp" target="_self">新任務</a>
                                                                                        </div>
                                                                                    </div><!-- /.navbar-collapse -->
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </nav>
<div style="background: url(./icon/running2.jpg);">
    <div class="container" ap-account-register="">
        <div class="row">
            <div class="col-xs-12">
                <div class="APCSS_slogan">
                    <img src="./icon/supermen2.png" style="width: 91px; height: 92px;">
                    <font color="white"><h3>許多有益健康的任務在這裡進行<br>
                    立即開始你的任務</h3></font>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-sm-offset-3">
                <div class="APCSS_registerBox">
                <div class="tab-content">
                    <div class="tab-pane active">
                        <form name="reg" action="RegisterServlet" method="post" role="form" class="ng-pristine ng-valid">
                            <div class="form-group">
                                <input type="text" class="form-control ng-pristine ng-valid" name="name" id="APJS_id-name" ng-model="form.name" validator="[required]" maxlength="255" placeholder="姓名" ap-focus="">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control ng-pristine ng-valid" name="email" id="APJS_id-email" ng-model="form.email" validator="[required, email]" placeholder="Email" maxlength="255">
                            </div>
                            <div class="form-group">
                                <select name="sex" id="APJS_id-gender" class="form-control ng-pristine ng-valid" ng-model="form.gender" validator="[required]">
                                    <option value="gender-info-text" disabled="">性別</option>
                                    <option value="0">暫不透露</option>
                                    <option value="1">男性</option>
                                    <option value="2">女性</option>
                                </select>
                            </div>
                            <div class="form-group APCSS_phoneNumGroup">
                                <div class="row">
                                    <div class="col-xs-6 col-sm-6 col-md-6">
                                        <select name="code" ng-model="form.country" class="form-control ng-pristine ng-valid">
                                                <option value="TW">台灣 +886</option>
                                                <option value="CN">中國 +86</option>
                                                <option value="HK">香港 +852</option>
                                                <option value="MO">澳門 +853</option>
                                                <option value="JP">日本 +81</option>
                                                <option value="KR">南韓 +82</option>
                                                <option value="US">北美 +1</option>
                                                <option value="MY">馬來西亞 +60</option>
                                                <option value="SG">新加坡 +65</option>
                                                <option value="Others">其他 +0</option>
                                        </select>
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-md-6">
                                        <input type="text" name="phone" id="APJS_id-phone" class="form-control ng-pristine ng-valid" ng-model="form.phoneNumber" validator="[required, phone]" placeholder="手機" maxlength="255" onkeyup="this.value=this.value.replace(/[^\d]/g,'')">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <input type="password" name="password" class="form-control ng-pristine ng-valid" id="APJS_id-pwd" ng-model="form.password" validator="[required, passwordLength]" maxlength="18" placeholder="密碼">
                            </div>
                            <div class="form-group">
                                <input type="password" name="password2" class="form-control ng-pristine ng-valid" id="APJS_id-confirm" ng-model="form.confirmPassword" validator="[required, confirmPassword]" validator-password="form.password" placeholder="確認密碼">
                            </div>
                            <input type="button" class="btn btn-primary btn-lg btn-block" value="建立帳號" onClick="check()">
                        </form>
                    </div>
                </div>
                <p>已經是 Supermen 會員?  <a href="login.jsp">登入</a></p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="apcss-footer">
    <div class="container">
        <div class="row apcss-footer-first-section">
            <div class="col-xs-12 col-sm-3 hidden-xs">
                <p class="apcss-footer-title">新建任務</p>
                    <a ga-click="Create Event" href="http://www.yuntech.edu.tw/" class="apcss-footer-content" target="_blank">如何開始第一個任務？</a>
                <a ga-click="FAQ" href="http://www.yuntech.edu.tw/" class="apcss-footer-content" target="_blank">常見問題</a>
            </div>
            <div class="col-xs-12 col-sm-3 hidden-xs">
                <p class="apcss-footer-title">參與任務</p>
                <a ga-click="Features" href="http://www.yuntech.edu.tw/" class="apcss-footer-content" target="_blank">功能介紹</a>
                
                <a ga-click="Event News" href="http://www.yuntech.edu.tw/" class="apcss-footer-content" target="_blank">任務新鮮事</a>
            </div>
            <div class="col-xs-12 col-sm-3 hidden-xs">
                <p class="apcss-footer-title">關於我們</p>
                <a ga-click="Join Us" href="http://www.yuntech.edu.tw/" class="apcss-footer-content" target="_blank">加入我們</a>
                <a ga-click="Terms of Service" href="http://www.yuntech.edu.tw/" class="apcss-footer-content" target="_blank">服務條款</a>
                    <a ga-click="Partner" href="http://www.yuntech.edu.tw/" class="apcss-footer-content" target="_blank">合作夥伴</a>
            </div>
            <div class="col-xs-12 col-sm-3 apcss-footer-phone-section2">
                <address>
                    <p class="apcss-footer-title">Supermen服務熱線</p>
                    <p class="apcss-footer-content">雲林斗六 : +886 1-1234-5678</p>
                    <p class="apcss-footer-content">週一至週五 09:00-18:00</p>
                    <a ga-click="Service Center" href="http://www.yuntech.edu.tw/" class="apcss-footer-customer-help" target="_blank" data-uv-lightbox="classic_widget" data-uv-mode="support" data-uv-primary-color="#0089d2" data-uv-link-color="#0089d2" data-uv-default-mode="support" data-uv-scanned="true" id="uv-1">
                        <span>客服小幫手</span>
                    </a>
                </address>
            </div>
        </div>
	    <a href="http://www.yuntech.edu.tw/" style="text-align: center; position: relative; Right: -160px; top: 0px;" class="apcss-logo" target="_blank">
	    	<h1>雲科大資管系團隊© Supermelly Inc. All Rights Reserved.</h1>
	    </a>
	</div>
</div>
</body>
</html>