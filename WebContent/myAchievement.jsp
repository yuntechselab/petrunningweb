﻿<%@ page language="java" import="com.supermen.servlet.controller.GlobalVariable" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html ng-app="accupass" class="js flexbox flexboxlegacy canvas canvastext webgl no-touch geolocation postmessage websqldatabase indexeddb hashchange history draganddrop websockets rgba hsla multiplebgs backgroundsize borderimage borderradius boxshadow textshadow opacity cssanimations csscolumns cssgradients cssreflections csstransforms csstransforms3d csstransitions fontface generatedcontent video audio localstorage sessionstorage webworkers applicationcache svg inlinesvg smil svgclippaths ng-scope">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"><style type="text/css">@charset "UTF-8";[ng\:cloak],[ng-cloak],[data-ng-cloak],[x-ng-cloak],.ng-cloak,.x-ng-cloak,.ng-hide{display:none !important;}ng\:form{display:block;}.ng-animate-block-transitions{transition:0s all!important;-webkit-transition:0s all!important;}.ng-hide-add-active,.ng-hide-remove{display:block!important;}</style>

    <title>Supermen</title>
	<link rel="Shortcut Icon" type="image/x-icon" href="./icon/shortcut.png" />
    <link type="text/css" rel="stylesheet" href="./register_files/css.min.css">
    <script type="application/javascript" async="" defer="" src="./mySport_files/track.js"></script>
    <script type="text/javascript" src="./forgetPassword_files/js.min.js"></script>
    <script type="text/javascript" src="./forgetPassword_files/resource"></script>
    
	<link rel="stylesheet" type="text/css" href="./mySport_files/style.css">
	<link rel="stylesheet" type="text/css" href="./mySport_files/nikeplus.css">
	<link rel="stylesheet" type="text/css" href="./mySport_files/nikeplus_trophies.css">
	
	<style type="text/css" media="screen">.uv-icon{-moz-box-sizing:border-box;-webkit-box-sizing:border-box;box-sizing:border-box;display:inline-block;cursor:pointer;position:relative;-moz-transition:all 300ms;-o-transition:all 300ms;-webkit-transition:all 300ms;transition:all 300ms;width:39px;height:39px;position:fixed;z-index:100002;opacity:0.8;-moz-transition:opacity 100ms;-o-transition:opacity 100ms;-webkit-transition:opacity 100ms;transition:opacity 100ms}.uv-icon.uv-bottom-right{bottom:10px;right:12px}.uv-icon.uv-top-right{top:10px;right:12px}.uv-icon.uv-bottom-left{bottom:10px;left:12px}.uv-icon.uv-top-left{top:10px;left:12px}.uv-icon.uv-is-selected{opacity:1}.uv-icon svg{width:39px;height:39px}.uv-popover{font-family:sans-serif;font-weight:100;font-size:13px;color:black;position:fixed;z-index:100001}.uv-popover-content{-moz-border-radius:5px;-webkit-border-radius:5px;border-radius:5px;background:white;position:relative;width:325px;height:325px;-moz-transition:background 200ms;-o-transition:background 200ms;-webkit-transition:background 200ms;transition:background 200ms}.uv-bottom .uv-popover-content{-moz-box-shadow:rgba(0,0,0,0.3) 0 -10px 60px,rgba(0,0,0,0.1) 0 0 20px;-webkit-box-shadow:rgba(0,0,0,0.3) 0 -10px 60px,rgba(0,0,0,0.1) 0 0 20px;box-shadow:rgba(0,0,0,0.3) 0 -10px 60px,rgba(0,0,0,0.1) 0 0 20px}.uv-top .uv-popover-content{-moz-box-shadow:rgba(0,0,0,0.3) 0 10px 60px,rgba(0,0,0,0.1) 0 0 20px;-webkit-box-shadow:rgba(0,0,0,0.3) 0 10px 60px,rgba(0,0,0,0.1) 0 0 20px;box-shadow:rgba(0,0,0,0.3) 0 10px 60px,rgba(0,0,0,0.1) 0 0 20px}.uv-left .uv-popover-content{-moz-box-shadow:rgba(0,0,0,0.3) 10px 0 60px,rgba(0,0,0,0.1) 0 0 20px;-webkit-box-shadow:rgba(0,0,0,0.3) 10px 0 60px,rgba(0,0,0,0.1) 0 0 20px;box-shadow:rgba(0,0,0,0.3) 10px 0 60px,rgba(0,0,0,0.1) 0 0 20px}.uv-right .uv-popover-content{-moz-box-shadow:rgba(0,0,0,0.3) -10px 0 60px,rgba(0,0,0,0.1) 0 0 20px;-webkit-box-shadow:rgba(0,0,0,0.3) -10px 0 60px,rgba(0,0,0,0.1) 0 0 20px;box-shadow:rgba(0,0,0,0.3) -10px 0 60px,rgba(0,0,0,0.1) 0 0 20px}.uv-ie8 .uv-popover-content{position:relative}.uv-ie8 .uv-popover-content .uv-popover-content-shadow{display:block;background:black;content:'';position:absolute;left:-15px;top:-15px;width:100%;height:100%;filter:progid:DXImageTransform.Microsoft.Blur(PixelRadius=15,MakeShadow=true,ShadowOpacity=0.30);z-index:-1}.uv-popover-tail{border:9px solid transparent;width:0;z-index:10;position:absolute;-moz-transition:border-top-color 200ms;-o-transition:border-top-color 200ms;-webkit-transition:border-top-color 200ms;transition:border-top-color 200ms}.uv-top .uv-popover-tail{bottom:-20px;border-top:11px solid white}.uv-bottom .uv-popover-tail{top:-20px;border-bottom:11px solid white}.uv-left .uv-popover-tail{right:-20px;border-left:11px solid white}.uv-right .uv-popover-tail{left:-20px;border-right:11px solid white}.uv-popover-loading{background:white;-moz-border-radius:5px;-webkit-border-radius:5px;border-radius:5px;position:absolute;width:100%;height:100%;left:0;top:0}.uv-popover-loading-text{position:absolute;top:50%;margin-top:-0.5em;width:100%;text-align:center}.uv-popover-iframe-container{height:100%}.uv-popover-iframe{-moz-border-radius:5px;-webkit-border-radius:5px;border-radius:5px;overflow:hidden}.uv-is-hidden{display:none}.uv-is-invisible{display:block !important;visibility:hidden !important}.uv-is-transitioning{display:block !important}.uv-no-transition{-moz-transition:none !important;-webkit-transition:none !important;-o-transition:color 0 ease-in !important;transition:none !important}.uv-fade{opacity:1;-moz-transition:opacity 200ms ease-out;-o-transition:opacity 200ms ease-out;-webkit-transition:opacity 200ms ease-out;transition:opacity 200ms ease-out}.uv-fade.uv-is-hidden{opacity:0}.uv-scale-top,.uv-scale-top-left,.uv-scale-top-right,.uv-scale-bottom,.uv-scale-bottom-left,.uv-scale-bottom-right,.uv-scale-right,.uv-scale-right-top,.uv-scale-right-bottom,.uv-scale-left,.uv-scale-left-top,.uv-scale-left-bottom,.uv-slide-top,.uv-slide-bottom,.uv-slide-left,.uv-slide-right{opacity:1;-moz-transition:all 80ms ease-out;-o-transition:all 80ms ease-out;-webkit-transition:all 80ms ease-out;transition:all 80ms ease-out}.uv-scale-top.uv-is-hidden{opacity:0;-moz-transform:scale(0.8) translateY(-15%);-ms-transform:scale(0.8) translateY(-15%);-webkit-transform:scale(0.8) translateY(-15%);transform:scale(0.8) translateY(-15%)}.uv-scale-top-left.uv-is-hidden{opacity:0;-moz-transform:scale(0.8) translateY(-15%) translateX(-10%);-ms-transform:scale(0.8) translateY(-15%) translateX(-10%);-webkit-transform:scale(0.8) translateY(-15%) translateX(-10%);transform:scale(0.8) translateY(-15%) translateX(-10%)}.uv-scale-top-right.uv-is-hidden{opacity:0;-moz-transform:scale(0.8) translateY(-15%) translateX(10%);-ms-transform:scale(0.8) translateY(-15%) translateX(10%);-webkit-transform:scale(0.8) translateY(-15%) translateX(10%);transform:scale(0.8) translateY(-15%) translateX(10%)}.uv-scale-bottom.uv-is-hidden{opacity:0;-moz-transform:scale(0.8) translateY(15%);-ms-transform:scale(0.8) translateY(15%);-webkit-transform:scale(0.8) translateY(15%);transform:scale(0.8) translateY(15%)}.uv-scale-bottom-left.uv-is-hidden{opacity:0;-moz-transform:scale(0.8) translateY(15%) translateX(-10%);-ms-transform:scale(0.8) translateY(15%) translateX(-10%);-webkit-transform:scale(0.8) translateY(15%) translateX(-10%);transform:scale(0.8) translateY(15%) translateX(-10%)}.uv-scale-bottom-right.uv-is-hidden{opacity:0;-moz-transform:scale(0.8) translateY(15%) translateX(10%);-ms-transform:scale(0.8) translateY(15%) translateX(10%);-webkit-transform:scale(0.8) translateY(15%) translateX(10%);transform:scale(0.8) translateY(15%) translateX(10%)}.uv-scale-right.uv-is-hidden{opacity:0;-moz-transform:scale(0.8) translateX(15%);-ms-transform:scale(0.8) translateX(15%);-webkit-transform:scale(0.8) translateX(15%);transform:scale(0.8) translateX(15%)}.uv-scale-right-top.uv-is-hidden{opacity:0;-moz-transform:scale(0.8) translateX(15%) translateY(-10%);-ms-transform:scale(0.8) translateX(15%) translateY(-10%);-webkit-transform:scale(0.8) translateX(15%) translateY(-10%);transform:scale(0.8) translateX(15%) translateY(-10%)}.uv-scale-right-bottom.uv-is-hidden{opacity:0;-moz-transform:scale(0.8) translateX(15%) translateY(10%);-ms-transform:scale(0.8) translateX(15%) translateY(10%);-webkit-transform:scale(0.8) translateX(15%) translateY(10%);transform:scale(0.8) translateX(15%) translateY(10%)}.uv-scale-left.uv-is-hidden{opacity:0;-moz-transform:scale(0.8) translateX(-15%);-ms-transform:scale(0.8) translateX(-15%);-webkit-transform:scale(0.8) translateX(-15%);transform:scale(0.8) translateX(-15%)}.uv-scale-left-top.uv-is-hidden{opacity:0;-moz-transform:scale(0.8) translateX(-15%) translateY(-10%);-ms-transform:scale(0.8) translateX(-15%) translateY(-10%);-webkit-transform:scale(0.8) translateX(-15%) translateY(-10%);transform:scale(0.8) translateX(-15%) translateY(-10%)}.uv-scale-left-bottom.uv-is-hidden{opacity:0;-moz-transform:scale(0.8) translateX(-15%) translateY(10%);-ms-transform:scale(0.8) translateX(-15%) translateY(10%);-webkit-transform:scale(0.8) translateX(-15%) translateY(10%);transform:scale(0.8) translateX(-15%) translateY(10%)}.uv-slide-top.uv-is-hidden{-moz-transform:translateY(-100%);-ms-transform:translateY(-100%);-webkit-transform:translateY(-100%);transform:translateY(-100%)}.uv-slide-bottom.uv-is-hidden{-moz-transform:translateY(100%);-ms-transform:translateY(100%);-webkit-transform:translateY(100%);transform:translateY(100%)}.uv-slide-left.uv-is-hidden{-moz-transform:translateX(-100%);-ms-transform:translateX(-100%);-webkit-transform:translateX(-100%);transform:translateX(-100%)}.uv-slide-right.uv-is-hidden{-moz-transform:translateX(100%);-ms-transform:translateX(100%);-webkit-transform:translateX(100%);transform:translateX(100%)}</style>
</head>
<body>
<div class="APCSS_wrapper"></div>
<nav class="navbar navbar-default navbar-fixed-top APCSS_header" role="navigation">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="navbar-header">
                    <!-- desktop version -->
                    <h1 class="accupass">
                        <a class="navbar-brand hidden-xs hidden-sm" href="index2.jsp" target="_self">
                            <img src="./icon/supermen.png" style="position: relative; width: 125px; height: 30px;">
                        </a>
                    </h1>
                    <form name="search" action="SearchEventServlet2" method="post" class="apcss-search-form ng-pristine ng-valid">
                        <div class="apcss-search-form-wrapper">
                            <input type="text" name="keyword" class="apcss-search-form-input" style="color:#FFFFFF" placeholder="找任務" onfocus="if(this.placeholder=='找任務'){this.placeholder=''};this.style.color='#FFFFFF';" onblur="if(this.placeholder==''||this.placeholder=='找任務'){this.placeholder='找任務';this.style.color='#FFFFFF';}">
                            <span class="apcss-search-form-button apcss-search-focus">
                            	<button type="submit" class="btn btn-primary ng-scope" style="position: relative; width: 38px; height: 38px; top: -6.5px; right: 12px;"><img src="./icon/search.png" style="position: relative; top: 0px; right: 1px;" width="14" height="14"></button>
                            </span>
                        </div>
                    </form>
                    <div class="dropdown apcss-dropdown-explore-activity" ng-class="hover">
                        <a class="apcss-dropdown-explore-activity-button" role="button" ng-mouseover="hover=&#39;open&#39;" ng-mouseleave="hover=&#39;&#39;" target="_blank">
                            <span>探索任務</span><img src="./icon/triangle.png" style="position: relative; top: -1px;">
                        </a>
                        <ul class="dropdown-menu apcss-dropdown-explore-activity-menu" ng-mouseleave="hover=&#39;&#39;" role="menu">
    
    <li role="presentation" class="dropdown-header">熱門主打</li>
        <li role="presentation">
            <a ga-click="Hit_1" role="menuitem" tabindex="-1" href="http://www.yuntech.edu.tw/" title="五月跑步約起來" target="_blank">
                <img src="./icon/banner1.jpg">
            </a>
        </li>
        <li role="presentation">
            <a ga-click="Hit_2" role="menuitem" tabindex="-1" href="http://www.yuntech.edu.tw/" title="小翼貓的一天" target="_blank">
                <img src="./icon/banner2.jpg">
            </a>
        </li>
        <li role="presentation" class="dropdown-header">時間</li>
        <li role="presentation">
            <div class="row">
            	<a class="col-md-3 menuitem" href="SearchEventServlet2?keyword=<%=GlobalVariable.keyword %>&date=1" title="今日" target="_self">今日</a>
              	<a class="col-md-3 menuitem" href="SearchEventServlet2?keyword=<%=GlobalVariable.keyword %>&date=2" title="明日" target="_self">明日</a>
             	<a class="col-md-3 menuitem" href="SearchEventServlet2?keyword=<%=GlobalVariable.keyword %>&date=3" title="本週" target="_self">本週</a>
               	<a class="col-md-3 menuitem" href="SearchEventServlet2?keyword=<%=GlobalVariable.keyword %>&date=4" title="本月" target="_self">本月</a>
            </div>
        </li>
</ul>
                    </div>
                </div>
                <div class="collapse navbar-collapse" id="APJS_id-userOpt">
                    <div class="apcss-userinfo-box navbar-right hidden-xs hidden-sm">
                        <div id="APJS_spinjs" class="APCSS_spinner" ap-page-load=""><div class="rect1"></div><div class="rect2"></div><div class="rect3"></div><div class="rect4"></div><div class="rect5"></div></div>
                            <div class="dropdown">
                                <a class="apcss-dropdown-user-button" id="APJS_dropdown-user" role="button" data-toggle="dropdown" data-target="#">
                                    <div class="apcss-avatar">
                                        <img src="./index2_files/default_user.png">
                                    </div>
                                    <img src="./icon/triangle.png" style="position: relative; left: 6px; top: -1px;">
                                </a>
                                <ul class="dropdown-menu" role="menu" aria-labelledby="APJS_dropdown-user">
                                    <li role="presentation" class="dropdown-header">參加任務</li>
                                    <li role="presentation"><a ga-click="User" role="menuitem" tabindex="-1" href="MyDetailServlet" target="_self">我的個人頁面</a></li>
                                    <li role="presentation"><a ga-click="My Tickets" role="menuitem" tabindex="-1" href="MyTicketServlet" target="_self">我的票劵</a></li>
                                    <li role="presentation"><a ga-click="My Tickets" role="menuitem" tabindex="-1" href="mySport.jsp" target="_self">我的運動記錄</a></li>
                                    <li role="presentation"><a ga-click="My Tickets" role="menuitem" tabindex="-1" href="myAchievement.jsp" target="_self">我的成就記錄</a></li>
                                    <li role="presentation" class="divider"></li>
                                    <li role="presentation" class="dropdown-header">主辦任務</li>
                                    <li role="presentation"><a ga-click="My Events" role="menuitem" tabindex="-1" href="ListEventServlet" target="_self">我登記的任務</a></li>
                                    <li role="presentation" class="divider"></li>
                                    <li role="presentation" class="dropdown-header">帳號設定</li>
                                    <li role="presentation"><a ga-click="Edit My Profile" role="menuitem" tabindex="-1" href="UpdateProfileServlet" target="_self">修改個人資料</a></li>
                                    <li role="presentation" class="divider"></li>
									<li role="presentation"><a ga-click="Log Out" role="menuitem" tabindex="-1" href="index.jsp" target="_self">登出</a></li>
								</ul>
                        	</div>
                        	<a ga-click="Create_events" class="apcss-create-activity-button" href="ListEventServlet" target="_self">新任務</a>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </nav>
<div class="APCSS_page-myContact" ap-my-contact-index="">
    <nav class="navbar navbar-default" role="navigation">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="collapse navbar-collapse">
                        <ul class="nav navbar-nav">
                            <li class="active"><a href="myAchievement.jsp" target="_self">成就總覽</a></li>
                            <li><a href="myAchievement2.jsp" target="_self">獎盃櫃</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </nav>
<div id="nikeplus">
	<div id="main">
		<div class="row" style="position: relative; top: 20px;">
			<div class="fourcol item">
			   <h3>
					<ul class="trophies">
						<li class="highmile highmile-bronze">
							<img src="./icon/trophy/56.png" style="position: relative; right: 20px; top: 5px;">
						</li>
					</ul>
					高里數銅獎杯
				</h3>
				<p>一個月內跑超過 24 公里</p>
			</div>
			<div class="fourcol item">
			   <h3>
					<ul class="trophies">
						<li class="highmile highmile-silver">
							<img src="./icon/trophy/57.png" style="position: relative; right: 20px; top: 5px;">
						</li>
					</ul>
					高里數銀獎杯
				</h3>
				<p>一個月內跑超過 40 公里</p>
			</div>
			<div class="fourcol item">
			   <h3>
					<ul class="trophies">
						<li class="highmile highmile-gold">
							<img src="./icon/trophy/58.png" style="position: relative; right: 20px; top: 5px;">
						</li>
					</ul>
					高里數金獎杯
				</h3>
				<p>一個月內跑超過 80 公里</p>
			</div>
			<div class="fourcol item">
			   <h3>
					<ul class="trophies">
						<li class="highmile highmile-platinum">
							<img src="./icon/trophy/59.png" style="position: relative; right: 20px; top: 5px;">
						</li>
					</ul>
					高里數白金獎杯
				</h3>
				<p>一個月內跑超過 160 公里</p>
			</div>
			<div class="fourcol item">
			   <h3>
					<ul class="trophies">
						<li class="run-more-often">
							<img src="./icon/trophy/35.png" style="position: relative; right: 20px; top: 5px;">
						</li>
					</ul>
					增加跑步頻率
				</h3>
				<p>在 X 週內跑 X 次</p>
			</div>
			<div class="fourcol item">
			   <h3>
					<ul class="trophies">
						<li class="run-more-miles">
							<img src="./icon/trophy/34.png" style="position: relative; right: 20px; top: 5px;">
						</li>
					</ul>
					跑更多公里數
				</h3>
				<p>在 X 週內跑 X 公里</p>
			</div>
			<div class="fourcol item">
			   <h3>
					<ul class="trophies">
						<li class="run-fuel-up">
							<img src="./icon/trophy/15.png" style="position: relative; right: 20px; top: 5px;">
						</li>
					</ul>
					我沒運動
				</h3>
				<p>總路程低於 0.5 公里</p>
			</div>
			<div class="fourcol item">
			   <h3>
					<ul class="trophies">
						<li class="run-burn-calories">
							<img src="./icon/trophy/40.png" style="position: relative; right: 20px; top: 5px;">
						</li>
					</ul>
					燃燒卡路里
				</h3>
				<p>在 X 週內燃燒 X 卡路里</p>
			</div>
			<div class="fourcol item">
			   <h3>
					<ul class="trophies">
						<li class="social-celebrity">
							<img src="./icon/trophy/87.png" style="position: relative; right: 20px; top: 5px;">
						</li>
					</ul>
					懶得運動
				</h3>
				<p>某次路程低於 0.5 公里</p>
			</div>
			<div class="fourcol item">
			   <h3>
					<ul class="trophies">
						<li class="delight-double-pleasure">
							<img src="./icon/trophy/44.png" style="position: relative; right: 15px; top: 5px;">
						</li>
					</ul>
					兩倍的努力
				</h3>
				<p>一天內跑了兩次</p>
			</div>
			<div class="fourcol item">
			   <h3>
					<ul class="trophies">
						<li class="delight-resolution">
							<img src="./icon/trophy/24.png" style="position: relative; right: 15px; top: 5px;">
						</li>
					</ul>
					決心
				</h3>
				<p>在新年跑步</p>
			</div>
			<div class="fourcol item">
			   <h3>
					<ul class="trophies">
						<li class="delight-el-fiesta">
							<img src="./icon/trophy/19.png" style="position: relative; right: 15px; top: 5px;">
						</li>
					</ul>
					真不科學
				</h3>
				<p>在五月節中依然保持活躍</p>
			</div>
			<div class="fourcol item">
			   <h3>
					<ul class="trophies">
						<li class="delight-zombie">
							<img src="./icon/trophy/23.png" style="position: relative; right: 15px; top: 5px;">
						</li>
					</ul>
					南瓜燈
				</h3>
				<p>在萬聖節運動</p>
			</div>
			<div class="fourcol item">
			   <h3>
					<ul class="trophies">
						<li class="delight-extra-frosting">
							<img src="./icon/trophy/124.png" style="position: relative; right: 15px; top: 5px;">
						</li>
					</ul>
					額外的驚喜
				</h3>
				<p>在生日當天運動</p>
			</div>
			<div class="fourcol item">
			   <h3>
					<ul class="trophies">
						<li class="delight-early-bird">
							<img src="./icon/trophy/69.png" style="position: relative; right: 15px; top: 5px;">
						</li>
					</ul>
					早起的鳥兒有蟲吃
				</h3>
				<p>在清晨 3 點到 6 點間進行過 5 次路跑</p>
			</div>
			<div class="fourcol item">
			   <h3>
					<ul class="trophies">
						<li class="delight-sleepwalker">
							<img src="./icon/trophy/17.png" style="position: relative; right: 15px; top: 5px;">
						</li>
					</ul>
					夜深人靜的夜晚
				</h3>
				<p>在凌晨 12 到 3 點之間跑了 5 次</p>
			</div>
			<div class="fourcol item">
			   <h3>
					<ul class="trophies">
						<li class="frequency-3-in-wk">
							<img src="./icon/trophy/26.png" style="position: relative; right: 15px; top: 5px;">
						</li>
					</ul>
					一週三次
				</h3>
				<p>在本週運動 3 次</p>
			</div>
			<div class="fourcol item">
			   <h3>
					<ul class="trophies">
						<li class="frequency-5-in-wk">
							<img src="./icon/trophy/27.png" style="position: relative; right: 15px; top: 5px;">
						</li>
					</ul>
					一週五次
				</h3>
				<p>在本週運動 5 次</p>
			</div>
			<div class="fourcol item">
			   <h3>
					<ul class="trophies">
						<li class="frequency-7-in-wk">
							<img src="./icon/trophy/28.png" style="position: relative; right: 15px; top: 5px;">
						</li>
					</ul>
					一週七次
				</h3>
				<p>在本週運動 7 次</p>
			</div>
			<div class="fourcol item">
			   <h3>
					<ul class="trophies">
						<li class="streak-3-weeks">
							<img src="./icon/trophy/29.png" style="position: relative; right: 15px; top: 5px;">
						</li>
					</ul>
					連續三週
				</h3>
				<p>連續運動 3 週</p>
			</div>
			<div class="fourcol item">
			   <h3>
					<ul class="trophies">
						<li class="streak-4-weeks">
							<img src="./icon/trophy/30.png" style="position: relative; right: 15px; top: 5px;">
						</li>
					</ul>
					連續四週
				</h3>
				<p>連續運動 4 週</p>
			</div>
			<div class="fourcol item">
			   <h3>
					<ul class="trophies">
						<li class="streak-5-weeks">
							<img src="./icon/trophy/36.png" style="position: relative; right: 15px; top: 5px;">
						</li>
					</ul>
					連續五週
				</h3>
				<p>連續運動 5 週</p>
			</div>
			<div class="fourcol item">
			   <h3>
					<ul class="trophies">
						<li class="streak-6-weeks">
							<img src="./icon/trophy/31.png" style="position: relative; right: 15px; top: 5px;">
						</li>
					</ul>
					連續六週
				</h3>
				<p>連續運動 6 週</p>
			</div>
			<div class="fourcol item">
			   <h3>
					<ul class="trophies">
						<li class="streak-3-months">
							<img src="./icon/trophy/32.png" style="position: relative; right: 15px; top: 5px;">
						</li>
					</ul>
					連續三個月
				</h3>
				<p>連續運動 3 個月</p>
			</div>
			<div class="fourcol item">
			   <h3>
					<ul class="trophies">
						<li class="streak-6-months">
							<img src="./icon/trophy/37.png" style="position: relative; right: 15px; top: 5px;">
						</li>
					</ul>
					連續六個月
				</h3>
				<p>連續運動 6 個月</p>
			</div>
			<div class="fourcol item">
			   <h3>
					<ul class="trophies">
						<li class="streak-9-months">
							<img src="./icon/trophy/38.png" style="position: relative; right: 15px; top: 5px;">
						</li>
					</ul>
					連續九個月
				</h3>
				<p>連續運動 9 個月</p>
			</div>
			<div class="fourcol item">
			   <h3>
					<ul class="trophies">
						<li class="streak-12-months">
							<img src="./icon/trophy/39.png" style="position: relative; right: 15px; top: 5px;">
						</li>
					</ul>
					連續十二個月
				</h3>
				<p>連續運動 12 個月</p>
			</div>
			<div class="fourcol item">
			   <h3>
					<ul class="trophies">
						<li class="frequency-all-mo">
							<img src="./icon/trophy/76.png" style="position: relative; right: 15px; top: 5px;">
						</li>
					</ul>
					2015 運動大神
				</h3>
				<p>在今年中，每個月都在運動</p>
			</div>
			<div class="fourcol item">
			   <h3>
					<ul class="trophies">
						<li class="pr_hyperwarm">
							<img src="./icon/trophy/119.png" style="position: relative; right: 15px; top: 5px;">
						</li>
					</ul>
					使命必達
				</h3>
				<p class="unknown">無時無刻都在運動，您的使命就是運動</p>
			</div>
			<div class="fourcol item">
			   <h3>
					<ul class="trophies">
						<li class="leaderboard-speed-demon">
							<img src="./icon/trophy/83.png" style="position: relative; right: 15px; top: 5px;">
						</li>
					</ul>
					天蠶腿
				</h3>
				<p>一小時內的平均速度超過 15 公里</p>
			</div>
			<div class="fourcol item">
			   <h3>
					<ul class="trophies">
						<li class="leaderboard-speed-demon">
							<img src="./icon/trophy/42.png" style="position: relative; right: 15px; top: 5px;">
						</li>
					</ul>
					走火入魔
				</h3>
				<p>在山洞中運動</p>
			</div>
			<div class="fourcol item">
			   <h3>
					<ul class="trophies">
						<li class="leaderboard-speed-demon">
							<img src="./icon/trophy/95.png" style="position: relative; right: 15px; top: 5px;">
						</li>
					</ul>
					任人宰割
				</h3>
				<p>在深山運動時遇到熊</p>
			</div>
		</div>
	</div>
</div>
</div>
<div class="apcss-footer">
    <div class="container">
        <div class="row apcss-footer-first-section">
            <div class="col-xs-12 col-sm-3 hidden-xs">
                <p class="apcss-footer-title">新建任務</p>
                    <a ga-click="Create Event" href="http://www.yuntech.edu.tw/" class="apcss-footer-content" target="_blank">如何開始第一個任務？</a>
                <a ga-click="FAQ" href="http://www.yuntech.edu.tw/" class="apcss-footer-content" target="_blank">常見問題</a>
            </div>
            <div class="col-xs-12 col-sm-3 hidden-xs">
                <p class="apcss-footer-title">參與任務</p>
                <a ga-click="Features" href="http://www.yuntech.edu.tw/" class="apcss-footer-content" target="_blank">功能介紹</a>
                
                <a ga-click="Event News" href="http://www.yuntech.edu.tw/" class="apcss-footer-content" target="_blank">任務新鮮事</a>
            </div>
            <div class="col-xs-12 col-sm-3 hidden-xs">
                <p class="apcss-footer-title">關於我們</p>
                <a ga-click="Join Us" href="http://www.yuntech.edu.tw/" class="apcss-footer-content" target="_blank">加入我們</a>
                <a ga-click="Terms of Service" href="http://www.yuntech.edu.tw/" class="apcss-footer-content" target="_blank">服務條款</a>
                    <a ga-click="Partner" href="http://www.yuntech.edu.tw/" class="apcss-footer-content" target="_blank">合作夥伴</a>
            </div>
            <div class="col-xs-12 col-sm-3 apcss-footer-phone-section2">
                <address>
                    <p class="apcss-footer-title">Supermen服務熱線</p>
                    <p class="apcss-footer-content">雲林斗六 : +886 1-1234-5678</p>
                    <p class="apcss-footer-content">週一至週五 09:00-18:00</p>
                    <a ga-click="Service Center" href="http://www.yuntech.edu.tw/" class="apcss-footer-customer-help" target="_blank" data-uv-lightbox="classic_widget" data-uv-mode="support" data-uv-primary-color="#0089d2" data-uv-link-color="#0089d2" data-uv-default-mode="support" data-uv-scanned="true" id="uv-1">
                        <span>客服小幫手</span>
                    </a>
                </address>
            </div>
        </div>
	    <a href="http://www.yuntech.edu.tw/" style="text-align: center; position: relative; Right: -160px; top: 0px;" class="apcss-logo" target="_blank">
	    	<h1>雲科大資管系團隊© Supermelly Inc. All Rights Reserved.</h1>
	    </a>
	</div>
</div>
</body>
</html>