/*
* STEPHEN D'ANDREA AND DAVE HITCHINGS WOULD LIKE 
* TO GIVE YOU THIS ADVICE FOR CLICK EVENTS:
*
* linkName - populates the pev2 value
*   - overwrites a value of '' in s.tl
* linkType - populates the pe value
*    - used to overwrite a value of 'o'
*    - link types are :
*        - 'o' - custom link
*        - 'd' - download link
*        - 'e' - exit link
*
* ALSO THIS IS A COOL LINK:
* http://microsite.omniture.com/t2/help/en_US/sc/implement/oms_sc_implement.pdf
*/

	// -- tracking:
	np.tracking = (function() {
		var _ = {};
		this.global_settings = {};
        this.is_mobile = np.settings.is_mobile,
        this.country_lc = np.settings.country.toLowerCase();
        var loaded = false;

		var init = function() {
			// if the global s is undefined it means tealium didn't load, lets create a fake object so the rest of the tracking doesn't fall over
			if (!window.s) {
				window.s = {
					"linkTrackVars" : ""
				};
			}
			s.linkTrackEvents = s.events;
			if( typeof s != "undefined" && s.linkTrackVars.indexOf("prop18") == -1 )
				s.linkTrackVars += ",prop18";
			if( typeof s != "undefined" && s.linkTrackVars.indexOf("prop21") == -1 )
				s.linkTrackVars += ",prop21";
			if( typeof s != "undefined" && s.linkTrackVars.indexOf("events") == -1 )
				s.linkTrackVars += ",events";
			if( typeof s != "undefined" && s.linkTrackVars.indexOf("eVar61") == -1 )
				s.linkTrackVars += ",eVar61";

			loaded = true;
			build_global_variable();
		};

		var build_global_variable = function() {
			this.global_settings = {};
			this.global_settings.account	= (window.location.hostname.indexOf("nike") !== -1 && window.location.hostname.indexOf("ecn") === -1) ? "nikecomprod" : "nikecomdev" ;
			this.global_settings.server	= this.global_settings.account;
			this.global_settings.channel = "nikeplus";
			this.global_settings.prop2 = "nikecom";
			this.global_settings.prop10 = "digital sport";
			this.global_settings.prop14 = this.country_lc;
			this.global_settings.prop15 = np.settings.locale.toLowerCase().substr(0,2);
			update_login_state_setting();
		};

		_.get_global_settings = function() {
			return this.global_settings;
		};

		// -- public method to track a click event:
		_.track_click_event = function(d) {
			if(!loaded){
				setTimeout(
					function(){
						_.track_click_event(d)
					}, 1000);
			}
			if( !np.util.get_property(np , "user_settings.cookies.eu_c_fp" , false) ){
				return;
			}
				
			d = fix_click_data(d);
			d.pageName = np.tracking.curPage;
			
			if( typeof d.events != "undefined")
				s.events = d.events;
			if( typeof d.eVar61 != "undefined")
				s.eVar61 = d.eVar61;
			
			fire_tracking_call(d, "click");
			
			if( typeof s != "undefined" )
				s.events = s.eVar61 = null;
		
		};
		
		// -- public method to track a page event:
		_.track_page_event = function(d, bypass) {
			if(!loaded){
				setTimeout(
					function(){
						_.track_page_event(d, bypass)
					}, 1000);
				return;
			}

			if(np.user_settings){
				if( !np.util.get_property(np , "user_settings.cookies.eu_c_fp" , false) )
					return;
			}
			d = fix_page_data(d);
			np.tracking.curPage = d.pageName;
			if(np.shared_page && !bypass){
				
				var share_data_networks = np.util.get_property(np, "share_data.networks", []);
				if(share_data_networks){ 
					d.provider = (share_data_networks.indexOf("facebook") > -1)?"facebook":"twitter";
				}else{
					d.provider = "";
				}
				np.util.share.tracking(d,d.provider);
				fire_tracking_call(d, "page");
			}else{
				
				fire_tracking_call(d, "page");
			}
		};
		
		_.set_login_provider = function(provider) {
		    np.util.cookie.create('tracking_login_provider', provider);
		};
		
		var update_login_state_setting = function() {
		    this.global_settings.eVar4 = "not logged in";
			if ( np.settings.is_logged_in ) {
				this.global_settings.eVar4 = "logged in";
				this.global_settings.prop50 = np.util.get_property(np, "settings.user.records.upmId");
				var provider = np.util.cookie.get('tracking_login_provider');
				if (provider) {
				    this.global_settings.eVar4 = "logged in:" + provider;
				}
			}
		};

		var fire_tracking_call = function(d, type) {
			switch (type) {
			case "page" :
				if(window.s && window.s.t){
					s.t(d);
				}
			break;
			case "click" :           
				var click_type;
				if(window.s && window.s.tl){
					if(d.click_type){
						click_type = typeof d.click_type != "undefined" ? d.click_type : "";
					}

					s.tl(true, 'o', click_type, d);
				}
			break;
			}

		};

		var fix_click_data = function(d) {
            if (!("pev2" in d) && ("pageName" in d)) {
                d.pev2 = d.pageName;
            }
			d = fix_tracking_data(d);
			return d;
		};

		var fix_page_data = function(d) {
			d = fix_tracking_data(d);
			return d;
		};

		var fix_tracking_data = function(d) {
		    
            update_login_state_setting();
            
			d = $.extend(true, {}, this.global_settings, d);

			if(d.page_name)
				d.pageName = d.page_name;

			// -- Prepend site prefixes:
			d.pageName = "nikecom>plus>" + d.pageName;
			if(d.eVar13 && d.eVar13.indexOf(get_site_prefix()) === -1)
				d.eVar13 = get_site_prefix() + ">" + d.eVar13;
				
			if(d.linkName){
				// set prop3 to linkName property passed in through track_page_event and prepend plus:
				if(d.prop17 == "nav" || d.prop17 == "search" || d.prop17 == "login")
					d.prop3 = "nav: " + d.linkName;
				else
					d.prop3 = "plus: " + d.linkName;
				// remove spaces from either side of colons
				d.prop3 = d.prop3.replace(/: /g, ":");
				d.prop3 = d.prop3.replace(/ :/g, ":");
				var p3 = d.prop3.split(":");
				if(p3[0] == "nav")
					d.linkName = "navigation link";
				else
					d.linkName = "Body page link";
			}
			
			if(d.pev2 && d.pev2.indexOf(get_site_prefix()) === -1)
				d.pev2 = get_site_prefix() + ">" + d.pev2;
            if(d.prop17)
				d.prop17 = "plus:" + d.prop17;
			return d;
		};

		var get_site_prefix = function() {
            var site_prefix = (!this.is_mobile) ? this.country_lc + "1p" : this.country_lc + "1pmo";
            return site_prefix;
		};
		
		if( np.user_settings.cookies.eu_c_fp == true ){
			if( np.settings.env_name == "prod" || np.settings.env_name == "prd" ) {
				np.util.require_script('//tags.tiqcdn.com/utag/nike/plus/prod/utag.js', init.bind(this));
			} else if ( np.settings.env_name == "ecn29" ) {
				np.util.require_script('//tags.tiqcdn.com/utag/nike/plus/qa/utag.js', init.bind(this));
			} else {
				np.util.require_script('//tags.tiqcdn.com/utag/nike/plus/int/utag.js', init.bind(this));
			}
		}else{
			init();
		}
		
		return _;
	
	})();
