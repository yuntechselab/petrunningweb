﻿<%@ page language="java" import="com.supermen.servlet.controller.GlobalVariable" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page import="java.sql.*"%>
<!DOCTYPE html>
<html ng-app="accupass" class="js flexbox flexboxlegacy canvas canvastext webgl no-touch geolocation postmessage websqldatabase indexeddb hashchange history draganddrop websockets rgba hsla multiplebgs backgroundsize borderimage borderradius boxshadow textshadow opacity cssanimations csscolumns cssgradients cssreflections csstransforms csstransforms3d csstransitions fontface generatedcontent video audio localstorage sessionstorage webworkers applicationcache svg inlinesvg smil svgclippaths ng-scope">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <title>Supermen</title>
    <link rel="Shortcut Icon" type="image/x-icon" href="./icon/shortcut.png" />
    <link type="text/css" rel="stylesheet" href="./register_files/css.min.css">
    <script type="application/javascript" async="" defer="" src="./forgetPassword_files/track.js"></script>
    <script type="text/javascript" async="" src="./forgetPassword_files/26hI9f54AQMRNaPo1MvFkw.js"></script>
    <script type="text/javascript" async="" src="./forgetPassword_files/atrk.js"></script>
    <script type="text/javascript" src="./forgetPassword_files/js.min.js"></script>
    <script type="text/javascript" src="./forgetPassword_files/resource"></script>

<style type="text/css" media="screen">.uv-icon{-moz-box-sizing:border-box;-webkit-box-sizing:border-box;box-sizing:border-box;display:inline-block;cursor:pointer;position:relative;-moz-transition:all 300ms;-o-transition:all 300ms;-webkit-transition:all 300ms;transition:all 300ms;width:39px;height:39px;position:fixed;z-index:100002;opacity:0.8;-moz-transition:opacity 100ms;-o-transition:opacity 100ms;-webkit-transition:opacity 100ms;transition:opacity 100ms}.uv-icon.uv-bottom-right{bottom:10px;right:12px}.uv-icon.uv-top-right{top:10px;right:12px}.uv-icon.uv-bottom-left{bottom:10px;left:12px}.uv-icon.uv-top-left{top:10px;left:12px}.uv-icon.uv-is-selected{opacity:1}.uv-icon svg{width:39px;height:39px}.uv-popover{font-family:sans-serif;font-weight:100;font-size:13px;color:black;position:fixed;z-index:100001}.uv-popover-content{-moz-border-radius:5px;-webkit-border-radius:5px;border-radius:5px;background:white;position:relative;width:325px;height:325px;-moz-transition:background 200ms;-o-transition:background 200ms;-webkit-transition:background 200ms;transition:background 200ms}.uv-bottom .uv-popover-content{-moz-box-shadow:rgba(0,0,0,0.3) 0 -10px 60px,rgba(0,0,0,0.1) 0 0 20px;-webkit-box-shadow:rgba(0,0,0,0.3) 0 -10px 60px,rgba(0,0,0,0.1) 0 0 20px;box-shadow:rgba(0,0,0,0.3) 0 -10px 60px,rgba(0,0,0,0.1) 0 0 20px}.uv-top .uv-popover-content{-moz-box-shadow:rgba(0,0,0,0.3) 0 10px 60px,rgba(0,0,0,0.1) 0 0 20px;-webkit-box-shadow:rgba(0,0,0,0.3) 0 10px 60px,rgba(0,0,0,0.1) 0 0 20px;box-shadow:rgba(0,0,0,0.3) 0 10px 60px,rgba(0,0,0,0.1) 0 0 20px}.uv-left .uv-popover-content{-moz-box-shadow:rgba(0,0,0,0.3) 10px 0 60px,rgba(0,0,0,0.1) 0 0 20px;-webkit-box-shadow:rgba(0,0,0,0.3) 10px 0 60px,rgba(0,0,0,0.1) 0 0 20px;box-shadow:rgba(0,0,0,0.3) 10px 0 60px,rgba(0,0,0,0.1) 0 0 20px}.uv-right .uv-popover-content{-moz-box-shadow:rgba(0,0,0,0.3) -10px 0 60px,rgba(0,0,0,0.1) 0 0 20px;-webkit-box-shadow:rgba(0,0,0,0.3) -10px 0 60px,rgba(0,0,0,0.1) 0 0 20px;box-shadow:rgba(0,0,0,0.3) -10px 0 60px,rgba(0,0,0,0.1) 0 0 20px}.uv-ie8 .uv-popover-content{position:relative}.uv-ie8 .uv-popover-content .uv-popover-content-shadow{display:block;background:black;content:'';position:absolute;left:-15px;top:-15px;width:100%;height:100%;filter:progid:DXImageTransform.Microsoft.Blur(PixelRadius=15,MakeShadow=true,ShadowOpacity=0.30);z-index:-1}.uv-popover-tail{border:9px solid transparent;width:0;z-index:10;position:absolute;-moz-transition:border-top-color 200ms;-o-transition:border-top-color 200ms;-webkit-transition:border-top-color 200ms;transition:border-top-color 200ms}.uv-top .uv-popover-tail{bottom:-20px;border-top:11px solid white}.uv-bottom .uv-popover-tail{top:-20px;border-bottom:11px solid white}.uv-left .uv-popover-tail{right:-20px;border-left:11px solid white}.uv-right .uv-popover-tail{left:-20px;border-right:11px solid white}.uv-popover-loading{background:white;-moz-border-radius:5px;-webkit-border-radius:5px;border-radius:5px;position:absolute;width:100%;height:100%;left:0;top:0}.uv-popover-loading-text{position:absolute;top:50%;margin-top:-0.5em;width:100%;text-align:center}.uv-popover-iframe-container{height:100%}.uv-popover-iframe{-moz-border-radius:5px;-webkit-border-radius:5px;border-radius:5px;overflow:hidden}.uv-is-hidden{display:none}.uv-is-invisible{display:block !important;visibility:hidden !important}.uv-is-transitioning{display:block !important}.uv-no-transition{-moz-transition:none !important;-webkit-transition:none !important;-o-transition:color 0 ease-in !important;transition:none !important}.uv-fade{opacity:1;-moz-transition:opacity 200ms ease-out;-o-transition:opacity 200ms ease-out;-webkit-transition:opacity 200ms ease-out;transition:opacity 200ms ease-out}.uv-fade.uv-is-hidden{opacity:0}.uv-scale-top,.uv-scale-top-left,.uv-scale-top-right,.uv-scale-bottom,.uv-scale-bottom-left,.uv-scale-bottom-right,.uv-scale-right,.uv-scale-right-top,.uv-scale-right-bottom,.uv-scale-left,.uv-scale-left-top,.uv-scale-left-bottom,.uv-slide-top,.uv-slide-bottom,.uv-slide-left,.uv-slide-right{opacity:1;-moz-transition:all 80ms ease-out;-o-transition:all 80ms ease-out;-webkit-transition:all 80ms ease-out;transition:all 80ms ease-out}.uv-scale-top.uv-is-hidden{opacity:0;-moz-transform:scale(0.8) translateY(-15%);-ms-transform:scale(0.8) translateY(-15%);-webkit-transform:scale(0.8) translateY(-15%);transform:scale(0.8) translateY(-15%)}.uv-scale-top-left.uv-is-hidden{opacity:0;-moz-transform:scale(0.8) translateY(-15%) translateX(-10%);-ms-transform:scale(0.8) translateY(-15%) translateX(-10%);-webkit-transform:scale(0.8) translateY(-15%) translateX(-10%);transform:scale(0.8) translateY(-15%) translateX(-10%)}.uv-scale-top-right.uv-is-hidden{opacity:0;-moz-transform:scale(0.8) translateY(-15%) translateX(10%);-ms-transform:scale(0.8) translateY(-15%) translateX(10%);-webkit-transform:scale(0.8) translateY(-15%) translateX(10%);transform:scale(0.8) translateY(-15%) translateX(10%)}.uv-scale-bottom.uv-is-hidden{opacity:0;-moz-transform:scale(0.8) translateY(15%);-ms-transform:scale(0.8) translateY(15%);-webkit-transform:scale(0.8) translateY(15%);transform:scale(0.8) translateY(15%)}.uv-scale-bottom-left.uv-is-hidden{opacity:0;-moz-transform:scale(0.8) translateY(15%) translateX(-10%);-ms-transform:scale(0.8) translateY(15%) translateX(-10%);-webkit-transform:scale(0.8) translateY(15%) translateX(-10%);transform:scale(0.8) translateY(15%) translateX(-10%)}.uv-scale-bottom-right.uv-is-hidden{opacity:0;-moz-transform:scale(0.8) translateY(15%) translateX(10%);-ms-transform:scale(0.8) translateY(15%) translateX(10%);-webkit-transform:scale(0.8) translateY(15%) translateX(10%);transform:scale(0.8) translateY(15%) translateX(10%)}.uv-scale-right.uv-is-hidden{opacity:0;-moz-transform:scale(0.8) translateX(15%);-ms-transform:scale(0.8) translateX(15%);-webkit-transform:scale(0.8) translateX(15%);transform:scale(0.8) translateX(15%)}.uv-scale-right-top.uv-is-hidden{opacity:0;-moz-transform:scale(0.8) translateX(15%) translateY(-10%);-ms-transform:scale(0.8) translateX(15%) translateY(-10%);-webkit-transform:scale(0.8) translateX(15%) translateY(-10%);transform:scale(0.8) translateX(15%) translateY(-10%)}.uv-scale-right-bottom.uv-is-hidden{opacity:0;-moz-transform:scale(0.8) translateX(15%) translateY(10%);-ms-transform:scale(0.8) translateX(15%) translateY(10%);-webkit-transform:scale(0.8) translateX(15%) translateY(10%);transform:scale(0.8) translateX(15%) translateY(10%)}.uv-scale-left.uv-is-hidden{opacity:0;-moz-transform:scale(0.8) translateX(-15%);-ms-transform:scale(0.8) translateX(-15%);-webkit-transform:scale(0.8) translateX(-15%);transform:scale(0.8) translateX(-15%)}.uv-scale-left-top.uv-is-hidden{opacity:0;-moz-transform:scale(0.8) translateX(-15%) translateY(-10%);-ms-transform:scale(0.8) translateX(-15%) translateY(-10%);-webkit-transform:scale(0.8) translateX(-15%) translateY(-10%);transform:scale(0.8) translateX(-15%) translateY(-10%)}.uv-scale-left-bottom.uv-is-hidden{opacity:0;-moz-transform:scale(0.8) translateX(-15%) translateY(10%);-ms-transform:scale(0.8) translateX(-15%) translateY(10%);-webkit-transform:scale(0.8) translateX(-15%) translateY(10%);transform:scale(0.8) translateX(-15%) translateY(10%)}.uv-slide-top.uv-is-hidden{-moz-transform:translateY(-100%);-ms-transform:translateY(-100%);-webkit-transform:translateY(-100%);transform:translateY(-100%)}.uv-slide-bottom.uv-is-hidden{-moz-transform:translateY(100%);-ms-transform:translateY(100%);-webkit-transform:translateY(100%);transform:translateY(100%)}.uv-slide-left.uv-is-hidden{-moz-transform:translateX(-100%);-ms-transform:translateX(-100%);-webkit-transform:translateX(-100%);transform:translateX(-100%)}.uv-slide-right.uv-is-hidden{-moz-transform:translateX(100%);-ms-transform:translateX(100%);-webkit-transform:translateX(100%);transform:translateX(100%)}</style>
<script type="text/javascript" charset="UTF-8" src="./forgetPassword_files/common.js"></script>
<script type="text/javascript" charset="UTF-8" src="./forgetPassword_files/util.js"></script>
<script type="text/javascript" charset="UTF-8" src="./forgetPassword_files/stats.js"></script>
<script type="text/javascript" charset="UTF-8" src="./forgetPassword_files/AuthenticationService.Authenticate"></script>
</head>
<body>
<div class="APCSS_wrapper"></div>
<nav class="navbar navbar-default navbar-fixed-top APCSS_header" role="navigation">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
				<div class="navbar-header">
                    <!-- desktop version -->
                    <h1 class="accupass">
                        <a class="navbar-brand hidden-xs hidden-sm" href="index2.jsp" target="_self">
                            <img src="./icon/supermen.png" style="position: relative; width: 125px; height: 30px;">
                        </a>
                    </h1>
                    <form name="search" action="SearchEventServlet2" method="post" class="apcss-search-form ng-pristine ng-valid">
                        <div class="apcss-search-form-wrapper">
                            <input type="text" name="keyword" class="apcss-search-form-input" style="color:#FFFFFF" placeholder="找任務" onfocus="if(this.placeholder=='找任務'){this.placeholder=''};this.style.color='#FFFFFF';" onblur="if(this.placeholder==''||this.placeholder=='找任務'){this.placeholder='找任務';this.style.color='#FFFFFF';}">
                            <span class="apcss-search-form-button apcss-search-focus">
                            	<button type="submit" class="btn btn-primary ng-scope" style="position: relative; width: 38px; height: 38px; top: -6.5px; right: 12px;"><img src="./icon/search.png" style="position: relative; top: 0px; right: 1px;" width="14" height="14"></button>
                            </span>
                        </div>
                    </form>
                    <div class="dropdown apcss-dropdown-explore-activity" ng-class="hover">
                        <a class="apcss-dropdown-explore-activity-button" role="button" ng-mouseover="hover=&#39;open&#39;" ng-mouseleave="hover=&#39;&#39;" target="_blank">
                            <span>探索任務</span><img src="./icon/triangle.png" style="position: relative; top: -1px;">
                        </a>
                        <ul class="dropdown-menu apcss-dropdown-explore-activity-menu" ng-mouseleave="hover=&#39;&#39;" role="menu">
    
    <li role="presentation" class="dropdown-header">熱門主打</li>
        <li role="presentation">
            <a ga-click="Hit_1" role="menuitem" tabindex="-1" href="http://www.yuntech.edu.tw/" title="五月跑步約起來" target="_blank">
                <img src="./icon/banner1.jpg">
            </a>
        </li>
        <li role="presentation">
            <a ga-click="Hit_2" role="menuitem" tabindex="-1" href="http://www.yuntech.edu.tw/" title="小翼貓的一天" target="_blank">
                <img src="./icon/banner2.jpg">
            </a>
        </li>
        <li role="presentation" class="dropdown-header">時間</li>
        <li role="presentation">
            <div class="row">
            	<a class="col-md-3 menuitem" href="SearchEventServlet2?keyword=<%=GlobalVariable.keyword %>&date=1" title="今日" target="_self">今日</a>
              	<a class="col-md-3 menuitem" href="SearchEventServlet2?keyword=<%=GlobalVariable.keyword %>&date=2" title="明日" target="_self">明日</a>
             	<a class="col-md-3 menuitem" href="SearchEventServlet2?keyword=<%=GlobalVariable.keyword %>&date=3" title="本週" target="_self">本週</a>
               	<a class="col-md-3 menuitem" href="SearchEventServlet2?keyword=<%=GlobalVariable.keyword %>&date=4" title="本月" target="_self">本月</a>
            </div>
        </li>
</ul>
                    </div>
                </div>
                <div class="collapse navbar-collapse" id="APJS_id-userOpt">
                    <div class="apcss-userinfo-box navbar-right hidden-xs hidden-sm">
                        <div id="APJS_spinjs" class="APCSS_spinner" ap-page-load=""><div class="rect1"></div><div class="rect2"></div><div class="rect3"></div><div class="rect4"></div><div class="rect5"></div></div>
                            <div class="dropdown">
                                <a class="apcss-dropdown-user-button" id="APJS_dropdown-user" role="button" data-toggle="dropdown" data-target="#">
                                    <div class="apcss-avatar">
                                        <img src="./index2_files/default_user.png">
                                    </div>
                                    <img src="./icon/triangle.png" style="position: relative; left: 6px; top: -1px;">
                                </a>
                                <ul class="dropdown-menu" role="menu" aria-labelledby="APJS_dropdown-user">
                                    <li role="presentation" class="dropdown-header">參加任務</li>
                                    <li role="presentation"><a ga-click="User" role="menuitem" tabindex="-1" href="MyDetailServlet" target="_self">我的個人頁面</a></li>
                                    <li role="presentation"><a ga-click="My Tickets" role="menuitem" tabindex="-1" href="MyTicketServlet" target="_self">我的票劵</a></li>
                                    <li role="presentation"><a ga-click="My Tickets" role="menuitem" tabindex="-1" href="mySport.jsp" target="_self">我的運動記錄</a></li>
                                    <li role="presentation"><a ga-click="My Tickets" role="menuitem" tabindex="-1" href="myAchievement.jsp" target="_self">我的成就記錄</a></li>
                                    <li role="presentation" class="divider"></li>
                                    <li role="presentation" class="dropdown-header">主辦任務</li>
                                    <li role="presentation"><a ga-click="My Events" role="menuitem" tabindex="-1" href="ListEventServlet" target="_self">我登記的任務</a></li>
                                    <li role="presentation" class="divider"></li>
                                    <li role="presentation" class="dropdown-header">帳號設定</li>
                                    <li role="presentation"><a ga-click="Edit My Profile" role="menuitem" tabindex="-1" href="UpdateProfileServlet" target="_self">修改個人資料</a></li>
                                    <li role="presentation" class="divider"></li>
									<li role="presentation"><a ga-click="Log Out" role="menuitem" tabindex="-1" href="index.jsp" target="_self">登出</a></li>
								</ul>
                        	</div>
                        	<a ga-click="Create_events" class="apcss-create-activity-button" href="ListEventServlet" target="_self">新任務</a>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>                                                     
                                                                      </nav>
<div class="APCSS_page-banner" ga-config-category="Index">
    <div class="container">
        <div class="row">
            
            <div class="col-xs-12 col-sm-12 col-md-12" ga-hover="Banner">
                <div class="jcarousel-wrapper" ga-index-banner="">
                    <div class="jcarousel hidden-xs hidden-sm" ap-jcarousel="" data-jcarousel="true" data-jcarouselautoscroll="true">
                        <ul style="left: -6160px; top: 0px;">
                        <li><img src="./index_files/banner_01.jpg"></li>
                        <li><img src="./index_files/banner_02.jpg"></li>
                        <li><img src="./index_files/banner_03.jpg"></li>
                        <li><img src="./index_files/banner_04.jpg"></li>
                        <li><img src="./index_files/banner_05.jpg"></li>
                        <li><img src="./index_files/banner_06.jpg"></li>
                        <li><img src="./index_files/banner_07.jpg"></li>
                        <li><img src="./index_files/banner_08.jpg"></li>
                        <li><img src="./index_files/banner_09.jpg"></li>
                        <li><img src="./index_files/banner_10.jpg"></li>
                        </ul>
                        <!-- Controls -->
                        <a class="jcarousel-control-prev" data-jcarouselcontrol="true"><img src="./icon/left.png" style="position: relative; top: 0px;" width="8" height="8"></a>
                        <a class="jcarousel-control-next" data-jcarouselcontrol="true"><img src="./icon/right.png" style="position: relative; top: 0px;" width="8" height="8"></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="APCSS_page-index" ap-home-index="">
    <div class="container">
        <div class="row" ga-hover="Featured" ga-event-item="">
            <div class="col-xs-12">
                <h3>為您推薦</h3>
            </div>
<% 
	Connection conn = null;
	Statement sta = null;
	ResultSet rs = null;
	 
	try {
		Class.forName("com.mysql.jdbc.Driver");
		String url = "jdbc:mysql://localhost:3306/supermen_db?useUnicode=true&characterEncoding=UTF-8";
		conn = DriverManager.getConnection(url, "root", "1234");
		sta = conn.createStatement();
		
		String sql = "SELECT * FROM event ORDER BY startDate ASC"; 
		rs = sta.executeQuery(sql);
		while (rs.next()) {
%>
		<div class="col-xs-12 col-sm-6 col-md-4 ng-scope">
		    <div class="APCSS_linkBox-event">
		        <div class="img">
		            <a href="BrowseEventServlet2?no=<%=rs.getString("no") %>" target="_self"><img src="./upload/<%=rs.getString("image") %>" width="100%" height="100%" class="img-reaponsive" alt=""></a>
		        </div>
		        <a href="BrowseEventServlet2?no=<%=rs.getString("no") %>" class="tit" target="_self"><strong><%=rs.getString("head") %></strong></a>
		        <div class="row">
		            <div class="col-xs-7 col-sm-7 col-md-7">
		                <span class="org">
		                    <img src="./upload/<%=rs.getString("image") %>" width="30" height="30" alt="" class="img-org">
		                    <%=rs.getString("organizerName") %>
		                </span>
		            </div>
		            <div class="col-xs-5 col-sm-5 col-md-5">
		                <span class="time">
		                    <img src="./icon/clock.png" style="position: relative; top: -1px;" width="12" height="12">
		                    <%=rs.getString("startDate") %>
		                </span>
		            </div>
		        </div>
		        <div class="infoBar">
		            <span class="browse" ap-tooltip="瀏覽人次" data-placement="bottom" tabindex="-1" data-original-title="" title="">
		                <img src="./icon/browse.png" style="position: relative; top: -1px;" width="15" height="15">
		                <%=rs.getString("view") %>
		            </span>
		        </div>
		    </div>
		</div>
<%
		}
	} catch (ClassNotFoundException e) {
    	e.printStackTrace();
  	} catch (SQLException e) {
    	e.printStackTrace();
 	} finally {
		conn.close();
		conn = null;
		
		sta.close();
		sta = null;
		
		rs.close();
		rs = null;
	}
%>
</div>
<div class="row">
	<div class="col-xs-12">
                <h3>
                    <a href="http://www.yuntech.edu.tw/" class="pull-right" target="_blank">
                        更多
                    </a>
                    任務新鮮事
                </h3>
            </div>
            
                <div class="col-xs-12 col-sm-6">
                    <a href="http://www.yuntech.edu.tw/" class="APCSS_linkBox-article" target="_blank">
                        <img src="./icon/fun_01.jpg" width="100%" alt="" class="img-responsive">
                        <span>減肥別再找藉口　4種常見理由與解決方法</span>
                    </a>
                </div>
                <div class="col-xs-12 col-sm-6">
                    <a href="http://www.yuntech.edu.tw/" class="APCSS_linkBox-article" target="_blank">
                        <img src="./icon/fun_02.jpg" width="100%" alt="" class="img-responsive">
                        <span>運動完好累？快速解除疲勞四要訣</span>
                    </a>
                </div>
       </div>
    </div>
</div>
<div class="apcss-footer">
    <div class="container">
        <div class="row apcss-footer-first-section">
            <div class="col-xs-12 col-sm-3 hidden-xs">
                <p class="apcss-footer-title">新建任務</p>
                    <a ga-click="Create Event" href="http://www.yuntech.edu.tw/" class="apcss-footer-content" target="_blank">如何開始第一個任務？</a>
                <a ga-click="FAQ" href="http://www.yuntech.edu.tw/" class="apcss-footer-content" target="_blank">常見問題</a>
            </div>
            <div class="col-xs-12 col-sm-3 hidden-xs">
                <p class="apcss-footer-title">參與任務</p>
                <a ga-click="Features" href="http://www.yuntech.edu.tw/" class="apcss-footer-content" target="_blank">功能介紹</a>
                
                <a ga-click="Event News" href="http://www.yuntech.edu.tw/" class="apcss-footer-content" target="_blank">任務新鮮事</a>
            </div>
            <div class="col-xs-12 col-sm-3 hidden-xs">
                <p class="apcss-footer-title">關於我們</p>
                <a ga-click="Join Us" href="http://www.yuntech.edu.tw/" class="apcss-footer-content" target="_blank">加入我們</a>
                <a ga-click="Terms of Service" href="http://www.yuntech.edu.tw/" class="apcss-footer-content" target="_blank">服務條款</a>
                    <a ga-click="Partner" href="http://www.yuntech.edu.tw/" class="apcss-footer-content" target="_blank">合作夥伴</a>
            </div>
            <div class="col-xs-12 col-sm-3 apcss-footer-phone-section2">
                <address>
                    <p class="apcss-footer-title">Supermen服務熱線</p>
                    <p class="apcss-footer-content">雲林斗六 : +886 1-1234-5678</p>
                    <p class="apcss-footer-content">週一至週五 09:00-18:00</p>
                    <a ga-click="Service Center" href="http://www.yuntech.edu.tw/" class="apcss-footer-customer-help" target="_blank" data-uv-lightbox="classic_widget" data-uv-mode="support" data-uv-primary-color="#0089d2" data-uv-link-color="#0089d2" data-uv-default-mode="support" data-uv-scanned="true" id="uv-1">
                        <span>客服小幫手</span>
                    </a>
                </address>
            </div>
        </div>
	    <a href="http://www.yuntech.edu.tw/" style="text-align: center; position: relative; Right: -160px; top: 0px;" class="apcss-logo" target="_blank">
	    	<h1>雲科大資管系團隊© Supermelly Inc. All Rights Reserved.</h1>
	    </a>
	</div>
</div>
</body>
</html>