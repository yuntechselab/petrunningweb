package com.supermen.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.supermen.database.model.DataBaseManager;
import com.supermen.vo.MemberVo;

public class MemberDao {
	public void signMember(MemberVo sign) {
		DataBaseManager dbmanage = new DataBaseManager();
		Connection conn = null;
		Statement sta = null;

		try {
			conn = dbmanage.initDB();
			sta = conn.createStatement();
			String sql = "INSERT INTO member(name, email, sex, code, phone, password, pet) VALUES('"
					+ sign.getMemberName() + "','" + sign.getMemberEmail() + "','" + sign.getMemberSex() + "','"
					+ sign.getMemberCode() + "','" + sign.getMemberPhone() + "','" + sign.getMemberPassword() + "','"
					+ sign.getMemberPet() + "')";
			sta.executeUpdate(sql);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			dbmanage.closeDB(sta, conn);
		}
	}

	public MemberVo forgetPasswordMember(String email, String name) {
		DataBaseManager dbmanage = new DataBaseManager();
		Connection conn = null;
		Statement sta = null;
		ResultSet rs = null;
		MemberVo member = null;

		try {
			conn = dbmanage.initDB();
			sta = conn.createStatement();
			String sql = "SELECT * FROM member WHERE email = " + "'" + email + "'" + " AND name = " + "'" + name + "'";

			rs = sta.executeQuery(sql);
			while (rs.next()) {
				member = new MemberVo();
				member.setMemberPassword(rs.getString("password"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			dbmanage.closeDB(rs, sta, conn);
		}

		return member;
	}

	public static boolean validateHaveMember(String email, String name) {
		DataBaseManager dbmanage = new DataBaseManager();
		Connection conn = null;
		Statement sta = null;
		ResultSet rs = null;
		boolean status = false;

		try {
			conn = dbmanage.initDB();
			sta = conn.createStatement();
			String sql = "SELECT * FROM member WHERE email = " + "'" + email + "'" + " AND name = " + "'" + name + "'";

			rs = sta.executeQuery(sql);
			status = rs.next();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			dbmanage.closeDB(rs, sta, conn);
		}

		return status;
	}

	public static boolean loginMember(String email, String password) {
		DataBaseManager dbmanage = new DataBaseManager();
		Connection conn = null;
		Statement sta = null;
		ResultSet rs = null;
		boolean status = false;

		try {
			conn = dbmanage.initDB();
			sta = conn.createStatement();
			String sql = "SELECT * FROM member WHERE email = " + "'" + email + "'" + " AND password = " + "'" + password
					+ "'";

			rs = sta.executeQuery(sql);
			status = rs.next();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			dbmanage.closeDB(rs, sta, conn);
		}

		return status;
	}

	public static boolean validateMember(String email) {
		DataBaseManager dbmanage = new DataBaseManager();
		Connection conn = null;
		Statement sta = null;
		ResultSet rs = null;
		boolean status = false;

		try {
			conn = dbmanage.initDB();
			sta = conn.createStatement();
			String sql = "SELECT * FROM member WHERE email = " + "'" + email + "'";

			rs = sta.executeQuery(sql);
			status = rs.next();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			dbmanage.closeDB(rs, sta, conn);
		}

		return status;
	}

	public void updateProfile(MemberVo member) {
		DataBaseManager dbmanage = new DataBaseManager();
		Connection conn = null;
		Statement sta = null;

		try {
			conn = dbmanage.initDB();
			sta = conn.createStatement();
			String sql = "UPDATE member SET name = '" + member.getMemberName() + "', password = '"
					+ member.getMemberPassword() + "', code = '" + member.getMemberCode() + "', phone = '"
					+ member.getMemberPhone() + "', nickname = '" + member.getMemberNickname() + "', sex = '"
					+ member.getMemberSex() + "', date = '" + member.getMemberDate() + "', id = '"
					+ member.getMemberId() + "', intro = '" + member.getMemberIntro() + "' WHERE email = " + "'"
					+ member.getMemberEmail() + "'";
			;
			sta.executeUpdate(sql);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			dbmanage.closeDB(sta, conn);
		}
	}

	public MemberVo profileMember(String email) {
		DataBaseManager dbmanage = new DataBaseManager();
		Connection conn = null;
		Statement sta = null;
		ResultSet rs = null;
		MemberVo member = null;

		try {
			conn = dbmanage.initDB();
			sta = conn.createStatement();
			String sql = "SELECT * FROM member WHERE email = " + "'" + email + "'";

			rs = sta.executeQuery(sql);
			while (rs.next()) {
				member = new MemberVo();
				member.setMemberEmail(rs.getString("email"));
				member.setMemberName(rs.getString("name"));
				member.setMemberPassword(rs.getString("password"));
				member.setMemberCode(rs.getString("code"));
				member.setMemberPhone(rs.getString("phone"));
				member.setMemberNickname(rs.getString("nickname"));
				member.setMemberSex(rs.getString("sex"));
				member.setMemberDate(rs.getString("date"));
				member.setMemberId(rs.getString("id"));
				member.setMemberIntro(rs.getString("intro"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			dbmanage.closeDB(rs, sta, conn);
		}

		return member;
	}

	public MemberVo myDetailMember(String email) {
		DataBaseManager dbmanage = new DataBaseManager();
		Connection conn = null;
		Statement sta = null;
		ResultSet rs = null;
		MemberVo member = null;

		try {
			conn = dbmanage.initDB();
			sta = conn.createStatement();
			String sql = "SELECT * FROM member WHERE email = " + "'" + email + "'";

			rs = sta.executeQuery(sql);
			while (rs.next()) {
				member = new MemberVo();
				member.setMemberName(rs.getString("name"));
				member.setMemberIntro(rs.getString("intro"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			dbmanage.closeDB(rs, sta, conn);
		}

		return member;
	}
}