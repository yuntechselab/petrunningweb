package com.supermen.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ArrayList;
import java.util.Calendar;

import com.supermen.database.model.DataBaseManager;
import com.supermen.servlet.controller.GlobalVariable;
import com.supermen.vo.EventVo;

public class EventDao {
	public void insertEvent(EventVo event) {
        DataBaseManager dbmanage = new DataBaseManager();
        Connection conn = null;
        Statement sta = null;

        try {
            conn = dbmanage.initDB();
            sta = conn.createStatement();
            String sql = "INSERT INTO event(email, head, startDate, startTime, endDate, endTime, timeZone, area, address, address2, organizerName, organizerEmail, "
            		+ "organizerPhone, category, summary, content, groupName, ticketName, ticketPrice, ticketCount, ticketSale, ticketStartDate, "
            		+ "ticketStartTime, ticketEndDate, ticketEndTime, "
            		+ "ticketDescription, precaution, image) VALUES('"
                    + event.getEventEmail()
                    + "','"
                    + event.getEventHead()
                    + "','"
                    + event.getEventStartDate()
                    + "','"
                    + event.getEventStartTime()
                    + "','"
                    + event.getEventEndDate()
                    + "','"
                    + event.getEventEndTime()
                    + "','"
                    + event.getEventTimeZone()
                    + "','"
                    + event.getEventArea()
                    + "','"
                    + event.getEventAddress()
                    + "','"
                    + event.getEventAddress2()
                    + "','"
                    + event.getEventOrganizerName()
                    + "','"
                    + event.getEventOrganizerEmail()
                    + "','"
                    + event.getEventOrganizerPhone()
                    + "','"
                    + event.getEventCategory()
                    + "','"
                    + event.getEventSummary()
                    + "','"
                    + event.getEventContent()
                    + "','"
                    + event.getEventGroupName()
                    + "','"
                    + event.getEventTicketName()
                    + "','"
                    + event.getEventTicketPrice()
                    + "','"
                    + event.getEventTicketCount()
                    + "','"
                    + event.getEventTicketSale()
                    + "','"
                    + event.getEventTicketStartDate()
                    + "','"
                    + event.getEventTicketStartTime()
                    + "','"
                    + event.getEventTicketEndDate()
                    + "','"
                    + event.getEventTicketEndTime()
                    + "','"
                    + event.getEventTicketDescription()
                    + "','"
                    + event.getEventPrecaution()
                    + "','"
                    + event.getEventImage()
                    + "')";
            sta.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            dbmanage.closeDB(sta, conn);
        }
    }
	
	public String countPeople(String sql) {
        DataBaseManager dbmanage = new DataBaseManager();
        Connection conn = null;
        Statement sta = null;
        ResultSet rs = null;
        String ticketCount = "";
        
        try {
            conn = dbmanage.initDB();
            sta = conn.createStatement();
            
            rs = sta.executeQuery(sql);
            while (rs.next()) {
            	ticketCount = rs.getString("ticketCount");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            dbmanage.closeDB(rs, sta, conn);
        }
        
		return ticketCount;
	}
	
	public ArrayList<EventVo> eventList(String email) {
        DataBaseManager dbmanage = new DataBaseManager();
        Connection conn = null;
        Statement sta = null;
        ResultSet rs = null;
        EventVo event = null;
        ArrayList<EventVo> list = new ArrayList<EventVo>();
        String sql2 = "";
        
        Date date = new Date(); //取得今天日期
        SimpleDateFormat bartDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String today = bartDateFormat.format(date);
        
        try {
            conn = dbmanage.initDB();
            sta = conn.createStatement();
            String sql = "SELECT * FROM event WHERE email = " + "'" + email + "'" + " AND unix_timestamp(CONCAT(endDate, ' ', endTime)) > unix_timestamp(" + "'" + today + "'" +  ")";

            rs = sta.executeQuery(sql);
            while (rs.next()) {
                event = new EventVo();
                event.setEventNo(rs.getInt("no"));
                event.setEventHead(rs.getString("head"));
                event.setEventOrganizerName(rs.getString("organizerName"));
                event.setEventStartDate(rs.getString("startDate"));
                event.setEventStartTime(rs.getString("startTime"));
                event.setEventEndDate(rs.getString("endDate"));
                event.setEventEndTime(rs.getString("endTime"));
                event.setEventTimeZone(rs.getString("timeZone"));
                
                sql2 = "SELECT COUNT(A.eventNo) AS ticketCount FROM apply_event A WHERE A.eventNo IN (SELECT B.no FROM event B WHERE status = '已報名' AND no = " + rs.getInt("no") + ")";
                event.setEventTicketCount(countPeople(sql2));
                list.add(event);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            dbmanage.closeDB(rs, sta, conn);
        }
        
        return list;
    }
	
	public ArrayList<EventVo> eventList2(String email) {
        DataBaseManager dbmanage = new DataBaseManager();
        Connection conn = null;
        Statement sta = null;
        ResultSet rs = null;
        EventVo event = null;
        ArrayList<EventVo> list = new ArrayList<EventVo>();
        String sql2 = "";
        
        Date date = new Date(); //取得今天日期
        SimpleDateFormat bartDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String today = bartDateFormat.format(date);
        
        try {
            conn = dbmanage.initDB();
            sta = conn.createStatement();
            String sql = "SELECT * FROM event WHERE email = " + "'" + email + "'" + " AND unix_timestamp(CONCAT(endDate, ' ', endTime)) < unix_timestamp(" + "'" + today + "'" +  ")";

            rs = sta.executeQuery(sql);
            while (rs.next()) {
                event = new EventVo();
                event.setEventNo(rs.getInt("no"));
                event.setEventHead(rs.getString("head"));
                event.setEventOrganizerName(rs.getString("organizerName"));
                event.setEventStartDate(rs.getString("startDate"));
                event.setEventStartTime(rs.getString("startTime"));
                event.setEventEndDate(rs.getString("endDate"));
                event.setEventEndTime(rs.getString("endTime"));
                event.setEventTimeZone(rs.getString("timeZone"));
                
                sql2 = "SELECT COUNT(A.eventNo) AS ticketCount FROM apply_event A WHERE A.eventNo IN (SELECT B.no FROM event B WHERE status = '已報名' AND no = " + rs.getInt("no") + ")";
                event.setEventTicketCount(countPeople(sql2));
                list.add(event);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            dbmanage.closeDB(rs, sta, conn);
        }
        
        return list;
    }
	
	public EventVo selectEvent(int no) {
        DataBaseManager dbmanage = new DataBaseManager();
        Connection conn = null;
        Statement sta = null;
        ResultSet rs = null;
        EventVo event = null;

        try {
            conn = dbmanage.initDB();
            sta = conn.createStatement();
            String sql = "SELECT * FROM event WHERE no = " + no;
            
            rs = sta.executeQuery(sql);
            while (rs.next()) {
            	event = new EventVo();
                event.setEventNo(rs.getInt("no"));
                event.setEventHead(rs.getString("head"));
                event.setEventStartDate(rs.getString("startDate"));
                event.setEventStartTime(rs.getString("startTime"));
                event.setEventEndDate(rs.getString("endDate"));
                event.setEventEndTime(rs.getString("endTime"));
                event.setEventTimeZone(rs.getString("timeZone"));
                event.setEventArea(rs.getString("area"));
                event.setEventAddress(rs.getString("address"));
                event.setEventAddress2(rs.getString("address2"));
                event.setEventOrganizerName(rs.getString("organizerName"));
                event.setEventOrganizerEmail(rs.getString("organizerEmail"));
                event.setEventOrganizerPhone(rs.getString("organizerPhone"));
                event.setEventCategory(rs.getString("category"));
                event.setEventSummary(rs.getString("summary"));
                event.setEventContent(rs.getString("content"));
                event.setEventGroupName(rs.getString("groupName"));
                event.setEventTicketName(rs.getString("ticketName"));
                event.setEventTicketPrice(rs.getString("ticketPrice"));
                event.setEventTicketCount(rs.getString("ticketCount"));
                event.setEventTicketSale(rs.getString("ticketSale"));
                event.setEventTicketStartDate(rs.getString("ticketStartDate"));
                event.setEventTicketStartTime(rs.getString("ticketStartTime"));
                event.setEventTicketEndDate(rs.getString("ticketEndDate"));
                event.setEventTicketEndTime(rs.getString("ticketEndTime"));
                event.setEventTicketDescription(rs.getString("ticketDescription"));
                event.setEventPrecaution(rs.getString("precaution"));
                event.setEventImage(rs.getString("image"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            dbmanage.closeDB(rs, sta, conn);
        }
        
        return event;
    }
	
	public void modifyEvent(EventVo event) {
        DataBaseManager dbmanage = new DataBaseManager();
        Connection conn = null;
        Statement sta = null;

        try {
            conn = dbmanage.initDB();
            sta = conn.createStatement();
            String sql = "UPDATE event SET head = '" + event.getEventHead()
            		+ "', startDate = '" + event.getEventStartDate()
            		+ "', startTime = '" + event.getEventStartTime()
            		+ "', endDate = '" + event.getEventEndDate()
            		+ "', endTime = '" + event.getEventEndTime()
            		+ "', timeZone = '" + event.getEventTimeZone()
            		+ "', area = '" + event.getEventArea()
            		+ "', address = '" + event.getEventAddress()
            		+ "', address2 = '" + event.getEventAddress2()
            		+ "', organizerName = '" + event.getEventOrganizerName()
            		+ "', organizerEmail = '" + event.getEventOrganizerEmail()
            		+ "', organizerPhone = '" + event.getEventOrganizerPhone()
            		+ "', category = '" + event.getEventCategory()
            		+ "', summary = '" + event.getEventSummary()
            		+ "', content = '" + event.getEventContent()
            		+ "', groupName = '" + event.getEventGroupName()
            		+ "', ticketName = '" + event.getEventTicketName()
            		+ "', ticketPrice = '" + event.getEventTicketPrice()
            		+ "', ticketCount = '" + event.getEventTicketCount()
            		+ "', ticketSale = '" + event.getEventTicketSale()
            		+ "', ticketStartDate = '" + event.getEventTicketStartDate()
            		+ "', ticketStartTime = '" + event.getEventTicketStartTime()
            		+ "', ticketEndDate = '" + event.getEventTicketEndDate()
            		+ "', ticketEndTime = '" + event.getEventTicketEndTime()
            		+ "', ticketDescription = '" + event.getEventTicketDescription()
            		+ "', precaution = '" + event.getEventPrecaution()
            		+ "', image = '" + event.getEventImage()
            		+ "' WHERE no = " + event.getEventNo();
            sta.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            dbmanage.closeDB(sta, conn);
        }
    }
	
	public void deleteEvent(int no) {
        DataBaseManager dbmanage = new DataBaseManager();
        Connection conn = null;
        Statement sta = null;
        
        try {
            conn = dbmanage.initDB();
            sta = conn.createStatement();
            String sql1 = "DELETE FROM event WHERE no = " + no;
            String sql2 = "DELETE FROM apply_event WHERE eventNo = " + no;
            
            sta.addBatch(sql1); 
            sta.addBatch(sql2); 
            sta.executeBatch();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            dbmanage.closeDB(sta, conn);
        }
    }
	
	public EventVo browseEvent(int no) {
        DataBaseManager dbmanage = new DataBaseManager();
        Connection conn = null;
        Statement sta = null;
        ResultSet rs = null;
        EventVo event = null;
        
        int compareDate = 0;
        Date date = new Date(); //取得今天日期
        SimpleDateFormat bartDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String today = bartDateFormat.format(date);

        try {
            conn = dbmanage.initDB();
            sta = conn.createStatement();
            String sql = "SELECT * FROM event WHERE no = " + no;
            
            rs = sta.executeQuery(sql);
            while (rs.next()) {
            	event = new EventVo();
                event.setEventNo(rs.getInt("no"));
                event.setEventHead(rs.getString("head"));
                event.setEventStartDate(rs.getString("startDate"));
                event.setEventStartTime(rs.getString("startTime"));
                event.setEventEndDate(rs.getString("endDate"));
                event.setEventEndTime(rs.getString("endTime"));
                event.setEventTimeZone(rs.getString("timeZone"));
                event.setEventAddress(rs.getString("address"));
                event.setEventAddress2(rs.getString("address2"));
                event.setEventOrganizerName(rs.getString("organizerName"));
                event.setEventContent(rs.getString("content"));
                event.setEventTicketName(rs.getString("ticketName"));
                event.setEventTicketPrice(rs.getString("ticketPrice"));
                event.setEventTicketCount(rs.getString("ticketCount"));
                event.setEventTicketSale(rs.getString("ticketSale"));
                event.setEventImage(rs.getString("image"));
                
                compareDate = compareDate(rs.getString("endDate") + " " + rs.getString("endTime"), today);
                event.setEventCompareDate(compareDate);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            dbmanage.closeDB(rs, sta, conn);
        }
        
        return event;
    }
	
	public EventVo countApplyPeople(int no) {
        DataBaseManager dbmanage = new DataBaseManager();
        Connection conn = null;
        Statement sta = null;
        ResultSet rs = null;
        EventVo event = null;

        try {
            conn = dbmanage.initDB();
            sta = conn.createStatement();
            String sql = "SELECT COUNT(*) AS num FROM apply_event WHERE eventNo = " + no;
            
            rs = sta.executeQuery(sql);
            while (rs.next()) {
            	event = new EventVo();
                event.setEventTicketCount(rs.getString("num"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            dbmanage.closeDB(rs, sta, conn);
        }
        
        return event;
    }
	
	public void viewSearchEvent(int no) {
        DataBaseManager dbmanage = new DataBaseManager();
        Connection conn = null;
        Statement sta = null;

        try {
            conn = dbmanage.initDB();
            sta = conn.createStatement();
            String sql = "UPDATE event SET view = view + 1 WHERE no = " + no;
            sta.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            dbmanage.closeDB(sta, conn);
        }
    }
	
	public static int compareDate(String DATE1, String DATE2) {
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm");
		
		try {
			Date dt1 = df.parse(DATE1);
			Date dt2 = df.parse(DATE2);
			if (dt1.getTime() > dt2.getTime()) {
				return 1;
			} else if (dt1.getTime() < dt2.getTime()) {
				return -1;
			} else {
				return 0;
			}
		} catch (Exception exception) {
			exception.printStackTrace();
		}
		
		return 0;
	}
	
	public EventVo applyEvent(int no, int ticketCount) {
        DataBaseManager dbmanage = new DataBaseManager();
        Connection conn = null;
        Statement sta = null;
        ResultSet rs = null;
        EventVo event = null;

        try {
            conn = dbmanage.initDB();
            sta = conn.createStatement();
            String sql = "SELECT * FROM event WHERE no = " + no;
            
            rs = sta.executeQuery(sql);
            while (rs.next()) {
            	event = new EventVo();
                event.setEventNo(rs.getInt("no"));
                event.setEventHead(rs.getString("head"));
                event.setEventStartDate(rs.getString("startDate"));
                event.setEventStartTime(rs.getString("startTime"));
                event.setEventEndDate(rs.getString("endDate"));
                event.setEventEndTime(rs.getString("endTime"));
                event.setEventAddress(rs.getString("address"));
                event.setEventAddress2(rs.getString("address2"));
                event.setEventOrganizerName(rs.getString("organizerName"));
                event.setEventOrganizerEmail(rs.getString("organizerEmail"));
                event.setEventOrganizerPhone(rs.getString("organizerPhone"));
                event.setEventTicketName(rs.getString("ticketName"));
                event.setEventTicketPrice(rs.getString("ticketPrice"));
                event.setEventTicketCount(Integer.toString(ticketCount));
                event.setEventImage(rs.getString("image"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            dbmanage.closeDB(rs, sta, conn);
        }
        
        return event;
    }

	public EventVo applyFillEvent(int no) {
        DataBaseManager dbmanage = new DataBaseManager();
        Connection conn = null;
        Statement sta = null;
        ResultSet rs = null;
        EventVo event = null;

        try {
            conn = dbmanage.initDB();
            sta = conn.createStatement();
            String sql = "SELECT * FROM event WHERE no = " + no;
            
            rs = sta.executeQuery(sql);
            while (rs.next()) {
            	event = new EventVo();
                event.setEventNo(rs.getInt("no"));
                event.setEventHead(rs.getString("head"));
                event.setEventStartDate(rs.getString("startDate"));
                event.setEventStartTime(rs.getString("startTime"));
                event.setEventEndDate(rs.getString("endDate"));
                event.setEventEndTime(rs.getString("endTime"));
                event.setEventAddress(rs.getString("address"));
                event.setEventAddress2(rs.getString("address2"));
                event.setEventOrganizerName(rs.getString("organizerName"));
                event.setEventOrganizerEmail(rs.getString("organizerEmail"));
                event.setEventOrganizerPhone(rs.getString("organizerPhone"));
                event.setEventTicketName(rs.getString("ticketName"));
                event.setEventTicketPrice(rs.getString("ticketPrice"));
                event.setEventImage(rs.getString("image"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            dbmanage.closeDB(rs, sta, conn);
        }
        
        return event;
    }
	
	public void applySuccessEvent(EventVo event) {
        DataBaseManager dbmanage = new DataBaseManager();
        Connection conn = null;
        Statement sta = null;
        Date date = new Date(); //取得今天日期
        SimpleDateFormat bartDateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
        String today = bartDateFormat.format(date);
        GlobalVariable.today = today;
        
        SimpleDateFormat bartDateFormat2 = new SimpleDateFormat("yyyy-MM-dd");
		try {
			Date date2 = bartDateFormat2.parse(bartDateFormat2.format(date));
	        Calendar calendar = Calendar.getInstance();
	        calendar.setTime(date2);
	        calendar.add(Calendar.DATE, 2);
	        String countDate = bartDateFormat2.format(calendar.getTime());
	        
	        GlobalVariable.countDate = countDate;
		} catch (ParseException e1) {
			e1.printStackTrace();
		}
        
        try {
            conn = dbmanage.initDB();
            sta = conn.createStatement();
            String sql = "INSERT INTO apply_event(num, name, email, phone, organizerName, head, ticketName, ticketPrice, date, initialDate, closeDate, status, eventNo) VALUES('"
                    + today
                    + "','"
                    + event.getEventName()
                    + "','"
                    + event.getEventEmail()
                    + "','"
                    + event.getEventPhone()
                    + "','"
                    + event.getEventOrganizerName()
                    + "','"
                    + event.getEventHead()
                    + "','"
                    + event.getEventTicketName()
                     + "','"
                    + event.getEventTicketPrice()
                    + "','"
                    + event.getEventDate()
                    + "','"
                    + event.getEventInitialDate()
                    + "','"
                    + event.getEventCloseDate()
                    + "','"                    
                    + event.getEventStatus()
                    + "','"
                    + event.getEventNo()
                    + "')";
            sta.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            dbmanage.closeDB(sta, conn);
        }
    }

	public ArrayList<EventVo> myCategoryTicket(String email) {
        DataBaseManager dbmanage = new DataBaseManager();
        Connection conn = null;
        Statement sta = null;
        ResultSet rs = null;
        EventVo event = null;
        ArrayList<EventVo> list = new ArrayList<EventVo>();
        
        try {
            conn = dbmanage.initDB();
            sta = conn.createStatement();
            String sql = "SELECT * FROM apply_event A WHERE A.email IN (SELECT B.email FROM member B WHERE email = " + "'" + email + "'" + ") GROUP BY eventNo";

            rs = sta.executeQuery(sql);
            while (rs.next()) {
                event = new EventVo();
                event.setEventNo(rs.getInt("no"));
                event.setEventName(rs.getString("name"));
                event.setEventEmail(rs.getString("email"));
                event.setEventPhone(rs.getString("phone"));
                event.setEventOrganizerName(rs.getString("organizerName"));
                event.setEventHead(rs.getString("head"));
                event.setEventTicketName(rs.getString("ticketName"));
                event.setEventTicketPrice(rs.getString("ticketPrice"));
                event.setEventDate(rs.getString("date"));
                event.setEventInitialDate(rs.getString("initialDate"));
                event.setEventCloseDate(rs.getString("closeDate"));
                event.setEventStatus(rs.getString("status"));
                event.setEventEventNo(rs.getInt("eventNo"));
                
                list.add(event);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            dbmanage.closeDB(rs, sta, conn);
        }
        
        return list;
    }
	
	public int viewEvent(int no) {
        DataBaseManager dbmanage = new DataBaseManager();
        Connection conn = null;
        Statement sta = null;
        ResultSet rs = null;
        int list = 0;
        
        try {
            conn = dbmanage.initDB();
            sta = conn.createStatement();
            String sql = "SELECT * FROM event WHERE no = " + no;
            
            rs = sta.executeQuery(sql);
            while (rs.next()) {
            	list = rs.getInt("view");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            dbmanage.closeDB(rs, sta, conn);
        }
        
        return list;
    }
	
	public String imageEvent(int no) {
        DataBaseManager dbmanage = new DataBaseManager();
        Connection conn = null;
        Statement sta = null;
        ResultSet rs = null;
        String list = "";
        
        try {
            conn = dbmanage.initDB();
            sta = conn.createStatement();
            String sql = "SELECT * FROM event WHERE no = " + no;
            
            rs = sta.executeQuery(sql);
            while (rs.next()) {
            	list = rs.getString("image");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            dbmanage.closeDB(rs, sta, conn);
        }
        
        return list;
    }
	
	public ArrayList<EventVo> myDetailTicket(String email, int eventNo) {
        DataBaseManager dbmanage = new DataBaseManager();
        Connection conn = null;
        Statement sta = null;
        ResultSet rs = null;
        EventVo event = null;
        ArrayList<EventVo> list = new ArrayList<EventVo>();
        
        try {
            conn = dbmanage.initDB();
            sta = conn.createStatement();
            String sql = "SELECT * FROM apply_event A WHERE A.email IN (SELECT B.email FROM member B WHERE email = " + "'" + email + "'" + " AND eventNo = " + "'" + eventNo + "'" + ")";

            rs = sta.executeQuery(sql);
            while (rs.next()) {
                event = new EventVo();
                event.setEventNo(rs.getInt("no"));
                event.setEventName(rs.getString("name"));
                event.setEventEmail(rs.getString("email"));
                event.setEventPhone(rs.getString("phone"));
                event.setEventHead(rs.getString("head"));
                event.setEventTicketName(rs.getString("ticketName"));
                event.setEventTicketPrice(rs.getString("ticketPrice"));
                event.setEventDate(rs.getString("date"));
                event.setEventStatus(rs.getString("status"));
                event.setEventEventNo(rs.getInt("eventNo"));
                
                list.add(event);
                GlobalVariable.ticketList.add(event);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            dbmanage.closeDB(rs, sta, conn);
        }
        
        return list;
    }
	
	public void applyCancelEvent(int no) {
        DataBaseManager dbmanage = new DataBaseManager();
        Connection conn = null;
        Statement sta = null;

        try {
            conn = dbmanage.initDB();
            sta = conn.createStatement();
            String sql = "UPDATE apply_event SET status = '已取消' WHERE no = " + no;
            sta.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            dbmanage.closeDB(sta, conn);
        }
    }
	
	public ArrayList<EventVo> searchEvent(String keyword) {
        DataBaseManager dbmanage = new DataBaseManager();
        Connection conn = null;
        Statement sta = null;
        ResultSet rs = null;
        EventVo event = null;
        ArrayList<EventVo> list = new ArrayList<EventVo>();
        Date date = new Date(); //取得今天日期
        SimpleDateFormat bartDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String today = bartDateFormat.format(date);
        
        try {
			conn = dbmanage.initDB();
			sta = conn.createStatement();
			
			String sql = "SELECT * FROM event WHERE head LIKE " + "'%" + keyword + "%'";
			
			if (GlobalVariable.area != null && !GlobalVariable.area.equals("0")) {
				sql += " AND area = " + "'" + GlobalVariable.area + "'";
			}
			
			if (GlobalVariable.category != null && !GlobalVariable.category.equals("0")) {
				sql += " AND category = " + "'" + GlobalVariable.category + "'";
			}
			
			if (GlobalVariable.date != null && !GlobalVariable.date.equals("0")) {
				if (GlobalVariable.date.equals("1")) {
					sql += " AND startDate = " + "'" + today + "'";
				} else if (GlobalVariable.date.equals("2")) {
					sql += " AND TO_DAYS(startDate) - TO_DAYS(NOW()) = 1";
				} else if (GlobalVariable.date.equals("3")) {
					sql += " AND YEARWEEK(DATE_FORMAT(startDate, '%Y-%m-%d')) = YEARWEEK(NOW())";
				} else {
					sql += " AND DATE_FORMAT(startDate, '%Y-%m') = DATE_FORMAT(NOW(), '%Y-%m')";
				}
			}
			
			sql += " ORDER BY startDate ASC";
			
	    	rs = sta.executeQuery(sql);
            while (rs.next()) {
                event = new EventVo();
                event.setEventNo(rs.getInt("no"));
                event.setEventHead(rs.getString("head"));
                event.setEventOrganizerName(rs.getString("organizerName"));
                event.setEventStartDate(rs.getString("startDate"));
                event.setEventView(rs.getInt("view"));
                event.setEventImage(rs.getString("image"));
                
                list.add(event);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            dbmanage.closeDB(rs, sta, conn);
        }
        
        return list;
    }
}