package com.supermen.servlet.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.supermen.dao.MemberDao;
import com.supermen.vo.MemberVo;

public class UpdateProfileServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");
        PrintWriter out = response.getWriter();
        
        String email = request.getParameter("email");
        String name = request.getParameter("name");
        String password = request.getParameter("password");
        String code = request.getParameter("code");
        String phone = request.getParameter("phone");
        String nickname = request.getParameter("nickname");
        String sex = request.getParameter("sex");
        String date = request.getParameter("date");
        String id = request.getParameter("id");
        String intro = request.getParameter("intro");
        
        MemberVo member = new MemberVo();
        member.setMemberEmail(email);
        member.setMemberName(name);
        member.setMemberPassword(password);
        member.setMemberCode(code);
        member.setMemberPhone(phone);
        member.setMemberNickname(nickname);
        member.setMemberSex(sex);
        member.setMemberDate(date);
        member.setMemberId(id);
        member.setMemberIntro(intro);
        
        MemberDao memberDao = new MemberDao();
        memberDao.updateProfile(member);
    
        out.println("<SCRIPT LANGUAGE='JavaScript'>");
        out.println("alert('��s���\')");
        out.println("</SCRIPT>");
        request.getRequestDispatcher("index2.jsp").include(request, response);
        
        out.close();
	}
	
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");
        
		HttpSession session = request.getSession();
        String email = (String)session.getAttribute("email");
        MemberDao memberDao = new MemberDao();
        MemberVo member = memberDao.profileMember(email);
        
        request.setAttribute("member", member);
        request.getRequestDispatcher("updateProfile.jsp").forward(request, response);
	}
}