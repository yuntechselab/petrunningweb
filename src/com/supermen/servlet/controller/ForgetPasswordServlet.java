package com.supermen.servlet.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.supermen.dao.MemberDao;
import com.supermen.vo.MemberVo;

public class ForgetPasswordServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");
		PrintWriter out = response.getWriter();
		
		String email = request.getParameter("email");
		String name = request.getParameter("name");
		
	    MemberDao memberDao = new MemberDao();
	    MemberVo member = memberDao.forgetPasswordMember(email, name);
	    
	    if (MemberDao.validateHaveMember(email, name)) {
            out.println("<SCRIPT LANGUAGE='JavaScript'>");
            out.println("alert('查詢成功!\\n您的密碼為: " + member.getMemberPassword() + "\\n請務必妥善保存此密碼!')");
            out.println("</SCRIPT>");
            request.getRequestDispatcher("login.jsp").include(request, response);
		} else {
            out.println("<SCRIPT LANGUAGE='JavaScript'>");
            out.println("alert('查詢失敗，Email或姓名錯誤')");
            out.println("</SCRIPT>");
            request.getRequestDispatcher("forgetPassword.jsp").include(request, response);
		}
		
		out.close();
	}
}