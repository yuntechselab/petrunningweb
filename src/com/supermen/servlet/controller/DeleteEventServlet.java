package com.supermen.servlet.controller;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.supermen.dao.EventDao;
import com.supermen.vo.EventVo;

public class DeleteEventServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");
        
		int no = Integer.parseInt(request.getParameter("no"));
		EventDao eventDao = new EventDao();
		EventVo event = eventDao.selectEvent(no);
		
        File file = new File("D:/workspace/PetRunning/WebContent/upload/" + event.getEventImage());
        file.delete();
        
		eventDao.deleteEvent(no);
		
		HttpSession session = request.getSession();
        String email = (String)session.getAttribute("email");
        ArrayList<EventVo> list = eventDao.eventList(email);
        request.setAttribute("list", list);
        request.getRequestDispatcher("ListEventServlet").forward(request, response);
	}
}