package com.supermen.servlet.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.supermen.dao.MemberDao;

public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");
		PrintWriter out = response.getWriter();
		
		String email = request.getParameter("email");
		String password = request.getParameter("password");

		HttpSession session = request.getSession(false);
		
		if (MemberDao.loginMember(email, password)) {
			if (session != null) {
				session.setAttribute("email", email);
			}
			
			GlobalVariable.email = email;
			request.getRequestDispatcher("index2.jsp").forward(request, response);
		} else {
            out.println("<SCRIPT LANGUAGE='JavaScript'>");
            out.println("alert('�n�J���ѡA�b���αK�X���~')");
            out.println("</SCRIPT>");
            request.getRequestDispatcher("login.jsp").include(request, response);
		}
		
		out.close();
	}
}