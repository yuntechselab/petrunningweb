package com.supermen.servlet.controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.supermen.dao.EventDao;
import com.supermen.vo.EventVo;

public class ListEventServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");
		
		HttpSession session = request.getSession();
        String email = (String)session.getAttribute("email");
        
		EventDao eventDao = new EventDao();
		ArrayList<EventVo> list = eventDao.eventList(email);
		ArrayList<EventVo> list2 = eventDao.eventList2(email);
		request.setAttribute("list", list);
		
		GlobalVariable.beginEvent = list.size();
		GlobalVariable.endEvent = list2.size();
		request.getRequestDispatcher("myEvent.jsp").forward(request, response);
	}
}