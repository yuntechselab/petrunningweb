package com.supermen.servlet.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.supermen.dao.EventDao;
import com.supermen.vo.EventVo;

public class ApplyEventServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");
        
        int no = Integer.parseInt(request.getParameter("no"));
        int ticketCount = Integer.parseInt(request.getParameter("ticketCount"));
		EventDao eventDao = new EventDao();
		EventVo event = eventDao.applyEvent(no, ticketCount);
        
        request.setAttribute("event", event);
        
        GlobalVariable.ticketPrice = event.getEventTicketPrice();
        
        if (event.getEventTicketPrice().equals("�K�O��"))
        	request.getRequestDispatcher("applyFreeEvent.jsp").forward(request, response);
        else
        	request.getRequestDispatcher("applyPaymentEvent.jsp").forward(request, response);
	}
}