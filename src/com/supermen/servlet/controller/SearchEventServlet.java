package com.supermen.servlet.controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.supermen.dao.EventDao;
import com.supermen.vo.EventVo;

public class SearchEventServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");
        
        GlobalVariable.keyword = "";
        GlobalVariable.area = "0";
        GlobalVariable.category = "0";
        GlobalVariable.date = "0";
        
        String keyword = request.getParameter("keyword");
        String area = request.getParameter("area");
        String category = request.getParameter("category");
        String date = request.getParameter("date");
        
        if (keyword != null)
        	GlobalVariable.keyword = keyword;
        if (area != null)
        	GlobalVariable.area = area;
        if (category != null)
        	GlobalVariable.category = category;
        if (date != null)
        	GlobalVariable.date = date;
        
    	EventDao eventDao = new EventDao();
    	ArrayList<EventVo> list = eventDao.searchEvent(keyword);
    	
	    if (!list.isEmpty()) {
	    	request.setAttribute("list", list);
	     	request.getRequestDispatcher("searchEvent.jsp").forward(request, response);
		} else { 
			request.getRequestDispatcher("searchNoEvent.jsp").forward(request, response);
		}
    }
	
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");
        
        String keyword = request.getParameter("keyword");
        String area = request.getParameter("area");
        String category = request.getParameter("category");
        String date = request.getParameter("date");
        
        if (keyword != null)
        	GlobalVariable.keyword = keyword;
        if (area != null)
        	GlobalVariable.area = area;
        if (category != null)
        	GlobalVariable.category = category;
        if (date != null)
        	GlobalVariable.date = date;
        
    	EventDao eventDao = new EventDao();
    	ArrayList<EventVo> list = eventDao.searchEvent(keyword);
    	
    	if (!list.isEmpty()) {
	    	request.setAttribute("list", list);
	     	request.getRequestDispatcher("searchEvent.jsp").forward(request, response);
		} else { 
			request.getRequestDispatcher("searchNoEvent.jsp").forward(request, response);
		}
	}
}