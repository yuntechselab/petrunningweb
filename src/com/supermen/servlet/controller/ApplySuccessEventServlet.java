package com.supermen.servlet.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.supermen.dao.EventDao;
import com.supermen.dao.MemberDao;
import com.supermen.vo.EventVo;
import com.supermen.vo.MemberVo;

public class ApplySuccessEventServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");
        
        int no = Integer.parseInt(request.getParameter("no"));
        String name = request.getParameter("name");
        String email = request.getParameter("email");
        String phone = request.getParameter("phone");
        String organizerName = request.getParameter("organizerName");
        String head = request.getParameter("head");
        String ticketName = request.getParameter("ticketName");
        String ticketPrice = request.getParameter("ticketPrice");
        String date = request.getParameter("date");
        String initialDate = request.getParameter("initialDate");
        String closeDate = request.getParameter("closeDate");
        String status = request.getParameter("status");
        
        EventVo event = new EventVo();
        event.setEventName(name);
        event.setEventEmail(email);
        event.setEventPhone(phone);
        event.setEventOrganizerName(organizerName);
        event.setEventHead(head);
        event.setEventTicketName(ticketName);
        event.setEventTicketPrice(ticketPrice);
        event.setEventDate(date);
        event.setEventInitialDate(initialDate);
        event.setEventCloseDate(closeDate);
        event.setEventStatus(status);
        event.setEventNo(no);
        
        if (GlobalVariable.ticketPrice.equals("�K�O��")) {
        	EventDao eventDao = new EventDao();
	        eventDao.applySuccessEvent(event);
	        
			event = eventDao.applyFillEvent(no);
			
			HttpSession session = request.getSession();
	        String email2 = (String)session.getAttribute("email");
	        MemberDao memberDao = new MemberDao();
	        MemberVo member = memberDao.profileMember(email2);
	        
	        request.setAttribute("event", event);
	        request.setAttribute("member", member);
	        
	        request.getRequestDispatcher("applySuccessFreeEvent.jsp").forward(request, response);
        } else {
    		EventDao eventDao = new EventDao();
    		event = eventDao.applyFillEvent(no);
    		
    		HttpSession session = request.getSession();
	        String email2 = (String)session.getAttribute("email");
	        MemberDao memberDao = new MemberDao();
	        MemberVo member = memberDao.profileMember(email2);
	        
	        request.setAttribute("event", event);
	        request.setAttribute("member", member);
	        
        	request.getRequestDispatcher("applySelectPaymentEvent.jsp").forward(request, response);
        }
    }
	
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");
        
        int no = Integer.parseInt(request.getParameter("no"));
        String name = request.getParameter("name");
        String email = request.getParameter("email");
        String phone = request.getParameter("phone");
        String organizerName = request.getParameter("organizerName");
        String head = request.getParameter("head");
        String ticketName = request.getParameter("ticketName");
        String ticketPrice = request.getParameter("ticketPrice");
        String date = request.getParameter("date");
        String initialDate = request.getParameter("initialDate");
        String closeDate = request.getParameter("closeDate");
        String status = request.getParameter("status");
        
        EventVo event = new EventVo();
        event.setEventName(name);
        event.setEventEmail(email);
        event.setEventPhone(phone);
        event.setEventOrganizerName(organizerName);
        event.setEventHead(head);
        event.setEventTicketName(ticketName);
        event.setEventTicketPrice(ticketPrice);
        event.setEventDate(date);
        event.setEventInitialDate(initialDate);
        event.setEventCloseDate(closeDate);
        event.setEventStatus(status);
        event.setEventNo(no);
        
        EventDao eventDao = new EventDao();
        eventDao.applySuccessEvent(event);
        
		event = eventDao.applyFillEvent(no);
		
		HttpSession session = request.getSession();
        String email2 = (String)session.getAttribute("email");
        MemberDao memberDao = new MemberDao();
        MemberVo member = memberDao.profileMember(email2);
        
        request.setAttribute("event", event);
        request.setAttribute("member", member);
        
        request.getRequestDispatcher("applySuccessPaymentEvent.jsp").forward(request, response);
	}
}