package com.supermen.servlet.controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.supermen.dao.EventDao;
import com.supermen.dao.MemberDao;
import com.supermen.vo.EventVo;
import com.supermen.vo.MemberVo;

public class MyDetailServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");
        
		HttpSession session = request.getSession();
        String email = (String)session.getAttribute("email");
        MemberDao memberDao = new MemberDao();
        MemberVo member = memberDao.myDetailMember(email);
        request.setAttribute("member", member);
        
		EventDao eventDao = new EventDao();
		ArrayList<EventVo> list = eventDao.myCategoryTicket(email);
		
		for (int i = 0; i < list.size(); i++) {
			EventVo event = list.get(i);
			GlobalVariable.viewEvent.add(eventDao.viewEvent(event.getEventEventNo()));
		}
		
		for (int i = 0; i < list.size(); i++) {
			EventVo event = list.get(i);
			GlobalVariable.imageEvent.add(eventDao.imageEvent(event.getEventEventNo()));
		}
		
		request.setAttribute("list", list);
		
        request.getRequestDispatcher("myDetail.jsp").forward(request, response);
	}
}