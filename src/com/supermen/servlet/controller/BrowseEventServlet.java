package com.supermen.servlet.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.supermen.dao.EventDao;
import com.supermen.vo.EventVo;

public class BrowseEventServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");
        
        int no = Integer.parseInt(request.getParameter("no"));
		EventDao eventDao = new EventDao();
		eventDao.viewSearchEvent(no);
		EventVo event = eventDao.browseEvent(no);
		EventVo event2 = eventDao.countApplyPeople(no);
		
        request.setAttribute("event", event);
        request.setAttribute("event2", event2);
        request.getRequestDispatcher("browseEvent.jsp").forward(request, response);
	}
}