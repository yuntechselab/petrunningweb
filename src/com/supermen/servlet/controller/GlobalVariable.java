package com.supermen.servlet.controller;

import java.util.ArrayList;

import com.supermen.vo.EventVo;

public class GlobalVariable {
	public static String email = "";
	public static String today = "";
	public static String countDate = "";
	public static String ticketPrice = "";
	public static int beginEvent = 0;
	public static int endEvent = 0;
	public static String image = "";
	public static int getImage = 0;
	public static ArrayList<EventVo> ticketList = new ArrayList<EventVo>();
	public static ArrayList<Integer> viewEvent = new ArrayList<Integer>();
	public static ArrayList<String> imageEvent = new ArrayList<String>();
	
	public static String keyword = "";
	public static String area = "0";
	public static String category = "0";
	public static String date = "0";
	
	public static int totalDistance = 0;
	public static float speed = 0.0f;
}