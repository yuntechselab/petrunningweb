package com.supermen.servlet.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.supermen.dao.EventDao;
import com.supermen.dao.MemberDao;
import com.supermen.vo.EventVo;
import com.supermen.vo.MemberVo;

public class ApplyFillEventServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");
        
        int no = Integer.parseInt(request.getParameter("no"));
		EventDao eventDao = new EventDao();
		EventVo event = eventDao.applyFillEvent(no);
		
		HttpSession session = request.getSession();
        String email = (String)session.getAttribute("email");
        MemberDao memberDao = new MemberDao();
        MemberVo member = memberDao.profileMember(email);
        
        request.setAttribute("event", event);
        request.setAttribute("member", member);
        
        if (event.getEventTicketPrice().equals("�K�O��"))
        	request.getRequestDispatcher("applyFillFreeEvent.jsp").forward(request, response);
        else
        	request.getRequestDispatcher("applyFillPaymentEvent.jsp").forward(request, response);
	}
	
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");
        
        int no = Integer.parseInt(request.getParameter("no"));
		EventDao eventDao = new EventDao();
		EventVo event = eventDao.applyFillEvent(no);
		
		HttpSession session = request.getSession();
        String email = (String)session.getAttribute("email");
        MemberDao memberDao = new MemberDao();
        MemberVo member = memberDao.profileMember(email);
        
        request.setAttribute("event", event);
        request.setAttribute("member", member);
        
        if (event.getEventTicketPrice().equals("�K�O��"))
        	request.getRequestDispatcher("applyFillFreeEvent.jsp").forward(request, response);
        else
        	request.getRequestDispatcher("applyFillPaymentEvent.jsp").forward(request, response);
	}
}