package com.supermen.servlet.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.supermen.dao.EventDao;

public class ApplyCancelEventServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");
        
		int no = Integer.parseInt(request.getParameter("no"));
		EventDao eventDao = new EventDao();
		eventDao.applyCancelEvent(no);
		
        request.getRequestDispatcher("MyTicketServlet").forward(request, response);
	}
}