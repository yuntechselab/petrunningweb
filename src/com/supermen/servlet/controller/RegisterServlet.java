package com.supermen.servlet.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.supermen.dao.MemberDao;
import com.supermen.vo.MemberVo;

public class RegisterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");
        PrintWriter out = response.getWriter();
        
        String name = request.getParameter("name");
        String email = request.getParameter("email");
        String sex = request.getParameter("sex");
        String code = request.getParameter("code");
        String phone = request.getParameter("phone");
        String password = request.getParameter("password");
        
        
        MemberVo member = new MemberVo();
        member.setMemberName(name);
        member.setMemberEmail(email);
        member.setMemberSex(sex);
        member.setMemberCode(code);
        member.setMemberPhone(phone);
        member.setMemberPassword(password);
        member.setMemberPet(0);

        
        if (MemberDao.validateMember(email) == false) {
        	MemberDao memberDao = new MemberDao();
        	memberDao.signMember(member);
	            
        	out.println("<SCRIPT LANGUAGE='JavaScript'>");
        	out.println("alert('註冊成功!')");
        	out.println("</SCRIPT>");
        	request.getRequestDispatcher("login.jsp").include(request, response);
        } else {
        	out.println("<SCRIPT LANGUAGE='JavaScript'>");
        	out.println("alert('帳號已存在，請前往登入頁，點選忘記密碼!')");
        	out.println("</SCRIPT>");
        	request.getRequestDispatcher("register.jsp").include(request, response);
        }
        
		out.close();
    }
}