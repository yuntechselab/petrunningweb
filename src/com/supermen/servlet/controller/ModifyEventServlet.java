package com.supermen.servlet.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.supermen.dao.EventDao;
import com.supermen.vo.EventVo;

public class ModifyEventServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");
        PrintWriter out = response.getWriter();
        
        int no = Integer.parseInt(request.getParameter("no"));
        String head = request.getParameter("head");
        String startDate = request.getParameter("startDate");
        String startTime = request.getParameter("startTime");
        String endDate = request.getParameter("endDate");
        String endTime = request.getParameter("endTime");
        String timeZone = request.getParameter("timeZone");
        String area = request.getParameter("area");
        String address = request.getParameter("address");
        String address2 = request.getParameter("address2");
        String organizerName = request.getParameter("organizerName");
        String organizerEmail = request.getParameter("organizerEmail");
        String organizerPhone = request.getParameter("organizerPhone");
        String category = request.getParameter("category");
        String summary = request.getParameter("summary");
        String content = request.getParameter("content");
        String groupName = request.getParameter("groupName");
        String ticketName = request.getParameter("ticketName");
        String ticketPrice = request.getParameter("ticketPrice");
        String ticketCount = request.getParameter("ticketCount");
        String ticketSale = request.getParameter("ticketSale");
        String ticketStartDate = request.getParameter("ticketStartDate");
        String ticketStartTime = request.getParameter("ticketStartTime");
        String ticketEndDate = request.getParameter("ticketEndDate");
        String ticketEndTime = request.getParameter("ticketEndTime");
        String ticketDescription = request.getParameter("ticketDescription");
        String precaution = request.getParameter("precaution");
        String image = "";
        
        if (GlobalVariable.getImage == 0) {
        	EventDao eventDao = new EventDao();
    		EventVo event = eventDao.selectEvent(no);
        	image = event.getEventImage();
        } else {
        	image = GlobalVariable.image;
        	GlobalVariable.getImage = 0;
        }
        
        EventVo event = new EventVo();
        event.setEventNo(no);
        event.setEventHead(head);
        event.setEventStartDate(startDate);
        event.setEventStartTime(startTime);
        event.setEventEndDate(endDate);
        event.setEventEndTime(endTime);
        event.setEventTimeZone(timeZone);
        event.setEventArea(area);
        event.setEventAddress(address);
        event.setEventAddress2(address2);
        event.setEventOrganizerName(organizerName);
        event.setEventOrganizerEmail(organizerEmail);
        event.setEventOrganizerPhone(organizerPhone);
        event.setEventCategory(category);
        event.setEventSummary(summary);
        event.setEventContent(content);
        event.setEventGroupName(groupName);
        event.setEventTicketName(ticketName);
        event.setEventTicketPrice(ticketPrice);
        event.setEventTicketCount(ticketCount);
        event.setEventTicketSale(ticketSale);
        event.setEventTicketStartDate(ticketStartDate);
        event.setEventTicketStartTime(ticketStartTime);
        event.setEventTicketEndDate(ticketEndDate);
        event.setEventTicketEndTime(ticketEndTime);
        event.setEventTicketDescription(ticketDescription);
        event.setEventPrecaution(precaution);
        event.setEventImage(image);
        
		EventDao eventDao = new EventDao();
		eventDao.modifyEvent(event);
    
        out.println("<SCRIPT LANGUAGE='JavaScript'>");
        out.println("alert('�s�覨�\')");
        out.println("</SCRIPT>");
        request.getRequestDispatcher("index2.jsp").include(request, response);
        
		out.close();
	}
	
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");
        
        int no = Integer.parseInt(request.getParameter("no"));
		EventDao eventDao = new EventDao();
		EventVo event = eventDao.selectEvent(no);
        
        request.setAttribute("event", event);
        
        if (event.getEventTicketPrice().equals("�K�O��"))
        	request.getRequestDispatcher("modifyFreeEvent.jsp").forward(request, response);
        else
        	request.getRequestDispatcher("modifyPaymentEvent.jsp").forward(request, response);
	}
}