package com.supermen.servlet.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class GetImgServlet extends HttpServlet {
	private static final long serialVersionUID = 4532615845943646849L;
	private String configPath = "D:/workspace/PetRunning/WebContent/upload/";
	    
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");
        
		this.doPost(request, response);
		GlobalVariable.getImage = 1;
	}
	 
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");
        
	   	String file = request.getParameter("file");
	  	File pic = new File(configPath + file);
	  
	 	FileInputStream fis = new FileInputStream(pic);
		OutputStream os = response.getOutputStream();
	  	
		try {
	    	int count = 0;
	    	byte[] buffer = new byte[1024 * 1024];
	    	
	     	while ((count = fis.read(buffer)) != -1)
	     		os.write(buffer, 0, count);
	      	
	     	os.flush();
	     	
	     	GlobalVariable.getImage = 1;
	  	} catch (IOException e) {
	      	e.printStackTrace();
	  	} finally {
	     	if (os != null)
	     		os.close();
	     	if (fis != null)
	          	fis.close();
	 	}  
	}
}