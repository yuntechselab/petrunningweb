package com.supermen.servlet.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

public class UploadifyServlet extends HttpServlet {
	private static final long serialVersionUID = -1510003415195816563L;
	private String configPath = "D:/workspace/PetRunning/WebContent/upload/";
	    
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");
        
		this.doPost(request, response);
	}
	 
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	 	request.setCharacterEncoding("UTF-8");
		response.setContentType("text/html; charset=UTF-8");
	 	
	 	String ret_fileName = null;
		String savePath = configPath; 
		PrintWriter out = response.getWriter();
		
		DiskFileItemFactory factory = new DiskFileItemFactory();
		ServletFileUpload upload = new ServletFileUpload(factory);
		upload.setHeaderEncoding("UTF-8");
		
		try {
	    	List<?> items = upload.parseRequest(request);
	       	Iterator<?> itr = items.iterator();
	   
	       	while (itr.hasNext()) {
	       		DiskFileItem item = (DiskFileItem)itr.next();
	       		String fileName = item.getName();
	       		
	       		if (fileName != null) {
	            	ret_fileName = fileName;
	            	GlobalVariable.image = ret_fileName;
	          	}
	        	
	       		if (!item.isFormField()) {
	       			try {
	       				File uploadedFile = new File(savePath, fileName);
	       				OutputStream os = new FileOutputStream(uploadedFile);
	                  	InputStream is = item.getInputStream();
	                   	byte buf[] = new byte[1024];
	                  	int length = 0;
	                  	
	                  	while ((length = is.read(buf)) > 0) {
	                  		os.write(buf, 0, length);
	                  	}

	                  	os.flush();
	                 	os.close();
	                 	is.close();
	            	} catch (Exception e) {
	                 	e.printStackTrace();
	              	}
	         	}
	    	}
		} catch (FileUploadException e) {
			e.printStackTrace();
	 	}
		
		out.print(ret_fileName);
	  	out.flush();
	   	out.close();
	}
}