package com.supermen.vo;

public class EventVo {
	private int no;
	private int eventNo;
    private String head;
    private String startDate;
    private String startTime;
    private String endDate;
    private String endTime;
    private String timeZone;
    private String area;
    private String address;
    private String address2;
    private String organizerName;
    private String organizerEmail;
    private String organizerPhone;
    private String category;
    private String summary;
    private String content;
    private String groupName;
    private String ticketName;
    private String ticketPrice;
    private String ticketCount;
    private String ticketSale;
    private String ticketStartDate;
    private String ticketStartTime;
    private String ticketEndDate;
    private String ticketEndTime;
    private String ticketDescription;
    private String precaution;
    private String name;
    private String email;
    private String phone;
    private String date;
    private String initialDate;
    private String closeDate;
    private String status;
    private int view;
    private int compareDate;
    private String image;
    
	public void setEventNo(int eventNo) {
		no = eventNo;
	}
	public void setEventEventNo(int eventEventNo) {
		eventNo = eventEventNo;
	}
	public void setEventHead(String eventHead) {
		head = eventHead;
	}
	public void setEventStartDate(String eventStartDate) {
		startDate = eventStartDate;
	}
	public void setEventStartTime(String eventStartTime) {
		startTime = eventStartTime;
	}
	public void setEventEndDate(String eventEndDate) {
		endDate = eventEndDate;
	}
	public void setEventEndTime(String eventEndTime) {
		endTime = eventEndTime;
	}
	public void setEventTimeZone(String eventTimeZone) {
		timeZone = eventTimeZone;
	}
	public void setEventArea(String eventArea) {
		area = eventArea;
	}
	public void setEventAddress(String eventAddress) {
		address = eventAddress;
	}
	public void setEventAddress2(String eventAddress2) {
		address2 = eventAddress2;
	}
	public void setEventOrganizerName(String eventOrganizerName) {
		organizerName = eventOrganizerName;
	}
	public void setEventOrganizerEmail(String eventOrganizerEmail) {
		organizerEmail = eventOrganizerEmail;
	}
	public void setEventOrganizerPhone(String eventOrganizerPhone) {
		organizerPhone = eventOrganizerPhone;
	}
	public void setEventCategory(String eventCategory) {
		category = eventCategory;
	}
	public void setEventSummary(String eventSummary) {
		summary = eventSummary;
	}
	public void setEventContent(String eventContent) {
		content = eventContent;
	}
	public void setEventGroupName(String eventGroupName) {
		groupName = eventGroupName;
	}
	public void setEventTicketName(String eventTicketName) {
		ticketName = eventTicketName;
	}
	public void setEventTicketPrice(String eventTicketPrice) {
		ticketPrice = eventTicketPrice;
	}
	public void setEventTicketCount(String eventTicketCount) {
		ticketCount = eventTicketCount;
	}
	public void setEventTicketSale(String eventTicketSale) {
		ticketSale = eventTicketSale;
	}
	public void setEventTicketStartDate(String eventTicketStartDate) {
		ticketStartDate = eventTicketStartDate;
	}
	public void setEventTicketStartTime(String eventTicketStartTime) {
		ticketStartTime = eventTicketStartTime;
	}
	public void setEventTicketEndDate(String eventTicketEndDate) {
		ticketEndDate = eventTicketEndDate;
	}
	public void setEventTicketEndTime(String eventTicketEndTime) {
		ticketEndTime = eventTicketEndTime;
	}
	public void setEventTicketDescription(String eventTicketDescription) {
		ticketDescription = eventTicketDescription;
	}
	public void setEventPrecaution(String eventPrecaution) {
		precaution = eventPrecaution;
	}
	public void setEventName(String eventName) {
		name = eventName;
	}
	public void setEventEmail(String eventEmail) {
		email = eventEmail;
	}
	public void setEventPhone(String eventPhone) {
		phone = eventPhone;
	}
	public void setEventDate(String eventDate) {
		date = eventDate;
	}
	public void setEventInitialDate(String eventInitialDate) {
		initialDate = eventInitialDate;
	}
	public void setEventCloseDate(String eventCloseDate) {
		closeDate = eventCloseDate;
	}
	public void setEventStatus(String eventStatus) {
		status = eventStatus;
	}
	public void setEventView(int eventView) {
		view = eventView;
	}
	public void setEventCompareDate(int eventCompareDate) {
		compareDate = eventCompareDate;
	}
	public void setEventImage(String eventImage) {
		image = eventImage;
	}
	
	public int getEventNo() {
		return no;
	}
	public int getEventEventNo() {
		return eventNo;
	}
	public String getEventHead() {
		return head;
	}
	public String getEventStartDate() {
		return startDate;
	}
	public String getEventStartTime() {
		return startTime;
	}
	public String getEventEndDate() {
		return endDate;
	}
	public String getEventEndTime() {
		return endTime;
	}
	public String getEventTimeZone() {
		return timeZone;
	}
	public String getEventArea() {
		return area;
	}
	public String getEventAddress() {
		return address;
	}
	public String getEventAddress2() {
		return address2;
	}
	public String getEventOrganizerName() {
		return organizerName;
	}
	public String getEventOrganizerEmail() {
		return organizerEmail;
	}
	public String getEventOrganizerPhone() {
		return organizerPhone;
	}
	public String getEventCategory() {
		return category;
	}
	public String getEventSummary() {
		return summary;
	}
	public String getEventContent() {
		return content;
	}
	public String getEventGroupName() {
		return groupName;
	}
	public String getEventTicketName() {
		return ticketName;
	}
	public String getEventTicketPrice() {
		return ticketPrice;
	}
	public String getEventTicketCount() {
		return ticketCount;
	}
	public String getEventTicketSale() {
		return ticketSale;
	}
	public String getEventTicketStartDate() {
		return ticketStartDate;
	}
	public String getEventTicketStartTime() {
		return ticketStartTime;
	}
	public String getEventTicketEndDate() {
		return ticketEndDate;
	}
	public String getEventTicketEndTime() {
		return ticketEndTime;
	}
	public String getEventTicketDescription() {
		return ticketDescription;
	}
	public String getEventPrecaution() {
		return precaution;
	}
	public String getEventName() {
		return name;
	}
	public String getEventEmail() {
		return email;
	}
	public String getEventPhone() {
		return phone;
	}
	public String getEventDate() {
		return date;
	}
	public String getEventInitialDate() {
		return initialDate;
	}
	public String getEventCloseDate() {
		return closeDate;
	}
	public String getEventStatus() {
		return status;
	}
	public int getEventView() {
		return view;
	}
	public int getEventCompareDate() {
		return compareDate;
	}
	public String getEventImage() {
		return image;
	}
}