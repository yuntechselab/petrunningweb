package com.supermen.vo;

public class MemberVo {
	private int no;
    private String name;
    private String email;
    private String sex;
    private String code;
    private String phone;
    private String password;
    private String nickname;
    private String date;
    private String id;
    private String intro;
    private int pet;
    
	public int getMemberPet() {
		return pet;
	}
	public void setMemberPet(int memberPet) {
		pet = memberPet;
	}
	public void setMemberNo(int memberNo) {
		no = memberNo;
	}
	public void setMemberName(String memberName) {
		name = memberName;
	}
	public void setMemberEmail(String memberEmail) {
		email = memberEmail;
	}
	public void setMemberSex(String memberSex) {
		sex = memberSex;
	}
	public void setMemberCode(String memberCode) {
		code = memberCode;
	}
	public void setMemberPhone(String memberPhone) {
		phone = memberPhone;
	}
	public void setMemberPassword(String memberPassword) {
		password = memberPassword;
	}
	public void setMemberNickname(String memberNickname) {
		nickname = memberNickname;
	}
	public void setMemberDate(String memberDate) {
		date = memberDate;
	}
	public void setMemberId(String memberId) {
		id = memberId;
	}
	public void setMemberIntro(String memberIntro) {
		intro = memberIntro;
	}
	
	public int getMemberNo() {
		return no;
	}
	public String getMemberName() {
		return name;
	}
	public String getMemberEmail() {
		return email;
	}
	public String getMemberSex() {
		return sex;
	}
	public String getMemberCode() {
		return code;
	}
	public String getMemberPhone() {
		return phone;
	}
	public String getMemberPassword() {
		return password;
	}
	public String getMemberNickname() {
		return nickname;
	}
	public String getMemberDate() {
		return date;
	}
	public String getMemberId() {
		return id;
	}
	public String getMemberIntro() {
		return intro;
	}
}